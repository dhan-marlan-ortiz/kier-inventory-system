

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->session->sess_destroy();
?>

	<form method="POST" action="<?=site_url('login_controller')?>">
		<div class='page-header'>
			<div class='container'>
				<div class='row'>
					<div class='col-sm-10'>
						<h1 class='text-success'><i class='glyphicon glyphicon-shopping-cart'></i>&nbsp;Kier Enterprises</h1>
					</div>
					<div class='col-sm-2'>
						<button class='btn btn-success btn-lg' type='submit' >Login </button>
					</div>
				</div>
			</div>
		</div>
	</form>		
				
					

		
		
		
		
<nav class="navbar navbar-default navbar-fixed-bottom">
  <div class="container">
    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
  </div>
</nav>

