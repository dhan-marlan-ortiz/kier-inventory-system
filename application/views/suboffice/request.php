<div class='container' >
	<div class='page-header'>
		<div class='row'>

			<div class='col-sm-12'>
				<span class='text-success h3'><i class='fa fa-send-o fa-fw'></i> Outbox </span>
				<div class="btn-group pull-right" role="group" aria-label="...">
				  <a href="<?=site_url('suboffice/create_request')?>" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Create New Order"><i class='fa fa-list-alt fa-fw'></i>&nbsp;Create</a>
				  <a href="<?=site_url("suboffice/inbox")?>" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Inbox"><i class='fa fa-inbox fa-fw'></i>&nbsp;Inbox</a>
				  <a href="<?=site_url('suboffice/request')?>" class="btn btn-default active" data-toggle="tooltip" data-placement="bottom" title="Pending Orders"><i class='fa fa-send-o fa-fw'></i>&nbsp;Outbox</a>
				  <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Trash"><i class='fa fa-recycle fa-fw'></i>&nbsp;Trash</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class='container'>

	<div class='row'>
		<div class="col-sm-5 col-sm-offset-7">

		</div>
	</div>

	<div class='table-responsive'>
		<table class='table table-condensed table-striped table-hover'>
			<thead>
				<tr>
					<th><span class='text-success'>#</span></th>
					<th><span class='text-success'>Subject</span></th>
					<th><span class='text-success'>Recipient</span></th>
					<th><span class='text-success'>Date</span></th>
					<th><span class='text-success'></span></th>
				</tr>
			</thead>
			<?php if(!isset($request)){ }else{ //if result is not empty ?>
			<tbody><?php $line_number = 1;	$i=0; foreach ($request as $request_item): ?>
				<form method='POST' action= "<?=site_url("suboffice/open_order")?>" role='form'>

					<tr>
						<td><?php echo $line_number; ?></td>
						<td><?php echo $request_item['orderSubject']; ?> </td>
						<td><?php echo $request_item['orderRecipient']; ?> </td>
						<td><?php echo $request_item['request_date']; ?> </td>
						<td><button type='submit' name='submit' class='btn btn-default'><i class='fa fa-folder-open-o fa-fw'></i>&nbsp;Open</button></td>
						<input type='hidden' name="request_id" value="<?php echo $request_item['request_id']; ?>" />
					</tr>
				</form><!--product list form end-->
				<?php $line_number++; $i++; endforeach ?>
			 </tbody>
			 <?php } // result end?>
		</table>
	</div><!--table responsive end-->

</div>

<nav class="navbar navbar-default navbar-fixed-bottom">
  <div class="container">
    <ol class="breadcrumb">
	  <li><a href="#"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</a></li>
	  <li class="active">Outbox </li>
	</ol>
  </div>
</nav>
