<!DOCTYPE html>
<html lang="en">
	<head>
		<title> <?php echo $title; ?>	</title>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta charset='utf-8'>
		<link href="<?php echo base_url('styles/css/bootstrap.css') ?>" rel='stylesheet'>
		<link href="<?php echo base_url('styles/css/bootstrap.min.css') ?>" rel='stylesheet'>
		<link href="<?php echo base_url('styles/font-awesome-4.4.0/css/font-awesome.css') ?>" rel='stylesheet'>
		<link href="<?php echo base_url('styles/font-awesome-4.4.0/css/font-awesome.min.css') ?>" rel='stylesheet'>
		<link href="<?php echo base_url('styles/css/personalize.css') ?>" rel='stylesheet'>
		<style> @media print {  thead {display: table-header-group;} } </style>
	</head>
<body>
