
	<div class='page-header' >
		<div class='container'>
			<div class='row'>
				<div class='col-sm-7'>
					<span class='text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
				</div>
				<div class='col-sm-5'>
					<div class="input-group">
					  <input type="text" class="form-control" placeholder="Search for...">
					  <span class="input-group-btn">
						<button class="btn btn-primary" type="button"><i class='fa fa-search fa-fw'></i>&nbsp;Search</button>
					  </span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class='table table-responsive' style='max-height: 400px; overflow: auto'>
			<table class="table table-fixed table-condensed table-striped">

				  <thead>
					<tr>
					  <th><span class='text-success'>#</span></th>
						<th><span class='text-success'>Product</span></th>
						<th><span class='text-success'>SKU</span></th>
						<th><span class='text-success'>Category</span></th>
						<th><span class='text-success'>Box</span></th>
						<th><span class='text-success'>Pack</span></th>
					  <th><span class='text-success'>Piece</span></th>
						<th><span class='text-success'>Details</span></th>
					</tr>
				  </thead>

				  <tbody><?php $line_number = 1;	$i=0; foreach ($product as $product_detail): ?>
					<form method='POST' action= "<?=site_url("warehouse/product_information")?>" role='form'>
						<tr>
							<td><?php echo $line_number; ?></td>
							<td><?php echo $product_detail['product_name']; ?> </td>
							<td><?php echo $product_detail['sku']; ?> </td>
							<td><?php echo $product_detail['product_type']; ?> </td>
							<td><?php echo $product_detail['box_stock']; ?> </td>
							<td><?php echo $product_detail['pack_stock']; ?> </td>
							<td><?php echo $product_detail['piece_stock']; ?> </td>

							<?php
								$sku = $product_detail['sku'];								$name = $product_detail['product_name'];
								$desc = $product_detail['product_description'];				$id = $product_detail['product_id'];
							?>

							<td><button type='button' class='btn btn-link' data-toggle="tooltip" data-placement="bottom" title="View details"><i class='fa fa-folder-open-o fa-fw'></i></button></td>
							<input type='hidden' name='id' value="<?php echo $product_detail['product_id']?>">
						</tr><?php $line_number++; $i++; endforeach ?>
					</form><!--product list form end-->
				  </tbody>

			</table>
		</div><!-- table div end-->
	</div> <!--container end-->







  <div class="container">
    <ol class="breadcrumb pull-right">
	  <li class="active"><i class='fa fa-cubes fa-fw'></i>&nbsp; Inventory</li>
		</ol>
  </div>
