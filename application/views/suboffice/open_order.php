<?php
	foreach ($request as $request_item):
		$subject = $request_item['orderSubject'];
		$recipient = $request_item['orderRecipient'];
		$date = $request_item['request_date'];
		$location = $request_item['deliveryLocation'];
		$orderNo = $request_item['request_id'];
	endforeach;
?>

<div class='container' >
	<div class='page-header'>
		<div class='row'>
			<div class='col-sm-12'>
				<span class='text-success h3'><i class='fa fa-send-o fa-fw'></i> Outbox </span>
				<div class="btn-group pull-right">
					<a type="button" class='btn btn-default' href="<?=site_url("suboffice/request")?>"><i class='fa fa-angle-double-left'></i>&nbsp;Return </a>
					<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Forward"><i class='fa fa-mail-forward fa-fw'></i> Forward</button>
					<!-- <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class='fa fa-trash fa-fw'></i> Delete</button> -->
					<button type="button" class="btn btn-default" onclick="printContent('printThis')"><i class='fa fa-print fa-fw'></i> Print</button>
				</div>
			</div>
		</div>
	</div>
</div>



<div class='container'>
	<div class='container-fluid panel panel-default' id='printThis'>
		<div class=''>
			<div class='row'>
				<div class='col-sm-12'>
					<h2 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i> Request Form</h2>
				</div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>To</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $recipient; ?></div>
				<div class='col-sm-2 col-xs-2'><strong>Request No.</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $orderNo; ?></div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Subject</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $subject; ?></div>
				<div class='col-sm-2 col-xs-2'><strong>Send order to</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $location; ?></div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Date</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $new_date_format = date('F d, Y', strtotime($date)); ?></div>
			</div>
			<hr>


		</div>




				<div id='product_table'>
					<table class='table table-condensed table-striped table-hover table-bordered'>
						<thead>
							<tr>
								<th colspan='4'></th>
								<th colspan='3' class='text-primary text-center'>Requested</th>
								<th colspan='3' class='text-success text-center'>Approved</th>
								<th colspan='2'></th>
							</tr>
							<tr>
								<th><span class='text-default'>#</span></th>
								<th><span class='text-default'>Product</span></th>
								<th><span class='text-default'>SKU</span></th>
								<th><span class='text-default'>Category</span></th>
								<th><span class='text-primary'>Box</span></th>
								<th><span class='text-primary'>Pack</span></th>
								<th><span class='text-primary'>Piece</span></th>
								<th><span class='text-success'>Box</span></th>
								<th><span class='text-success'>Pack</span></th>
								<th><span class='text-success'>Piece</span></th>
								<th><span class='text-default'>Status</span></th>
								<th><span class='text-default'>Note</span></th>
							</tr>
						</thead>

						<tbody>
						 <?php if(isset($orderedList)){ ?>
							<?php $line_number = 1;
							foreach ($orderedList as $request_item): ?>
								<tr>
									<td><?php echo $line_number; ?></td>
									<td><?php echo $request_item['product_name']; ?> </td>
									<td><?php echo $request_item['sku']; ?> </td>
									<td><?php echo $request_item['product_type']; ?> </td>
									<td><?php echo $request_item['box']; ?> </td>
									<td><?php echo $request_item['pack']; ?> </td>
									<td><?php echo $request_item['piece']; ?> </td>
									<td><?php echo $request_item['approved_box']; ?> </td>
									<td><?php echo $request_item['approved_pack']; ?> </td>
									<td><?php echo $request_item['approved_piece']; ?> </td>
									<td><?php echo $request_item['status']; ?> </td>
									<td><?php echo $request_item['orderMessage']; ?> </td>
								</tr>
							<?php $line_number++;
							endforeach ?>

						<?php } ?>
						 </tbody>

					</table>
				</div>



	</div>
</div>


<div class='page-header'>
	<div class='container'>
		<ol class="breadcrumb pull-right">
		  <li class='active'><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</li>
		  <li><a href="<?=site_url("suboffice/request")?>"><i class='fa fa-send fa-fw'></i>&nbsp;Outbox</a></li>
		  <li class='active'><i class='fa fa-list-alt fa-fw'></i>&nbsp;Request Form</li>
		</ol>
	</div>
</div>
