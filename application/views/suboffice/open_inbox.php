	<div class='container' >
	<div class='page-header'>
		<span class='text-success h3'><i class='fa fa-inbox fa-fw'></i> Inbox </span>
	</div>
</div>

<?php
	foreach ($inboxInfo as $inboxData):
		$firstname = $inboxData['firstname'];
		$lastname = $inboxData['lastname'];
		$account_id = $inboxData['account_id'];
		$subject = $inboxData['orderSubject'];
		$message = $inboxData['orderMessage'];
		$date = $inboxData['request_date'];
		$location = $inboxData['deliveryLocation'];
	endforeach;
?>

<div class='container'>
	<div class='row'>

		<div class='col-sm-4 col-xs-12'>
			<dl class="dl-horizontal">
			  <dt>Sender :</dt><dd><?php echo $firstname." ".$lastname; ?></dd>
			  <dt>Employee ID :</dt><dd><?php echo $account_id; ?></dd>
			  <dt>Send this order to : </dt><dd><?php echo $location; ?></dd>
			  <dt>Date requested : </dt><dd><?php echo $date; ?></dd>
			</dl>
		</div> <!--end column 1-->

		<div class='col-sm-8 col-xs-12'>
			<div class='row'>

				<div class='col-sm-3 col-xs-4'>
					<h4><span class='text-primary'><i class='fa fa-list-alt fa-fw'></i>&nbsp; Order list</span></h4>
				</div>

				<div class='col-sm-6 col-xs-4'>
					<div class="btn-group" role="group" aria-label="...">
					  <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Forward"><i class='fa fa-mail-forward fa-fw'></i></button>
					  <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class='fa fa-trash fa-fw'></i></button>
					  <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Print"><i class='fa fa-print fa-fw'></i></button>
					</div>
				</div>

				<div class='col-sm-3 col-xs-4'>
					<a class='btn btn-default pull-right' href="<?=site_url("suboffice/inbox")?>"><i class='fa fa-angle-double-left'></i>&nbsp; Return </a>
				</div>

			</div>
			<div class='table-responsive'  style='max-height: 350px; overflow: auto'>
				<table class='table table-condensed table-striped table-hover'>
					<thead>
						<tr>
							<th><span class='text-success'>#</span></th>
							<th><span class='text-success'>Product</span></th>
							<th><span class='text-success'>SKU</span></th>
							<th><span class='text-success'>Category</span></th>

							<th><span class='text-success'>Box</span></th>
							<th><span class='text-success'>Pack</span></th>
							<th><span class='text-success'>Piece</span></th>
							<th><span class='text-success'>Status</span></th>
							<th><span class='text-success'>Note</span></th>
							<th><span class='text-success'></span></th>
						</tr>
					</thead>

					<tbody>
					<?php if(isset($inboxOrderList)){ ?>
						<?php  $line = 1; foreach($inboxOrderList as $orderList): ?>
							<tr>
								<td> <?php echo $line; ?> </td>
								<td> <?php echo $orderList['product_name']; ?> </td>
								<td> <?php echo $orderList['sku']; ?> </td>
								<td> <?php echo $orderList['product_type']; ?> </td>

								<td> <?php echo $orderList['box']; ?></td>
								<td> <?php echo $orderList['pack']; ?> </td>
								<td> <?php echo $orderList['piece']; ?> </td>
								<td> <?php echo $orderList['status']; ?> </td>
								<td> <?php echo $orderList['orderMessage']; ?> </td>
								<td> <a><i class='fa fa-edit fa-fw'></i></a> </td>
							</tr>
					<?php $line++; endforeach; } ?>

					</tbody>

				</table>
			</div>

		</div> <!--end col 2-->

	</div>
</div>



  <div class="container">
    <ol class="breadcrumb pull-right">
	  <li class='active'><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</li>
	  <li><a href="<?=site_url("suboffice/inbox")?>"><i class='fa fa-send fa-fw'></i>&nbsp;Inbox</a></li>
	  <li class='active'><i class='fa fa-list-alt fa-fw'></i>&nbsp;Order List</li>
	</ol>
  </div>
