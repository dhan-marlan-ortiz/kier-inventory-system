<?php
	foreach ($request as $request_item):
		$subject = $request_item['orderSubject'];
		$recipient = $request_item['orderRecipient'];
		$date = $request_item['request_date'];
		$location = $request_item['deliveryLocation'];
		$orderNo = $request_item['request_id'];
	endforeach; 
?>

<div class='page-header' >
	<div class='container'>
		<div class='row'>
			<div class='col-sm-3 col-xs-12'>
				<span class='text-success h3'><i class='fa fa-list-alt fa-fw'></i> Create Order </span>
			</div>
			<div class='col-sm-4 col-xs-12'>
				<div class="btn-group" role="group" aria-label="...">
				  <a  href="<?=site_url('suboffice/create_request')?>" class="btn btn-default active" data-toggle="tooltip" data-placement="bottom" title="Create New Order"><i class='fa fa-list-alt fa-fw'></i>&nbsp;Create</a>
				  <a href="<?=site_url("suboffice/inbox")?>" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Inbox"><i class='fa fa-inbox fa-fw'></i>&nbsp;Inbox</a>
				  <a href="<?=site_url("suboffice/request")?>" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Pending Orders"><i class='fa fa-send-o fa-fw'></i>&nbsp;Outbox</a>
				  <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Trash"><i class='fa fa-recycle fa-fw'></i>&nbsp;Trash</button>
				</div>
			</div>
			<div class="col-sm-5 col-xs-12'">
				<div class="input-group">
				  <input type="text" class="form-control" placeholder="Search for...">
				  <span class="input-group-btn">
					<button class="btn btn-primary" type="button"><i class='fa fa-search fa-fw'></i>&nbsp;Search</button>
				  </span>
				</div>
			</div>
		</div>  
	</div>
</div><!--page header end-->

<div class='container panel panel-default'>
	<div class='page-header'>
		<div class='row'>
			<div class='col-sm-6'>
				<h1 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i>&nbsp;Request Form</h1>	
			</div>
			<div class='col-sm-6'>
				<dl class="dl-horizontal pull-right">
					<dt>To :</dt><dd><?php echo $recipient; ?></dd>
					<dt>Subject :</dt><dd><?php echo $subject; ?></dd>
					<dt>Date requested : </dt> <dd> <?php echo $date; ?> </dd>
					<dt>Order No. : </dt><dd> <?php echo $orderNo; ?> </dd>
					<dt>Send order to : </dt><dd><?php echo $location; ?></dd>
				</dl>
			</div>
		</div>		
	</div>
	
	<div class='panel panel-default'>
		<div class='panel-heading'>
			<h4>Products</h4>
		</div>
		<div class='panel-body'>
			<div class='table-responsive' style='max-height: 350px; overflow: auto' id='product_table'>
				<table class='table table-condensed table-striped table-hover'>
					<thead>
						<tr>
							<th><span class='text-success'>#</span></th>
							<th><span class='text-success'>Type</span></th>
							<th><span class='text-success'>SKU</span></th>
							<th><span class='text-success'>Name</span></th>
							<th><span class='text-success'>Description</span></th>
							<th><span class='text-success'>Box</span></th>
							<th><span class='text-success'>Pack</span></th>
							<th><span class='text-success'>Piece</span></th>
							<th><span class='text-success'>Status</span></th>
							<th><span class='text-success'>Note</span></th>
						</tr>
					</thead>
					
					<tbody>
					 <?php if(isset($orderedList)){ ?>
						<?php $line_number = 1; 
						foreach ($orderedList as $request_item): ?>
							<tr>
								<td><?php echo $line_number; ?></td>								
								<td><?php echo $request_item['product_type']; ?> </td>	
								<td><?php echo $request_item['sku']; ?> </td>	
								<td><?php echo $request_item['product_name']; ?> </td>	
								<td><?php echo $request_item['product_description']; ?> </td>	
								<td><?php echo $request_item['box']; ?> </td>	
								<td><?php echo $request_item['pack']; ?> </td>	
								<td><?php echo $request_item['piece']; ?> </td>	
								<td><?php echo $request_item['status']; ?> </td>	
								<td><?php echo $request_item['orderMessage']; ?> </td>	
							</tr>
						<?php $line_number++; 
						endforeach ?>
						
					<?php } ?>
					 </tbody>
					 
				</table>
			</div>
		</div>
		<div class='panel-footer'>
			
		</div>
	</div>
	
</div>

<div class='container'>
	
		<div class='row'>
			
			<div class='col-sm-2 '>
				<a class="btn btn-default btn-block" href="<?=site_url('')?>" ><i class='fa fa-edit'></i>&nbsp;Modify</a>
			</div>
			<div class='col-sm-2 col-sm-offset-6'>
				<a class="btn btn-default btn-block" href="<?php echo site_url('suboffice/printRequest/'.$orderNo); ?>" target="_blank"><i class='fa fa-print'></i>&nbsp;Print</a>
			</div>
			<div class='col-sm-2'>
				<a name='submit' href="<?=site_url('suboffice/request')?>" class="btn btn-success btn-block">Finish&nbsp;<i class='fa fa-check'></i></a>
			</div>
		
		</div>
	
</div>

<div class='page-header'>
	<div class='container'>
		<ol class="breadcrumb pull-right">
		  <li class='active'><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</li>
		  <li><a href="<?=site_url("suboffice/request")?>"><i class='fa fa-send fa-fw'></i>&nbsp;Outbox</a></li>
		  <li class='active'><i class='fa fa-list-alt fa-fw'></i>&nbsp;Order List</li>
		</ol>
	</div>
</div>




	
		
		
		
		
		
