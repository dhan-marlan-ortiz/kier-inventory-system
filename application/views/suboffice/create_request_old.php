<style> .short{ max-width: 80px; min-width: 80px; } </style>
	
<div class='page-header' >
	<div class='container'>
		<div class='row'>
			<div class='col-sm-3 col-xs-12'>
				<span class='text-success h3'><i class='fa fa-list-alt fa-fw'></i> Create Order </span>
			</div>
			<div class='col-sm-4 col-xs-12'>
				<div class="btn-group" role="group" aria-label="...">
				  <a  href="<?=site_url('suboffice/create_request')?>" class="btn btn-default active" data-toggle="tooltip" data-placement="bottom" title="Create New Order"><i class='fa fa-list-alt fa-fw'></i>&nbsp;Create</a>
				  <a href="<?=site_url("suboffice/inbox")?>" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Inbox"><i class='fa fa-inbox fa-fw'></i>&nbsp;Inbox</a>
				  <a href="<?=site_url("suboffice/request")?>" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Pending Orders"><i class='fa fa-send-o fa-fw'></i>&nbsp;Outbox</a>
				  <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Trash"><i class='fa fa-recycle fa-fw'></i>&nbsp;Trash</button>
				</div>
			</div>
			<div class="col-sm-5 col-xs-12'">
				<div class="input-group">
				  <input type="text" class="form-control" placeholder="Search for...">
				  <span class="input-group-btn">
					<button class="btn btn-primary" type="button"><i class='fa fa-search fa-fw'></i>&nbsp;Search</button>
				  </span>
				</div>
			</div>
			
			
				
			
			
		</div>  
	</div>
</div><!--page header end-->
	
<div class="container">
	<div class="row">
		<div class='col-sm-7'>
		  <div class="panel panel-default">
		  
				<div class="panel-heading">
					<h4><span class='text-primary'><i class='fa fa-shopping-cart fa-fw'></i>&nbsp;Products</span></h4>
				</div><!--panel heading end-->
				
				<div class='table table-responsive' style='max-height: 350px; overflow: auto'>
					<table class="table table-fixed table-condensed table-striped">
					  <thead>
						<tr>
						  <th><span class='text-success'>#</span></th>				<th><span class='text-success'>Type</span></th>	
						  <th><span class='text-success'>SKU</span></th>			<th><span class='text-success'>Name</span></th>	
						  <th><span class='text-success'>Description</span></th>	<th><span class='text-success'>Order</span></th>			
						  <th><span class='text-success'>Details</span></th>
						</tr>
					  </thead>
					  
					  <tbody><?php $line_number = 1;  $i=0; foreach ($product as $product_detail): ?>
						<form method='POST' action="<?=site_url("suboffice/product_information")?>" role='form'>
							<tr>
								<td><?php echo $line_number; ?></td>								<td><?php echo $product_detail['product_type']; ?> </td>
								<td><?php echo $product_detail['sku']; ?> </td>						<td><?php echo $product_detail['product_name']; ?> </td>
								<td><?php echo $product_detail['product_description']; ?> </td>		
											
								
								<?php
									$sku = $product_detail['sku'];						$name = $product_detail['product_name'];	
									$desc = $product_detail['product_description'];		$id = $product_detail['product_id'];
								?>
								
								<td><button type="button" class="btn btn-link" onclick="addRow('dataTable', '<?php echo $sku; ?>',  '<?php echo $name; ?>',  '<?php echo $desc; ?>', '<?php echo $id; ?>')"><i class='fa fa-shopping-cart fa-fw'></i></button></td>
								<td><button type='button' class='btn btn-link' data-toggle="tooltip" data-placement="bottom" title="View details"><i class='fa fa-folder-open-o fa-fw'></i></button></td>
								<input type='hidden' name='id' value="<?php echo $product_detail['product_id']?>">
							</tr><?php $line_number++; $i++; endforeach ?>
						</form><!--product list form end-->
					  </tbody>
					</table>
				</div> <!--div table end-->
		  
		  </div><!--panel end-->
	  </div> <!--end col-->
 
	<form method="post" action="<?=site_url('suboffice/order_submit')?>" id="orderList"> 
 
		<div class='col-sm-5'>
			<div class='panel panel-default'>
				
				<div class="panel-heading">
					<h4><span class='text-primary'><i class='fa fa-list-alt fa-fw'></i>&nbsp;Order list</span></h4>
				</div>
		  
				<div class='table table-responsive' style='max-height: 300px; overflow: auto'>
					<table  class='table table-condensed table-hover' id="dataTable">
						<tr>
							<th></th>
							<th><span class='text-success'>SKU</span></th>
							<th><span class='text-success'>Name</span></th>
							<th><span class='text-success'>Description</span></th>
							<th><span class='text-success'>Box</span></th>
							<th><span class='text-success'>Pack</span></th>
							<th><span class='text-success'>Piece</span></th>
							<th></th>
						</tr>
					</table>
				</div>
				
				<div class="panel-footer">
					<div class='row'>
						<div class='col-sm-6 col-xs-6'>
							<button class='btn btn-default btn-sm' type='button' onclick="deleteRow('dataTable')"><i class='fa fa-remove'></i>&nbsp;Remove </button>
							<a class='btn btn-default pull-right btn-sm' href="<?=site_url("suboffice/create_request")?>" type='button'><i class='fa fa-eraser'></i>&nbsp; Clear All </a>
						</div>
						<div class='col-sm-6 col-xs-6'>
							<button type="button" class="btn btn-primary pull-right input-sm" data-toggle="modal" data-target=".send-Order"><i class='fa fa-check'></i>&nbsp; Send</button>
						</div>
					</div>
				</div><!--footer end-->
						

			</div><!--order list panel end-->
			
			<div class='row'>
				<div class='col-sm-12'>
					<?php if(isset($order_sent)){ ?>
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Well done!</strong> Orders successfully sent.
						</div>
					<?php } ?>
				</div>
			</div><!--aler row end-->
			
		</div><!-- col2 end -->
		
	
		
	</div><!--row end-->
	
	
	
	
	
</div><!--container end-->



<!--send order modal-->
<div class="modal fade send-Order" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
       
	   <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-primary" id="exampleModalLabel"><i class='fa fa-send'></i>&nbsp;Sending Option</h4>
	   </div>
	   
	   <div class="modal-body">
		  <div class="form-group">
			<label class='text-success'>Subject:</label>
			<input class="form-control required" id='orderSubject' name='orderSubject' required />
		  </div>
		  <div class="form-group">
            <label for="message-text" class="control-label">Message:</label>
            <textarea class="form-control" name='orderMessage' placeholder='Optional message, remarks or notes.'></textarea>
          </div>
		  <div class="form-group">
			<label class='text-success'>Where do you want to send this order?</label>
			<select class="form-control input-sm required" name='orderRecipient' required >
				<select class="form-control " name='area' id="branch" required>
					<option value="" disabled selected><span style='color:#777'>Select location</span></option></option>
					<option value="Warehouse">Warehouse</option>
					<option value="Main Office">Main Office</option>
					<option value="Kier Enterprises Irosin">Kier Enterprises Irosin</option>
					<option value="Kier Enterprises Gubat">Kier Enterprises Gubat</option>
					<option value="Kier Enterprises Casiguran">Kier Enterprises Casiguran</option>
				  </select>
			</select>
		  </div>
		  <div class="form-group">
			<label class='text-success'>Where do you want your orders to be deliverd?</label>
			<select class="form-control input-sm required" name='orderReceiver' required>
				<select class="form-control " name='area' id="branch" required>
					<option value="" disabled selected><span style='color:#777'>Select location</span></option></option>
					<option value="Warehouse">Warehouse</option>
					<option value="Main Office">Main Office</option>
					<option value="Kier Enterprises Irosin">Kier Enterprises Irosin</option>
					<option value="Kier Enterprises Gubat">Kier Enterprises Gubat</option>
					<option value="Kier Enterprises Casiguran">Kier Enterprises Casiguran</option>
				  </select>
			</select>
		  </div>
		   <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			<button type="submit" class="btn btn-primary"><i class='fa fa-send'></i>&nbsp;Send</button>
		  </div>
		
	   </div>
	   

	   
	</div>
  </div>
</div><!--send order modal end-->

</form><!--send order form end-->	   
	
<nav class="navbar navbar-default navbar-fixed-bottom">
  <div class="container">
    <ol class="breadcrumb">
	  <li><a href="#"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</a></li>
	  <li class="active">Create Order</li>
	</ol>
  </div>
</nav>
	
	<script>
		function addRow(tableID, sku, name, desc, id){
			var table = document.getElementById(tableID);
			
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
	
			//checkbox
			var cell1  = row.insertCell(0);
			var element1 = document.createElement("input");
			element1.type = "checkbox";
			element1.name = "checkbox[]";
			cell1.appendChild(element1);
			
			//SKU
			var cell2 = row.insertCell(1);
			cell2.innerHTML = sku;
			
			//name
			var cell3 = row.insertCell(2);
			cell3.innerHTML = name;
			
			//description
			var cell4 = row.insertCell(3);
			cell4.innerHTML = desc;
		
			//box
			var cell5 = row.insertCell(4);
			var element5 = document.createElement("input");
				element5.type = "number";
				element5.name = "box[]";
				element5.placeholder = 0;
				element5.step = 1;
				element5.min = 0;
				element5.className = "form-control short input-sm"
			cell5.appendChild(element5);
			
			
			// pack
			var cell6 = row.insertCell(5);
			var element6 = document.createElement("input");
				element6.type = "number";
				element6.name = "pack[]";
				element6.placeholder = 0;
				element6.step = 1;
				element6.min = 0;
				element6.className = "form-control short input-sm"
			cell6.appendChild(element6);
			
			// piece
			var cell7 = row.insertCell(6);
			var element7 = document.createElement("input");
			element7.type = "number";
			element7.name = "piece[]";
				element7.placeholder = 0;
				element7.step = 1;
				element7.min = 0;
				element7.className = "form-control short input-sm"
			cell7.appendChild(element7);
			
			//id
			var cell8 = row.insertCell(7);
			var element8 = document.createElement("input");
			element8.type = "hidden";
			element8.name = "id[]";
			element8.value = id;
			cell8.appendChild(element8);
		}
		
		function deleteRow(tableID){
			try{
				var table = document.getElementById(tableID);
				var rowCount = table.rows.length;
				
					for(var i=0; i<rowCount; i++){
						var row = table.rows[i];
						var chkbox = row.cells[0].childNodes[0];
													
						if(null != chkbox && true == chkbox.checked){
							table.deleteRow(i);
							rowCount--;
							i++;
							
						}
					}
				}catch(e){
					alert(e);
				}
		}
	</script>