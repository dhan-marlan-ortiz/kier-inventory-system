<?php
	foreach ($request as $request_item):
		$subject = $request_item['orderSubject'];
		$recipient = $request_item['orderRecipient'];
		$date = $request_item['request_date'];
		$location = $request_item['deliveryLocation'];
		$orderNo = $request_item['request_id'];
	endforeach; 
?>
<body onload="window.print();">
<div class='panel'>
	<div class='page-header'>
		<div class='row'>
			<div class='col-sm-6'>
				<h1 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i>&nbsp;Request Form</h1>	
			</div>
			<div class='col-sm-6'>
				<dl class="dl-horizontal pull-right">
					<dt>To :</dt><dd><?php echo $recipient; ?></dd>
					<dt>Subject :</dt><dd><?php echo $subject; ?></dd>
					<dt>Date requested : </dt> <dd> <?php echo $date; ?> </dd>
					<dt>Order No. : </dt><dd> <?php echo $orderNo; ?> </dd>
					<dt>Send order to : </dt><dd><?php echo $location; ?></dd>
				</dl>
			</div>
		</div>		
	</div>
	
	<div class='panel panel-default'>
		<div class='panel-heading'>
			<h4>Products</h4>
		</div>
		<div class='panel-body'>
			
				<table class='table table-condensed table-striped table-hover'>
					<thead>
						<tr>
							<th><span class='text-success'>#</span></th>
							<th><span class='text-success'>Type</span></th>
							<th><span class='text-success'>SKU</span></th>
							<th><span class='text-success'>Name</span></th>
							<th><span class='text-success'>Description</span></th>
							<th><span class='text-success'>Box</span></th>
							<th><span class='text-success'>Pack</span></th>
							<th><span class='text-success'>Piece</span></th>
							<th><span class='text-success'>Status</span></th>
							<th><span class='text-success'>Note</span></th>
						</tr>
					</thead>
					
					<tbody>
					 <?php if(isset($orderedList)){ ?>
						<?php $line_number = 1; 
						foreach ($orderedList as $request_item): ?>
							<tr>
								<td><?php echo $line_number; ?></td>								
								<td><?php echo $request_item['product_type']; ?> </td>	
								<td><?php echo $request_item['sku']; ?> </td>	
								<td><?php echo $request_item['product_name']; ?> </td>	
								<td><?php echo $request_item['product_description']; ?> </td>	
								<td><?php echo $request_item['box']; ?> </td>	
								<td><?php echo $request_item['pack']; ?> </td>	
								<td><?php echo $request_item['piece']; ?> </td>	
								<td><?php echo $request_item['status']; ?> </td>	
								<td><?php echo $request_item['orderMessage']; ?> </td>	
							</tr>
						<?php $line_number++; 
						endforeach ?>
						
					<?php } ?>
					 </tbody>
					 
				</table>
			
		</div>
		<div class='panel-footer'>
			
		</div>
	</div>
	
</div>
</body>

		
		
		
