<style> .short{ max-width: 80px; min-width: 80px; } </style>
<?php $currentArea = $this->session->userdata('area'); ?>

<div class='container' >
	<div class='page-header'>
		<div class='row'>
			<div class='col-sm-12'>
				<span class='text-success h3'><i class='fa fa-list-alt fa-fw'></i> Create Order </span>
				<div class="btn-group pull-right" role="group" aria-label="...">
				  <a  href="<?=site_url('suboffice/create_request')?>" class="btn btn-default active" data-toggle="tooltip" data-placement="bottom" title="Create New Order"><i class='fa fa-list-alt fa-fw'></i>&nbsp;Create</a>
				  <a href="<?=site_url("suboffice/inbox")?>" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Inbox"><i class='fa fa-inbox fa-fw'></i>&nbsp;Inbox</a>
				  <a href="<?=site_url("suboffice/request")?>" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Pending Orders"><i class='fa fa-send-o fa-fw'></i>&nbsp;Outbox</a>
				  <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Trash"><i class='fa fa-recycle fa-fw'></i>&nbsp;Trash</button>
				</div>
			</div>
		</div>
	</div>
</div><!--page header end-->

<div class="container">

	<form method="post" action="<?=site_url('suboffice/order_submit')?>" id="orderList">
		<div class='panel'>

			<div class="panel-heading">
				<h4>
					<span class='text-primary'><i class='fa fa-list-alt fa-fw'></i>&nbsp;Order list</span>
					<button class='btn btn-link pull-right' type='button' onclick="deleteRow('dataTable')"><i class='fa fa-minus-circle'></i> Remove</button>
					<button type="button" class="btn btn-link pull-right" data-toggle="modal" data-target=".addProductModal"><i class='fa fa-plus-circle'></i> Add Product</button>
				</h4>
			</div>

			<div class='panel-body'>
				<div class='table table-responsive' style='max-height: 300px; overflow: auto'>
					<table  class='table table-condensed table-hover' id="dataTable">
						<tr>
							<th></th>
							<th><span class='text-success'>SKU</span></th>
							<th><span class='text-success'>Name</span></th>
							<th><span class='text-success'>Description</span></th>
							<th><span class='text-success'>Box</span></th>
							<th><span class='text-success'>Pack</span></th>
							<th><span class='text-success'>Piece</span></th>
							<th></th>
						</tr>
					</table>
				</div>
			</div>

		</div><!--order list panel end-->

		<div class='pull-right'>
			<a type='button' class='btn btn-default' href="<?=site_url("suboffice/create_request")?>">&nbsp;Clear&nbsp;</a>
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".send-Order">&nbsp;Send&nbsp;</button>
		</div>




		<div class='row'>
			<div class='col-sm-12'>
				<?php if(isset($order_sent)){ ?>
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <strong>Well done!</strong> Orders successfully sent.
					</div>
				<?php } ?>
			</div>
		</div><!--aler row end-->

</div><!--container end-->



<!--send order modal-->
<div class="modal fade send-Order" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

	   <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-primary" id="exampleModalLabel"><i class='fa fa-send'></i>&nbsp;Sending Option</h4>
	   </div>

	   <div class="modal-body">
		  <div class="form-group">
			<label class='text-success'>Subject:</label>
			<select class="form-control " name='orderSubject' id="orderSubject" required>
				<option value="" disabled selected><span style='color:#777'>Select Request Type</span></option></option>
				<option value="Warehouse to Branch Request">Warehouse to Branch Request</option>
				<option value="Branch to Branch Request">Branch to Branch Request </option>
			</select>
		</div>
		  <div class="form-group">
			<label class='text-success'>Where do you want to send this order?</label>

				<select class="form-control " name='orderRecipient' id="branch" required>
					<option value="" disabled selected><span style='color:#777'>Select location</span></option>
					<?php if($currentArea != 'Warehouse'){ echo "<option value='Warehouse'>Warehouse</option>"; } ?>
					<?php if($currentArea != 'Kier Enterprises Irosin'){ echo "<option value='Kier Enterprises Irosin'>Kier Enterprises Irosin</option>";} ?>
					<?php if($currentArea != 'Kier Enterprises Gubat'){ echo "<option value='Kier Enterprises Gubat'>Kier Enterprises Gubat</option>"; } ?>
					<?php if($currentArea != 'Kier Enterprises Casiguran'){ echo "<option value='Kier Enterprises Casiguran'>Kier Enterprises Casiguran</option>"; } ?>
				</select>

		  </div>

		  <div class="form-group">
			<label class='text-success'>Where do you want your orders to be delivered?</label>

				<select class="form-control " name='orderReceiver' id="branch" required>
					<option value="<?php echo $this->session->userdata('area'); ?>"><span style='color:#777'><?php echo $this->session->userdata('area'); ?></span></option></option>
					<?php if($currentArea != 'Warehouse'){ echo "<option value='Warehouse'>Warehouse</option>"; } ?>
					<?php if($currentArea != 'Kier Enterprises Irosin'){ echo "<option value='Kier Enterprises Irosin'>Kier Enterprises Irosin</option>";} ?>
					<?php if($currentArea != 'Kier Enterprises Gubat'){ echo "<option value='Kier Enterprises Gubat'>Kier Enterprises Gubat</option>"; } ?>
					<?php if($currentArea != 'Kier Enterprises Casiguran'){ echo "<option value='Kier Enterprises Casiguran'>Kier Enterprises Casiguran</option>"; } ?>
				</select>

		  </div>
		   <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			<button type="submit" class="btn btn-primary"><i class='fa fa-send'></i>&nbsp;Send</button>
			<input type='hidden' value="<?php echo uniqid(); ?>" name='orderId' />
		  </div>

	   </div>



	</div>
  </div>
</div><!--send order modal end-->



</form><!--send order form end-->
<br>
<br>
<br>
<br>
<nav class="navbar navbar-default navbar-fixed-bottom">
  <div class="container">
    <ol class="breadcrumb">
	  <li><a href="#"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</a></li>
	  <li class="active">Create Order</li>
	</ol>
  </div>
</nav>



<div class="modal fade addProductModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class='modal-header'>
				<div class='row'>

					<div class='col-sm-6'>
						<h4 class="modal-title"><i class='fa fa-shopping-cart'></i> Product</h4>
					</div>

					<div class='col-sm-6'>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search for...">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class='fa fa-search'></i> Search</button>
							</span>
						</div>
					</div>

				</div>
			</div>

			<div class='panel-body'>
				<div class='table table-responsive' style='max-height: 350px; overflow: auto'>
					<table class="table table-condensed table-hover table-striped" id='productTable'>

						<thead>
							<tr>
								<th><span class='text-success'>#</span></th>
								<th><span class='text-success'>Product</span></th>
								<th><span class='text-success'>SKU</span></th>
								<th><span class='text-success'>Category</span></th>
								<th><span class='text-success'>Box</span></th>
								<th><span class='text-success'>Pack</span></th>
								<th><span class='text-success'>Piece</span></th>
								<th><span class='text-success'>Order</span></th>
								<th><span class='text-success'>Details</span></th>
							</tr>
						</thead>

						<tbody><?php $line_number = 1;  $i=0; foreach ($product as $product_detail): ?>
							<form method='POST' action="<?=site_url("suboffice/")?>" role='form'>
								<tr>
									<td><?php echo $line_number; ?></td>
									<td><?php echo $product_detail['product_name']; ?> </td>
									<td><?php echo $product_detail['sku']; ?> </td>
									<td><?php echo $product_detail['product_type']; ?> </td>
									<td><?php echo $product_detail['box_stock']; ?> </td>
									<td><?php echo $product_detail['pack_stock']; ?> </td>
									<td><?php echo $product_detail['piece_stock']; ?> </td>

									<?php
										$sku = $product_detail['sku'];						$name = $product_detail['product_name'];
										$desc = $product_detail['product_description'];		$id = $product_detail['product_id'];
									?>

									<td><button type="button" class="btn btn-link" data-dismiss='modal' onclick="addRow('dataTable', '<?php echo $sku; ?>',  '<?php echo $name; ?>',  '<?php echo $desc; ?>', '<?php echo $id; ?>')">Add</button></td>
									<td><button type='button' class='btn btn-link' data-toggle="tooltip" data-placement="bottom" title="View details">View</button></td>
									<input type='hidden' name='id' value="<?php echo $product_detail['product_id']?>">
								</tr><?php $line_number++; $i++; endforeach ?>
							</form><!--product list form end-->
						</tbody>

					</table>
				</div> <!--div table end-->
			</div>

			<div class='modal-footer'>
				<button class='btn btn-default pull-right' data-dismiss='modal'>Close</button>
			</div>

		</div>
	</div>
</div>



	<script>

		function addRow(tableID, sku, name, desc, id){
			var table = document.getElementById(tableID);

			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);

			//checkbox
			var cell1  = row.insertCell(0);
			var element1 = document.createElement("input");
			element1.type = "checkbox";
			element1.name = "checkbox[]";
			cell1.appendChild(element1);

			//SKU
			var cell2 = row.insertCell(1);
			cell2.innerHTML = sku;

			//name
			var cell3 = row.insertCell(2);
			cell3.innerHTML = name;

			//description
			var cell4 = row.insertCell(3);
			cell4.innerHTML = desc;

			//box
			var cell5 = row.insertCell(4);
			var element5 = document.createElement("input");
				element5.type = "number";
				element5.name = "box[]";
				element5.placeholder = 0;
				element5.step = 1;
				element5.min = 0;
				element5.className = "form-control short input-sm"
			cell5.appendChild(element5);


			// pack
			var cell6 = row.insertCell(5);
			var element6 = document.createElement("input");
				element6.type = "number";
				element6.name = "pack[]";
				element6.placeholder = 0;
				element6.step = 1;
				element6.min = 0;
				element6.className = "form-control short input-sm"
			cell6.appendChild(element6);

			// piece
			var cell7 = row.insertCell(6);
			var element7 = document.createElement("input");
			element7.type = "number";
			element7.name = "piece[]";
				element7.placeholder = 0;
				element7.step = 1;
				element7.min = 0;
				element7.className = "form-control short input-sm"
			cell7.appendChild(element7);

			//id
			var cell8 = row.insertCell(7);
			var element8 = document.createElement("input");
			element8.type = "hidden";
			element8.name = "id[]";
			element8.value = id;
			cell8.appendChild(element8);
		}

		function deleteRow(tableID){
			try{
				var table = document.getElementById(tableID);
				var rowCount = table.rows.length;

					for(var i=0; i<rowCount; i++){
						var row = table.rows[i];
						var chkbox = row.cells[0].childNodes[0];

						if(null != chkbox && true == chkbox.checked){
							table.deleteRow(i);
							rowCount--;
							i++;

						}
					}
				}catch(e){
					alert(e);
				}
		}
	</script>
