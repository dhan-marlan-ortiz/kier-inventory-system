	<?php  $fname = $this->session->userdata('fname'); ?>
	<?php  $utype = $this->session->userdata('utype'); ?>
	<?php  $uid = $this->session->userdata('uid'); ?>

		<!-- header -->
		<nav class="navbar navbar-inverse box-shadow">
		  <div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="<?=site_url('login_controller/suboffice')?>"><span class='text-success'><strong>Kier Enterprises</strong></span></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li <?php if($active == "home"){ echo "class='active'";} ?> ><a href="<?=site_url('Login_controller/suboffice')?>"><strong <?php if($active != "home"){ echo "class='text-primary'";} ?> >Home</strong></a></li>
				<li <?php if($active == "inventory"){ echo "class='active'";} ?> ><a href="<?=site_url('suboffice/inventory')?>"><strong <?php if($active != "inventory"){ echo "class='text-primary'";} ?>>Inventory</strong></a></li>
				<li <?php if($active == "request"){ echo "class='active'";} ?> ><a href="<?=site_url('suboffice/inbox')?>"><strong <?php if($active != "request"){ echo "class='text-primary'";} ?>>Request</strong></a></li>

			  </ul>

			  <ul class="nav navbar-nav navbar-right">
				<li ><a class='btn btn-link' href="<?=site_url('Login_controller/logout')?>"><strong class='text-danger'><i class='fa fa-power-off fa-fw'></i> Logout</a></strong></li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
