

<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	if (isset ($_GET['wr'])) { echo "<script>"; echo "alert (\"You must login first!\");"; echo "</script>"; } 
	$this->session->sess_destroy();
?>

		</script>

		<div class='container '>
			<div class='row'>
				<div class='col-md-4 col-md-offset-4'>
					
						<div class='container-fluid'><br>
							<div class='page-header'>
								<p class='text-center h1 text-success'><strong>Kier Enterprises</strong></p>
								<p class='h2 text-center'><small>Warehouse Inventory System</small></p>
							</div>
							<form method="POST" action="<?=site_url('login_controller/login/submit')?>" class='form-horizontal' role='form'>
									<div class="form-group has-success has-feedback">
									  <input type="text" name="username" placeholder="Username" class="form-control input-lg" id="inputSuccess2" aria-describedby="inputSuccess2Status" required size='50'>
									  <span class="glyphicon glyphicon-user form-control-feedback" aria-hidden="true"></span>
									  <span id="inputSuccess2Status" class="sr-only">(success)</span>
									</div>
									<div class="form-group has-success has-feedback">
									  <input type="password" name="password" placeholder="Password" class="form-control input-lg" id="inputSuccess2" aria-describedby="inputSuccess2Status" required size='50'>
									  <span class="glyphicon glyphicon-lock form-control-feedback" aria-hidden="true"></span>
									  <span id="inputSuccess2Status" class="sr-only">(success)</span>
									</div>
									<div class='form-group'>
										<button type="submit"  class='btn btn-success btn-block btn-lg'> SIGN IN</button>
									</div>
							</form>
						</div>
					
					<div class='row'>
						<div class='container-fluid'>
							<div class='form-group'>
								<?php if (isset ($_GET['lf'])){echo "<div class='alert alert-danger text-center' role='alert'> Please check username or password <i class='fa fa-exclamation-circle fa-lg'></i></div>";} ?>
								<?php if (isset ($_GET['fa'])){echo "<div class='alert alert-danger text-center' role='alert'> Forbidden Access <i class='fa fa-exclamation-circle fa-lg'></i></div>";} ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
		</div>

