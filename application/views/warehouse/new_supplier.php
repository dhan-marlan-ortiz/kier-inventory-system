<!-- HEADER -->
<div class='container'>
	<br/>
	<div class='panel'>
		<ul class="nav nav-tabs" role="tablist">
			<li><span class=' text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span></li>
			<li class="pull-right <?php if(isset($registration)){ echo "active"; } ?>" role="presentation"><a href="#new" role="tab" data-toggle="tab"><i class='fa fa-plus'></i> New</a></li>
			<li class="pull-right <?php if(!isset($registration)){ echo "active"; } ?>" role="presentation" class="active"><a href="#existing" role="tab" data-toggle="tab"><i class='fa fa-list'></i> Suppliers</a></li>
		</ul>
	</div>
</div>

<!-- MAIN BODY -->
<div class='container'>
  <div class="tab-content">

    <div role="tabpanel" class="tab-pane <?php if(!isset($registration)){ echo "active"; } ?> " id="existing">
			<div class='row'>
				<div class='col-sm-8 col-sm-offset-2'>
					<div class='panel panel-default' id='users'>

						<div class='panel-heading'>
							<div class='row'>
			          <div class='col-sm-6'>
									<h4 class='text-primary'><strong>Existing Suppliers</strong></h4>
			          </div>
			          <div class='col-sm-6'>
									<div class='input-group'>
										<input class="search form-control" placeholder="Search" />
										<div class='input-group-btn'>
											<button class="sort btn btn-default" data-sort="name">Sort</button>
											<button class="btn btn-default" onclick="printContent('printThis');"><i class='fa fa-print'></i> Print</button>
											<a class="btn btn-default" href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-remove'></i> Close</a>
					          </div>
									</div>
			          </div>

			        </div>
						</div>

						<div class='panel-body'>
							<ul class='list list-unstyled list-group'>
							<?php
			          if(isset($suppliers)){
									$i=1;
			            foreach($suppliers as $s): ?>
										<li class='list-group-item'>

								      <div class='row' style='border: solid 1px white' >
												<div class='name col-sm-10 col-xs-10'>
													<address>
														<strong><?php echo $s['name']; ?></strong><br>
														<?php echo $s['address']; ?><br>
														<?php echo $s['telephone']; ?><br>
													</address>
			                  </div>

			                  <div class='telephone col-sm-2 col-xs-2'>
													<ul class='list-unstyled'>
														<li><a data-toggle="modal" data-target=".supplierEdit<?php echo $i; ?>"><i class='fa fa-edit'></i> Edit</a></li>
														<li><a href="<?php echo site_url('warehouse/suppliers/'.$s['id']) ?>"><i class='fa fa-shopping-cart'></i> Products</a></li>
													</ul>
				                </div>
											</div>

										</li>

										<div class="modal fade supplierEdit<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
										  <div class="modal-dialog modal-md">
										    <div class="modal-content">

													<form method='post' action='<?=site_url('warehouse/new_supplier')?>'>
														<input type='hidden' name='editId' value="<?php echo $s['id']; ?>">
														<div class='modal-header'>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 									<h4 class="modal-title"><?php echo $s['name']; ?></h4>
														</div>
														<div class='modal-body'>
																<div class="form-group">
															    <label>Supplier</label>
															    <input name='editName' type="text" class="form-control" value="<?php echo $s['name']; ?>">
															  </div>
																<div class="form-group">
															    <label>Address</label>
																	<textarea name='editAddress' type="text" class="form-control"><?php echo $s['address']; ?></textarea>
															  </div>
																<div class="form-group">
															    <label>Telephone</label>
															    <input name='editTelephone' type="text" class="form-control" value="<?php echo $s['telephone']; ?>">
															  </div>
														</div>
														<div class='modal-footer'>
															<div class='row'>
																	<div class='col-sm-6'>
																		<button class='btn btn-default btn-block' data-dismiss='modal'>Close</button>
																	</div>
																	<div class='col-sm-6'>
																		<button type='submit' name='edit' class='btn btn-primary btn-block'>Save</button>
																	</div>
															</div>
														</div>
													</form>

										    </div>
										  </div>
										</div>
							<?php
								$i++;
								endforeach; } ?>
							</ul>



						</div><!-- end panel body -->
					</div><!-- -->
				</div>
		  </div>
		</div>

    <div role="tabpanel" class="tab-pane <?php if(isset($registration)){ echo "active"; } ?>" id="new">
			<div class='row'>
			  <div class='col-sm-4 col-sm-offset-4'>
					<form method='POST' action="<?=site_url('warehouse/new_supplier')?>" >

		        <div class='panel panel-default'>

		          <div class='panel-heading'>
		            <h4 class='text-primary'><strong>Add New Supplier</strong></h4>
		          </div>

		          <div class='panel-body'>
		            <div class="form-group">
		              <label>Supplier Name</label>
								  <input type="text" class="form-control" name='name' required <?php if(isset($registration)){ if($registration == 0){ echo "value=".$supplierInfo['name'].""; } }?> />
		            </div>

		            <div class="form-group">
		              <label>Address</label>
		              <textarea class="form-control" name='address'><?php if(isset($registration)){ if($registration == 0){ echo $supplierInfo['address']; } }?></textarea>
		            </div>

		            <div class="form-group">
		              <label>Telephone No.</label>
		              <input type="text" class="form-control" name='telephone' <?php if(isset($registration)){ if($registration == 0){ echo "value=".$supplierInfo['telephone'].""; } } ?> />
		            </div>

		            <div class="form-group">
									<div class='row'>
			              <div class="col-sm-6">
			                <a href="<?=site_url('warehouse/inventory')?>" name='register' class="btn btn-default btn-block"><i class='fa fa-remove'></i> Close</a>
			              </div>
			              <div class="col-sm-6">
			                <button type="submit" name='register' class="btn btn-success btn-block"><i class='fa fa-save'></i> Save</button>
			              </div>
									</div>
		            </div>

		          </div>
		        </div>
		      </form>

		      <?php if(isset($registration)){
		        if($registration == 1){ ?>
		        <div class="alert alert-success" role="alert">
		          <strong> <?php echo $supplierInfo['name']; ?> </strong>
		          has been successfully added to record.
		        </div>
		      <?php } else if($registration == 0){ ?>
		        <div class="alert alert-danger" role="alert">
		          New supplier registration failed.
		        </div>
		      <?php } } ?>

					<?PHP if(isset($update)){
						if($update == 1){
							echo "<div class='alert alert-success' role='alert'>Record Updated.</div>";
						}
					} ?>

        </div>
		  </div>
	  </div>

  </div> <!-- END TAB CONTENT-->
</div><!-- END CONTAINER -->

<!-- MAIN BODY -->
<div class='container'>
  <div class='row'>
    <div class='col-sm-4'>



		</div><!-- END COL 1 -->

		<div class='col-sm-8'>

		</div>

<!-- FOOTER -->
<div class="container">
	<ol class="breadcrumb pull-right">
    <li>
      <a href="<?=site_url('warehouse/inventory')?>" >
        <i class='fa fa-cube fa-fw'></i>&nbsp;Inventory
      </a>
    </li>
		<li class="active"><i class='fa fa-truck fa-fw'></i>&nbsp;Supplier</li>
	</ol>
</div>







<script>
		var options = {
		  valueNames: [ 'name']
		};

		var userList = new List('users', options);


</script>


<div class='container-fluid hidden' id='printThis'>
	<div class='page-header'>
		<h1>Suppliers</h1>
	</div>
	<table class='table table-condensed'>
		<thead><tr><th>#</th><th>Suppliers</th><th>Address</th><th>Telephone</th></tr></thead>
		<tbody>
			<?php
			if(isset($suppliers)){
				$i=1; foreach($suppliers as $s): ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $s['name']; ?></td>
						<td><?php echo $s['address']; ?></td>
						<td><?php echo $s['telephone']; ?></td>
					</tr>
			<?php $i++; endforeach; } ?>
		</tbody>
	</table>
</div>
