<div class='container'>
	<div class='page-header'>
		<span class=' text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
	</div>
</div>


<div class='container'>
	<div class='panel panel-default'>
		<form method='POST' action="<?=site_url('warehouse/edit_product_submit')?>" role='form'> <input type='hidden' name='product_id' value="<?php echo $product['product_id']; ?>" >
			<input type='hidden' name='supplier' value="<?php echo $product['id']; ?>" />
			<!-- HEADER -->
			<div class='panel-heading'>
				<h2 class='text-primary'><i class='fa fa-bookmark-o'></i>&nbsp;Product Information</h2>
				<div class='row'>
					<div class='col-sm-12'>
						<address>
							<strong><?php echo $product['name']; ?></strong>
							<br/><?php echo $product['address']; ?>
						</address>
					</div>
				</div>
			</div>

			<!-- BODY -->
			<div class='panel-body'>

				<div class='row'>
					<div class='col-sm-6'>	<!-- DETAILS -->
						<div class="collapse in collapseDetails">
							<table class='table'>
								<thead>
									<tr>
										<th class='text-primary h4'>Product Details</th>
										<th>
											<a class="btn btn-link pull-right" role="button" data-toggle="collapse" href=".collapseDetails" aria-expanded="false" aria-controls="collapseExample">
												<i class='fa fa-edit fa-fw'>&nbsp;</i>Edit
											</a>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr><th>Product Name</th><td><?php echo $product['product_name']; ?></td></tr>
									<tr><th>Category</th><td><?php echo $product['product_type']; ?></td></tr>
									<tr><th>Manufacturer</th><td><?php echo $product['manufacturer']; ?></td></tr>
								</tbody>
							</table>
						</div> <!-- collapese end -->

						<div class="collapse collapseDetails">
							<table class='table'>
								<thead>
									<tr>
										<th class='text-primary h4'>Edit Product Details</th>
										<th class='pull-right'>
											<button type='submit' class="btn btn-link"> <i class='fa fa-save fa-fw'>&nbsp;</i>Save all </button>
											<button class="btn btn-link" type='button' role="button" data-toggle="collapse" onclick="revert_details()" href=".collapseDetails" aria-expanded="false" aria-controls="collapseExample">
												<i class='fa fa-remove fa-fw'>&nbsp;</i>Cancel
											</button>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr><th>Product Name</th><td><input type='text' value="<?php echo $product['product_name']; ?>" id='product_name' name='product_name' class='form-control' required></td></tr>
									<tr><th>Category</th><td><input type='text' value="<?php echo $product['product_type']; ?>" id='product_type' name='product_type' class='form-control' required></td></tr>
									<tr><th>Manufacturer</th><td><input type='text' value="<?php echo $product['manufacturer']; ?>" id='manufacturer' name='manufacturer' class='form-control'></td></tr>
								</tbody>
							</table>
							<script>
								function revert_details(){
									document.getElementById("product_name").value = "<?php echo $product['product_name']; ?>";
									document.getElementById("product_type").value = "<?php echo $product['product_type']; ?>";
									document.getElementById("manufacturer").value = "<?php echo $product['manufacturer']; ?>";
								}
							</script>
						</div> <!-- collapese end -->
					</div><!-- END COL1 -->

					<div class='col-sm-6'><!-- VARIANT -->
						<div class="collapse in collapseVariants">
							<table class='table'>
								<thead>
									<tr>
										<th class='text-primary h4'>Product Variant</th>
										<th>
											<a class="btn btn-link pull-right" role="button" data-toggle="collapse" href=".collapseVariants">
												<i class='fa fa-edit fa-fw'>&nbsp;</i>Edit
											</a>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr><th>SKU</th><td><?php echo $product['sku']; ?></td></tr>
									<tr><th>Description</th><td><?php echo $product['product_description']; ?></td></tr>
								</tbody>
							</table>
						</div> <!-- collapese end -->

						<div class="collapse collapseVariants">
							<table class='table'>
								<thead>
									<tr>
										<th class='text-primary h4'>Edit Product Variant</th>
										<th class='pull-right'>
											<button type='submit' class="btn btn-link"> <i class='fa fa-save fa-fw'>&nbsp;</i>Save all </button>
											<button class="btn btn-link" type='button' role="button" data-toggle="collapse" onclick="revert_variants()" href=".collapseVariants">
												<i class='fa fa-remove fa-fw'>&nbsp;</i>Cancel
											</button>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr><th>Product Name</th><td><input type='text' value="<?php echo $product['sku']; ?>" id='sku' name='sku' class='form-control' required></td></tr>
									<tr><th>Category</th><td><textarea id='product_description' name='product_description' class='form-control'><?php echo $product['product_description']; ?></textarea></td></tr>
								</tbody>
							</table>
							<script>
								function revert_variants(){
									document.getElementById("sku").value = "<?php echo $product['sku']; ?>";
									document.getElementById("product_description").value = "<?php echo $product['product_description']; ?>";
									document.getElementById("supplier").value = "<?php echo $product['supplier']; ?>";
								}
							</script>
						</div> <!-- collapese end -->
					</div><!-- END COL2 -->

				</div><!-- END ROW -->
			</div><!-- PANEL BODY -->

			<div class='panel-body'>
				<div class='row'>
				<div class='col-sm-6'>
					<!-- WATCH LEVEL -->
					<div class="collapse in collapseWatch">
						<table class='table'>
							<thead>
								<tr>
									<th class='text-primary h4'>Watch Level</th>
									<th>
										<a class="btn btn-link pull-right" role="button" data-toggle="collapse" href=".collapseWatch">
											<i class='fa fa-edit fa-fw'>&nbsp;</i>Edit
										</a>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr><th>Box</th><td><?php echo $product['box_watch']; ?></td></tr>
								<tr><th>Pack</th><td><?php echo $product['pack_watch']; ?></td></tr>
								<tr><th>Piece</th><td><?php echo $product['pack_watch']; ?></td></tr>
							</tbody>
						</table>
					</div>
					<div class="collapse collapseWatch">
						<table class='table'>
							<thead>
								<tr>
									<th class='text-primary h4'>Edit Product Watch</th>
									<th class='pull-right'>
										<button type='submit' class="btn btn-link"> <i class='fa fa-save fa-fw'>&nbsp;</i>Save all </button>
										<button class="btn btn-link" type='button' role="button" data-toggle="collapse" onclick="revert_watch" href=".collapseWatch">
											<i class='fa fa-remove fa-fw'>&nbsp;</i>Cancel
										</button>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr><th>New Box</th><td><input type='number' value="<?php echo $product['box_watch']; ?>" id='box_watch' name='box_watch' class='form-control' ></td></tr>
								<tr><th>New Pack</th><td><input type='number' value="<?php echo $product['pack_watch']; ?>" id='pack_watch' name='pack_watch' class='form-control' ></td></tr>
								<tr><th>New Piece</th><td><input type='number' value="<?php echo $product['piece_watch']; ?>" id='piece_watch' name='piece_watch' class='form-control' ></td></tr>
							</tbody>
						</table>
						<script>
							function revert_watch(){
								document.getElementById("box_watch").value = "<?php echo $product['box_watch']; ?>";
								document.getElementById("pack_watch").value = "<?php echo $product['pack_watch']; ?>";
								document.getElementById("piece_watch").value = "<?php echo $product['piece_watch']; ?>";
							}
						</script>
					</div>
				</div>
				<div class='col-sm-6'>
					<div class="collapse in collapseCost">
						<table class='table'>
							<thead>
								<tr>
									<th class='text-primary h4'>Buying Cost</th>
									<th>
										<a class="btn btn-link pull-right" role="button" data-toggle="collapse" href=".collapseCost">
											<i class='fa fa-edit fa-fw'>&nbsp;</i>Edit
										</a>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr><th>Box</th><td><?php echo $product['box_cost']; ?></td></tr>
								<tr><th>Pack</th><td><?php echo $product['pack_cost']; ?></td></tr>
								<tr><th>Piece</th><td><?php echo $product['piece_cost']; ?></td></tr>
							</tbody>
						</table>
					</div>
					<div class="collapse collapseCost">
						<table class='table'>
							<thead>
								<tr>
									<th class='text-primary h4'>Edit Buying Cost</th>
									<th class='pull-right'>
										<button type='submit' class="btn btn-link"> <i class='fa fa-save fa-fw'>&nbsp;</i>Save all </button>
										<button class="btn btn-link" type='button' role="button" data-toggle="collapse" onclick="revert_cost" href=".collapseCost">
											<i class='fa fa-remove fa-fw'>&nbsp;</i>Cancel
										</button>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr><th>New Box</th><td><input type='number' min=0 max=9999999 step=0.01 value="<?php echo $product['box_cost']; ?>" id='box_cost' name='box_cost' class='form-control'></td></tr>
								<tr><th>New Pack</th><td><input type='number' min=0 max=9999999 step=0.01 value="<?php echo $product['pack_cost']; ?>" id='pack_cost' name='pack_cost' class='form-control' ></td></tr>
								<tr><th>New Piece</th><td><input type='number' min=0 max=9999999 step=0.01 value="<?php echo $product['piece_cost']; ?>" id='piece_cost' name='piece_cost' class='form-control' ></td></tr>
							</tbody>
						</table>
						<script>
						<script>
								function revert_cost(){
									document.getElementById("box_cost").value = "<?php echo $product['box_cost']; ?>";
									document.getElementById("pack_cost").value = "<?php echo $product['pack_cost']; ?>";
									document.getElementById("piece_cost").value = "<?php echo $product['piece_cost']; ?>";
								}
						</script>
						</script>
					</div>

				</div>
			</div>
			</div>

			<div class='panel-body'>
				<div class='row'>
					<div class='col-sm-6'>
						<div class="collapse in collapseStock">
							<table class='table'>
								<thead>
									<tr>
										<th class='text-primary h4' colspan="2">Available Stocks</th>
									</tr>
								</thead>
								<tbody>
									<tr><th>Box</th><td><?php echo $product['box_stock']; ?></td></tr>
									<tr><th>Pack</th><td><?php echo $product['pack_stock']; ?></td></tr>
									<tr><th>Piece</th><td><?php echo $product['piece_stock']; ?></td></tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class='col-sm-6'>
						<div class="collapse in collapsePrice">
							<table class='table'>
								<thead>
									<tr>
										<th class='text-primary h4'>Selling Price</th>
										<th>
											<a class="btn btn-link pull-right" role="button" data-toggle="collapse" href=".collapsePrice">
												<i class='fa fa-edit fa-fw'>&nbsp;</i>Edit
											</a>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr><th>Box</th><td><?php echo $product['box_price']; ?></td></tr>
									<tr><th>Pack</th><td><?php echo $product['pack_price']; ?></td></tr>
									<tr><th>Piece</th><td><?php echo $product['piece_price']; ?></td></tr>
								</tbody>
							</table>
						</div>
						<div class="collapse collapsePrice">
							<table class='table'>
								<thead>
									<tr>
										<th class='text-primary h4'>Edit Selling Price</th>
										<th class='pull-right'>
											<button type='submit' class="btn btn-link"> <i class='fa fa-save fa-fw'>&nbsp;</i>Save all </button>
											<button class="btn btn-link" type='button' role="button" data-toggle="collapse" onclick="revert_price" href=".collapsePrice">
												<i class='fa fa-remove fa-fw'>&nbsp;</i>Cancel
											</button>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr><th>New Box</th><td><input type='number' min=0 max=9999999 step=0.01 value="<?php echo $product['box_price']; ?>" id='box_price' name='box_price' class='form-control' ></td></tr>
									<tr><th>New Pack</th><td><input type='number' min=0 max=9999999 step=0.01 value="<?php echo $product['pack_price']; ?>" id='pack_price' name='pack_price' class='form-control' ></td></tr>
									<tr><th>New Piece</th><td><input type='number' min=0 max=9999999 step=0.01 value="<?php echo $product['piece_price']; ?>" id='piece_price' name='piece_price' class='form-control' ></td></tr>
								</tbody>
							</table>
							<script>
								function revert_price(){
									document.getElementById("box_price").value = "<?php echo $product['box_price']; ?>";
									document.getElementById("pack_price").value = "<?php echo $product['pack_price']; ?>";
									document.getElementById("piece_price").value = "<?php echo $product['piece_price']; ?>";
								}
							</script>
						</div>

					</div>
				</div>
			</div>

			<div class='panel-footer'>
				<div class='row'>
						<div class='col-sm-2 col-sm-offset-8'>
							<button type='button' name='print' onclick="printContent('printThis');" class='btn btn-default btn-block'><i class='fa fa-download'></i>&nbsp;Download&nbsp;/&nbsp;<i class='fa fa-print'></i>&nbsp;Print</button>&nbsp;
						</div>
						<div class='col-sm-2'>
							<a type='button' href="<?=site_url('warehouse/inventory')?>" class='btn btn-default btn-block'><i class='fa fa-remove'></i> Close </a>
						</div>
				</div>
			</div>


		</form>
	</div><!-- END PANEL DEFAULT -->
</div><!-- END CONTAINER -->




<!--FOOTER-->
<div class="container" >
	<ol class="breadcrumb pull-right">
	  <li><a href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</a></li>
	  <li class="active"><i class='fa fa-shopping-cart fa-fw'></i>&nbsp; Product Information</li>
	</ol>
</div>

<div class='hidden' id='printThis'>
	<div class='container-fluid'>
		<div class='row'>
			<div class='col-sm-12'>

					<h2 class='text-primary'><i class='fa fa-bookmark-o'></i>&nbsp;Product Information</h2>
					<address>
						<strong><?php echo $product['name']; ?></strong>
						<br/><?php echo $product['address']; ?>
					</address>

			</div>
		</div>
		<div class='row'>
			<div class='col-sm-6 col-sm-offset-3'>
				<table class='table table-condensed'>
						<thead>
							<tr><th colspan='2'>&nbsp;</th></tr>
							<tr><th class='active' colspan='2'>Product Details</th></tr>
						</thead>
						<tbody>
							<tr><td>Product Name</td>	<td><?php echo $product['product_name']; ?></td></tr>
							<tr><td>Category</td>			<td><?php echo $product['product_type']; ?></td></tr>
							<tr><td>Manufacturer</td>	<td><?php echo $product['manufacturer']; ?></td></tr>
						</tbody>
						<thead>
							<tr><th colspan='2'>&nbsp;</th></tr>
							<tr><th class='active' colspan='2'>Product Variant</th></tr>
						</thead>
						<tbody>
							<tr><td>Description</td>							<td><?php echo $product['product_description']; ?></td></tr>
							<tr><td>Stock-keeping Unit (SKU)</td>	<td><?php echo $product['sku']; ?></td></tr>
						</tbody>
						<thead>
							<tr><th colspan='2'>&nbsp;</th></tr>
							<tr><th class='active' colspan='2'>Available Stock</th></tr>
						</thead>
						<tbody>
							<tr><td>Box</td><td><?php echo $product['box_stock']; ?></td></tr>
							<tr><td>Pack</td><td><?php echo $product['pack_stock']; ?></td></tr>
							<tr><td>Piece</td><td><?php echo $product['piece_stock']; ?></td></tr>
						</tbody>
						<thead>
							<tr><th colspan='2'>&nbsp;</th></tr>
							<tr><th class='active' colspan='2'>Buy Cost</th></tr>
						</thead>
						<tbody>
							<tr><td>Box</td><td><?php echo $product['box_cost']; ?></td></tr>
							<tr><td>Pack</td><td><?php echo $product['pack_cost']; ?></td></tr>
							<tr><td>Piece</td><td><?php echo $product['piece_cost']; ?></td></tr>
						</tbody>

						<thead>
							<tr><th colspan='2'>&nbsp;</th></tr>
							<tr><th class='active' colspan='2'>Selling Price</th></tr>
						</thead>
						<tbody>
							<tr><td>Box</td><td><?php echo $product['box_price']; ?></td></tr>
							<tr><td>Pack</td><td><?php echo $product['pack_price']; ?></td></tr>
							<tr><td>Piece</td><td><?php echo $product['piece_price']; ?></td></tr>
						</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
