<div class='container'>
	<div class='page-header'>
    <div class='row'>
    <div class="col-sm-6">
      <span class=' text-success h3'><i class='fa fa-cog fa-fw'></i> Activity Log </span>
    </div>

    <div class="col-sm-5">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"><i class='fa fa-search'></i> Search</button>
        </span>
			</div><!-- /input-group -->
		</div><!-- /.col-lg-6 -->
		<div class="col-sm-1">
			<button class='btn btn-default' onclick="printContent('printThis');"><i class='fa fa-print'></i> Print</button>
		</div>
	</div>
</div>
</div>

<div class='container'>
  <div style='max-height: 450px; overflow:auto' class='container-fluid'>
  <table class='table table-condensed table-striped'>
    <thead>
      <tr>
        <th>#</th><th>Date/Time</th><th>Activity</th><th>User</th><th>Description</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(isset($activities)){
        $i=1; foreach($activities as $a):
          $desc = $a['description'];
          $action = $a['action'];
          $description = "";
          if($action == "Login") $description = $a['firstname']." ".$a['lastname']." has login into ".$a['description'];
          else if($action == "Logout") $description = $a['firstname']." ".$a['lastname']." has logout from ".$a['description'];

      ?>
      <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $a['date'] ?></td>
        <td><?php echo $a['action'] ?></td>
        <td><?php echo $a['firstname']." ".$a['lastname'] ?></td>
        <td><?php echo $description; ?></td>
      </tr>

      <?php
        $i++; endforeach;
      }
      ?>

    </tbody>
  </table>
  </div>
</div>

<div class='hidden' id='printThis'>
	<h3><i class='fa fa-cog fa-fw'></i> Activity Log</h3>
	<hr>
	<table class='table table-condensed table-striped'>
    <thead>
      <tr>
        <th>#</th><th>Date/Time</th><th>Activity</th><th>User</th><th>Description</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(isset($activities)){
        $i=1; foreach($activities as $a):
          $desc = $a['description'];
          $action = $a['action'];
          $description = "";
          if($action == "Login") $description = $a['firstname']." ".$a['lastname']." has login into ".$a['description'];
          else if($action == "Logout") $description = $a['firstname']." ".$a['lastname']." has logout from ".$a['description'];

      ?>
      <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $a['date'] ?></td>
        <td><?php echo $a['action'] ?></td>
        <td><?php echo $a['firstname']." ".$a['lastname'] ?></td>
        <td><?php echo $description; ?></td>
      </tr>

      <?php
        $i++; endforeach;
      }
      ?>

    </tbody>
  </table>
</div>
