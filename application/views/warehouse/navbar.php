<?php  $fname = $this->session->userdata('fname'); ?>
<?php  $utype = $this->session->userdata('utype'); ?>
<?php  $uid = $this->session->userdata('uid'); ?>

<!-- header -->
<nav class="navbar navbar-default">
  <div class="container">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
	  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
	  <a class="navbar-brand" href="<?=site_url('login_controller/warehouse')?>"><span class='text-success'><span>Kier Enterprises</span></span></a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	  <ul class="nav navbar-nav">
		<li <?php if($active == "home"){ echo "class='active'";} ?> ><a href="<?=site_url('Login_controller/warehouse')?>"><span <?php if($active != "home"){ echo "class='text-info'";} ?> >Home</span></a></li>
		<li <?php if($active == "inventory"){ echo "class='active'";} ?> ><a href="<?=site_url('warehouse/inventory')?>" ><span <?php if($active != "inventory"){ echo "class='text-info'";} ?> >Inventory</span></a></li>
		<li <?php if($active == "request"){ echo "class='active'";} ?> ><a href="<?=site_url('warehouse_request/inbox')?>"><span <?php if($active != "request"){ echo "class='text-info'";} ?>>Request</span></a></li>
		<li <?php if($active == "report"){ echo "class='active'";} ?>><a href="<?=site_url('warehouse_report/landingPage')?>"><span <?php if($active != "report"){ echo "class='text-info'";} ?>>Report</span></a></li>

	  </ul>

	  <ul class="nav navbar-nav navbar-right">
			<li <?php if($active == "activity_log"){ echo "class='active'";} ?> ><a href="<?=site_url('activities/getActivities')?>"><span <?php if($active != "activity_log"){ echo "class='text-info'";} ?>>Activity Log</span></a></li>
		<li ><a class='btn btn-link' href="<?=site_url('Login_controller/logout')?>"><span class='text-danger'><i class='fa fa-power-off fa-fw'></i> Logout</a></span></li>
	  </ul>
	</div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
