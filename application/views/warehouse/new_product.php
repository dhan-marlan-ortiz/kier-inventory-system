<div class='container'>
	<div class='page-header'>
		<span class=' text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
	</div>
</div>

<!-- MAIN BODY -->
<form method='post' action="<?=site_url('warehouse/new_product')?>">
	<div class='container'>
		<div class='row'>
			<div class='col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4'>

					<div class='page-header'>
						<span class=' text-primary h2'>Select Supplier </span>
					</div>

					<div class='panel'>
						<textarea id='selectedSupplier' type="text" name='supplier' class="form-control " data-toggle="modal" data-target=".supplierList" placeholder='Click here to view list of suppliers' id='selectedSupplier' readonly required style='cursor: pointer;' /></textarea>
						<input type='hidden' name='id' id='supplierId' />
					</div>

					<div class='row'>
						<div class='col-sm-6 col-xs-6 col-md-6'>
							<a href="<?=site_url('warehouse/inventory')?>" class='btn btn-default btn-block'><i class='glyphicon glyphicon-chevron-left pull-left'></i>&nbsp;Back </a>
						</div>
						<div class='col-sm-6 col-xs-6 col-md-6'>
							<button class='btn btn-primary btn-block' type='submit' name='next'> Next&nbsp;<i class='glyphicon glyphicon-chevron-right pull-right'></i></button>
						</div>
					</div>

			</div>
		</div>
	</div>
</form>
<!-- END MAIN BODY -->
<br/>
<br/>
<!-- FOOTER -->
<div class='container'>
	<ol class="breadcrumb pull-right">
		<li><a href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</a></li>
		<li class="active"><i class='fa fa-th fa-fw'></i>&nbsp; Product Registration</li>
		<li class="active">Select Supplier</li>
	</ol>
</div>

<!-- SEARCH SUPPLIER MODAL -->
<div class="modal fade supplierList" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-md">
		<div class='panel panel-success'>
			<div id="users">

				<div class='panel-heading'>
					<div class='row'>
						<div class='col-sm-10'>
						  <input type="text" class="form-control col-sm-10 search" placeholder="Search Supplier">
						 </div>
						<div class='col-sm-2'>
							<button class="btn btn-default btn-block sort" data-sort='name' type="button"><i class='fa fa-sort-alpha-asc'></i>&nbsp;Sort</button>
						</div>
					</div>
				</div>

				<div class='panel-body'>
					<div class='table-responsive' style='max-height: 250px; overflow: auto'>
						<div class="list list-group">
							<?php if(isset($suppliers)){ foreach($suppliers as $supplier_data): ?>
								<a class='list-group-item' style='border:none;' onclick="addSupplier('<?php echo $supplier_data["name"]; ?>', '<?php echo $supplier_data["id"]; ?>');" data-dismiss="modal">
									<i class='fa fa-plus-square-o'></i>&nbsp;&nbsp;&nbsp;<span class="name text-primary"><?php echo $supplier_data['name']; ?></span>
								</a>
							<?php endforeach; } ?>
						</div>
					</div>
				</div>

				<div class='panel-footer'>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>

		</div><!--end users-->
	</div>
</div>
</div>





<script>


	function addSupplier(name, id){
		document.getElementById('selectedSupplier').innerHTML = name;
		document.getElementById('supplierId').value = id;
	}

	var options = {
	  valueNames: [ 'name']
	};

	var userList = new List('users', options);

	function hide(obj){
		var el = document.getElementById(obj);
		el.style.display = 'none';
	}

	function show(obj){
		var el = document.getElementById(obj);
		el.style.display = 'block';
	}
</script>
