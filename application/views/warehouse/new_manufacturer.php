<!-- HEADER -->
<div class='container'>
	<br/>
	<div class='panel'>
		<ul class="nav nav-tabs" role="tablist">
			<li><span class=' text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span></li>
			<li class="pull-right <?php if(isset($registration) OR validation_errors() == TRUE ){ echo "active"; } ?> " role="presentation"><a href="#new" role="tab" data-toggle="tab"><i class='fa fa-plus'></i> New</a></li>
			<li class="pull-right <?php if(!isset($registration) AND validation_errors() == FALSE ){ echo 'active'; } ?>" role="presentation" class="active"><a href="#existing" role="tab" data-toggle="tab"><i class='fa fa-list'></i> Manufacturers</a></li>
		</ul>
	</div>
</div>


<!-- MAIN BODY -->
<div class='container'>

	<!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?php if(!isset($registration) && validation_errors() == FALSE ){ echo 'active'; } ?>" id="existing">
			<div class='row'>
				<div class='col-sm-8 col-sm-offset-2'>
					<div class='panel panel-default' id='users'>

						<div class='panel-heading'>
							<div class='row'>
			          <div class='col-sm-4'>
									<h4 class='text-primary'><strong>Manufacturers</strong></h4>
								</div>
								<div class='col-sm-8'>
									<div class='input-group'>
										<input class="search form-control" placeholder="Search" />
										<div class='input-group-btn'>
											<button class="sort btn btn-default" data-sort="name"><i class='fa fa-sort-alpha-asc'></i> Sort</button>
											<button class="btn btn-default" onclick="printContent('printThis');"><i class='fa fa-print'></i> Print</button>
											<a class="btn btn-default" href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-remove'></i> Close</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class='panel-body'>

							<ul class='list list-unstyled list-group'>
							<?php

			          if(isset($manufacturers)){
									$i=1;
			            foreach($manufacturers as $m): ?>
										<li class='list-group-item'>

								      <div class='row' style='border: solid 1px white' >
												<div class='name col-sm-10 col-xs-10'>
													<address>
														<strong><?php echo $m['name']."<br>"; ?></strong>
														<?php if($m['address']){ echo $m['address']."<br>"; } ?>
														<?php if($m['telephone']){ echo $m['telephone']."<br>"; }?>
													</address>
			                  </div>

			                  <div class='telephone col-sm-2 col-xs-'>
													<a data-toggle="modal" data-target=".supplierEdit<?php echo $i; ?>"><i class='fa fa-edit'></i> Edit</a><br/>
													<a href="#"><i class='fa fa-shopping-cart'></i> Products</a><br/>
													<a href="#"><i class='fa fa-truck'></i> Suppliers</a>
			                  </div>
											</div>

										</li>

										<div class="modal fade supplierEdit<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
										  <div class="modal-dialog modal-md">
										    <div class="modal-content">

													<form method='post' action='<?=site_url('warehouse/new_manufacturer')?>'>
														<input type='hidden' name='editId' value="<?php echo $m['id']; ?>">
														<div class='modal-header'>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 									<h4 class="modal-title"><?php echo $m['name']; ?></h4>
														</div>
														<div class='modal-body'>
																<div class="form-group">
															    <label>Manufacturer</label>
															    <input name='editName' type="text" class="form-control" value="<?php echo $m['name']; ?>">
															  </div>
																<div class="form-group">
															    <label>Address</label>
																	<textarea name='editAddress' type="text" class="form-control"><?php echo $m['address']; ?></textarea>
															  </div>
																<div class="form-group">
															    <label>Telephone</label>
															    <input name='editTelephone' type="text" class="form-control" value="<?php echo $m['telephone']; ?>">
															  </div>
														</div>
														<div class='modal-footer'>
															<div class='row'>
																	<div class='col-sm-6'>
																		<button class='btn btn-default btn-block' data-dismiss='modal'>Close</button>
																	</div>
																	<div class='col-sm-6'>
																		<button type='submit' name='edit' class='btn btn-primary btn-block'>Save</button>
																	</div>
															</div>
														</div>
													</form>

										    </div>
										  </div>
										</div>
							<?php
								$i++;
								endforeach; } ?>
							</ul>

						</div><!-- end panel body -->
					</div><!-- -->
				</div>
		  </div>
		</div>

    <div role="tabpanel" class="tab-pane <?php if(isset($registration) OR validation_errors() == TRUE ){ echo "active"; } ?>" id="new">
			<div class='row'>
			<div class='col-sm-4 col-sm-offset-4'>

	      <form method='POST' action="<?=site_url('warehouse/new_manufacturer')?>" >
					<div class='panel panel-default'>

	          <div class='panel-heading'>
	            <h4 class='text-primary'><strong>Add New Manufacturer</strong></h4>
	          </div>

	          <div class='panel-body'>
	            <div class="form-group">
	              <label class="control-label">Manufacturer Name</label>
								<input type="text" class="form-control" name='name' value="<?php if(validation_errors()){ echo set_value('name'); } ?>" required  />
	            </div>

	            <div class="form-group">
	              <label class="control-label">Address</label>
								<textarea class="form-control" name='address'><?php if(validation_errors()){ echo set_value('address'); } ?></textarea>
	            </div>

	            <div class="form-group">
	              <label class="control-label">Telephone No.</label>
								<input type="text" class="form-control" name='telephone' value="<?php if(validation_errors()){ echo set_value('telephone'); } ?>" />
	            </div>

	            <div class="form-group">
								<div class=row>
		              <div class="col-sm-6">
		                <a href="<?=site_url('warehouse/inventory')?>" name='register' class="btn btn-default btn-block"><i class='fa fa-remove'></i> Close</a>
		              </div>
		              <div class="col-sm-6">
		                <button type="submit" name='register' class="btn btn-success btn-block"><i class='fa fa-save'></i> Save</button>
		              </div>
								</div>
	            </div>

	          </div>
	        </div>
	      </form>

				<?php if(isset($registration)){ ?>
	        <div class="alert alert-success" role="alert"><strong>Success! </strong> New manufacturer has been successfully added.</div>
	      <?php } ?>

	      <?php if(validation_errors()){
	        echo "<div class='alert alert-danger' role='alert'>".validation_errors()."</div>";
	      } ?>

			</div><!-- COL1 END -->
		</div>
	</div> <!-- end new tab-->

</div> <!-- END TAB CONTENT-->

</div><!-- END CONTAINER -->

<!-- FOOTER -->
<div class="container">
	<ol class="breadcrumb pull-right">
    <li>
      <a href="<?=site_url('warehouse/inventory')?>" >
        <i class='fa fa-cube fa-fw'></i>&nbsp;Inventory
      </a>
    </li>
		<li class="active"><i class='fa fa-building-o fa-fw'></i>&nbsp;Manufacturer</li>
	</ol>
</div>


<script>
		var options = {
		  valueNames: [ 'name']
		};

		var userList = new List('users', options);
</script>

<div class='container-fluid hidden' id='printThis'>
	<div class='page-header'>
		<h1>Manufacturers</h1>
	</div>
	<table class='table table-condensed'>
		<thead><tr><th>#</th><th>Manufacturer</th><th>Address</th><th>Telephone</th></tr></thead>
		<tbody>
			<?php
			if(isset($manufacturers)){
				$i=1; foreach($manufacturers as $m): ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $m['name']; ?></td>
						<td><?php echo $m['address']; ?></td>
						<td><?php echo $m['telephone']; ?></td>
					</tr>
			<?php $i++; endforeach; } ?>
		</tbody>
	</table>
</div>
