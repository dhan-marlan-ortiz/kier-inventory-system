<?php
	foreach($invoiceInfo as $invoiceData):
		$date = $invoiceData['date'];
		$invoice = $invoiceData['invoice_num'];
		$supplier = $invoiceData['supplier'];
	endforeach;
	foreach($supplierAddress as $address):
		$supplierAddress = $address['address'];
	endforeach;
?>

<div class='container panel panel-default'>

	<div class='page-header'>

			<h2 class='text-primary'><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Delivery Invoice</h2>
			<div class='row'>
				<div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'>
					<address>
					  <strong id="supplier"><?php echo $supplier; ?></strong><br>
					  <?php echo $address['address']; ?>
					</address>
				</div>

				<?php



?>
				<div class='col-xs-6 col-sm-8 col-md-8 col-lg-8'>
					<dl class="dl-horizontal pull-right">


							<dt>Invoice No. : </dt><dd><?php echo $invoice; ?></dd>
							<dt>Date : </dt><dd><?php echo $date; ?></dd>


					</dl>
				</div>
			</div>

	</div>


	<div class="panel panel-default">
	  <div class='panel-heading'>
		<h4>
			<span class='text-primary'>Products</span>
		</h4>
	  </div>
	  <div class="panel-body">
		<div class='table-responsive'>
			<table class='table'>
				<thead>
					<tr>
						<th>#</th><th>SKU</th><th>Product Name</th><th>Quantity</th><th>Unit</th><th>Unit Price</th><th>Discount</th><th>Subtotal</th>
					</tr>
				</thead>
				<tbody>
						<?php $i=1;
									$grandTotal = 0; $totalDiscount = 0;
						foreach($invoiceInfo as $invoiceData): ?>
							<tr>
								<td> <?php echo $i; ?></td>
								<td> <?php echo $invoiceData['invoice_sku']; ?></td>
								<td> <?php echo $invoiceData['product_name']; ?></td>
								<td> <?php echo number_format($invoiceData['quantity']); ?></td>
								<td> <?php echo $invoiceData['unit']; ?></td>
								<td> <?php echo number_format($invoiceData['price'], 2); ?></td>
								<td> <?php echo number_format($invoiceData['discount'], 2); ?></td>
								<td> <?php echo number_format($invoiceData['subtotal'], 2); ?></td>
							</tr>
						<?php $i++;
								 $totalDiscount = $totalDiscount + $invoiceData['discount'];
								$grandTotal = $grandTotal + $invoiceData['subtotal'];
								endforeach; ?>
				</tbody>
			</table>
		</div>
	  </div>
	  <div class='panel-footer'>
		<div class='row'>

			<div class='col-sm-2 text-primary'>
				Product Count
			</div>
			<div class='col-sm-2'>
				<strong  id='totalCount'><?php echo $i-1; ?> </strong>
			</div>

			<div class='col-sm-2 text-primary'>
				Total Discount
			</div>
			<div class='col-sm-2' >
				<strong id='totalDiscount'><?php echo $totalDiscount; ?> </strong>
			</div>

			<div class='col-sm-2 text-danger'>
				Grand Total
			</div>
			<div class='col-sm-2' >
				<strong id='totalCost'><?php echo $grandTotal; ?> </strong>
			</div>
		</div>
	</div>
	</div>

	<div class='panel'>
		<div class='row'>

			<div class='col-sm-2 '>
				<a class="btn btn-default btn-block" href="#" ><i class='fa fa-edit'></i>&nbsp;Modify</a>
			</div>
			<div class='col-sm-2 col-sm-offset-6'>
				<a class="btn btn-default btn-block" onclick="printContent('printThis');" ><i class='fa fa-print'></i>&nbsp;Print</a>
			</div>
			<div class='col-sm-2'>
				<a name='submit' href="<?=site_url('warehouse/delivery1')?>" class="btn btn-success btn-block"><i class='fa fa-check'></i>&nbsp;Ok</a>
			</div>

		</div>
	</div>
</div>

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
	  <li><a href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</a></li>
	  <li><a href="<?=site_url('warehouse/delivery1')?>"><i class='fa fa-truck fa-fw'></i>&nbsp; Delivery</a></li>
	  <li class="active"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp; Delivery Invoice</li>
	</ol>
</div>


<!-- PRINT -->
<div id='printThis' class='hidden'>
	<div class='page-header'>
			<h2 class='text-primary'><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Delivery Invoice <small class='pull-right'>#<?php echo $invoice; ?></small></h2>
			<div class='row'>
				<div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'>
					<address>
					  <strong id="supplier"><?php echo $supplier; ?></strong><br>
					  <?php echo $address['address']; ?>
					</address>
				</div>
				<div class='col-xs-6 col-sm-8 col-md-8 col-lg-8'>
					<span class='pull-right'><?php echo $date; ?></span>
				</div>
			</div>
	</div>

	<table class='table'>
		<thead>
			<tr>
				<th>#</th><th>SKU</th><th>Product</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Discount</th><th>Subtotal</th>
			</tr>
		</thead>
		<tbody>
				<?php
					$i=1;

					foreach($invoiceInfo as $invoiceData): ?>
					<tr>
						<td> <?php echo $i; ?></td>
						<td> <?php echo $invoiceData['invoice_sku']; ?></td>
						<td> <?php echo $invoiceData['product_name']; ?></td>
						<td> <?php echo number_format($invoiceData['quantity']); ?></td>
						<td> <?php echo $invoiceData['unit']; ?></td>
						<td> <?php echo number_format($invoiceData['price'], 2 ); ?></td>
						<td> <?php echo number_format($invoiceData['discount'], 2); ?></td>
						<td> <?php echo number_format($invoiceData['subtotal'], 2); ?></td>
					</tr>

				<?php
					$i++;

				endforeach; ?>
		</tbody>
	</table>

	<hr/>

	<div class='row'>
		<div class='col-sm-6 col-xs-6'>
			<table class='table table-condensed' >
				<thead><tr><th colspan='2'>Invoice Summary</th></tr></thead>
				<tbody>
					<tr><td>Product Count</td><td><?php echo $i-1; ?></td></tr>
					<tr><td>Total Discount</td><td><?php echo $totalDiscount; ?></td></tr>
					<tr><td>Grand Total</td><td><?php echo $grandTotal; ?></td></tr>
				</tbody>
			</table>

		</div>

	</div>

</div>
