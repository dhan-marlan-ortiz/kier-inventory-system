<div class='container'>
	<div class='page-header'>
		<span class=' text-success h3'><i class='fa fa-home fa-fw'></i> Home </span>
	</div>
</div>

<div class='container'>

		<div class="row">

		  <div class="col-sm-6 col-md-4">
		    <div class="thumbnail">
					<div class='panel'>
						<div class="caption">
							<i class="fa fa-user fa-pull-left fa-5x fa-border"></i>
			        <h3><strong><?php echo $this->session->userdata('fname')." ".$this->session->userdata('lname'); ?></strong></h3>
			        <p><?php echo $this->session->userdata('pos'); ?></p>
						</div>
					</div>
					<div class='panel'>
						<table class='table'>
							<tbody>
								<tr>
									<td><small>Employee ID</small></td>
									<td><strong><?php echo $this->session->userdata('uid'); ?></strong></td>
								</tr>
								<tr>
									<td><small>User Name<small></td>
									<td><strong><?php echo $this->session->userdata('un'); ?></strong></td>
								</tr>
								<tr>
									<td><small>Account Type<small></td>
									<td>
										<strong>
											<?php
												if($this->session->userdata('utype') == "Warehouse"){
													echo "Warehouse Officer";
												}else if($this->session->userdata('utype') == "Sub-Office"){
													echo "Branch Officer";
												}else{
													echo $this->session->userdata('utype');
												}
											?>
										</strong>
									</td>
								</tr>
								<tr>
									<td><small>Branch/Office<small></td>
									<td><strong><?php echo $this->session->userdata('area'); ?></strong></td>
								</tr>
							</tbody>
						</table>
					</div>
		    </div>
		  </div>

			<div class="col-sm-6 col-md-8">
				<div class='page-header'>
					<h3>Product Watch</h3>
				</div>

						  <?php
								if(isset($product)){
									foreach ($product as $product_detail):
							?>

										<form method='POST' action= "<?=site_url("warehouse/watchList")?>" role='form'>
											<div class="alert alert-warning" role="alert">
												<strong>Stock alert!&nbsp;</strong>
												<?php echo $product_detail['name']; ?>
												<input type='submit' name='details' class='btn btn-link pull-right' value='View details' />
												<input type='hidden' name='supplier' value="<?php echo $product_detail['supplier']?>" />
											</div>
										</form>

							<?php
										endforeach;
								}
							?>

			</div>
		</div>

</div>

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
		<li class="active"><i class='fa fa-home fa-fw'></i>&nbsp; Home</li>
	</ol>
</div>
