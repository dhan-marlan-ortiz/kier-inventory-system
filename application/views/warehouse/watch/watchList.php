<div class='container'>
	<div class='page-header'>
		<span class=' text-success h3'><i class='fa fa-home fa-fw'></i> Home </span>

	</div>
</div>






<div class='container'>
	<div class='panel panel-default'>

		<div class='panel-heading'>
			<h4>
				<i class='fa fa-list'></i> Watch List
				<span id='supplier' class='pull-right'></span>
			</h4>
		</div>

		<div class='panel-body'>

			<div class='table table-responsive' style='max-height: 350px; overflow: auto'>
				<table class="table table-fixed table-condensed" id='orderList'>
				  <?php if(isset($product)){  ?>

				  <thead>
						<tr>
							<th>#</th><th>Product</th><th>SKU</th><th>Category</th>
							<th>Box</th><th>Pack</th><th>Piece</th><th>Details</th>
						</tr>
				  </thead>
				  <tbody>
						<?php $line_number = 1; ?>

						<?php $i=1; foreach ($product as $product_detail): ?>

							<form method='POST' action= "<?=site_url("warehouse/listDetails")?>" role='form' name='watchList'>
								<tr>
									<td><?php echo $line_number; ?></td>
									<td><?php echo $product_detail['product_name']; ?> </td>
									<td><?php echo $product_detail['sku']; ?> </td>
									<td><?php echo $product_detail['product_type']; ?> </td>
									<td><?php echo $product_detail['box_stock']; ?> </td>
									<td><?php echo $product_detail['pack_stock']; ?> </td>
									<td><?php echo $product_detail['piece_stock']; ?> </td>
									<td><button type='submit' name='details' class='btn btn-link input-sm'>View</button></td>

									<input type='hidden' name='id' value="<?php echo $product_detail['product_id']?>">
									<input type='hidden' name='productId[]' value="<?php echo $product_detail['product_id']?>">
									<input type='hidden' id='supplierId' name='supplierId' value="<?php echo $product_detail['supplier']?>">
									<input type='hidden' id='supplierName' value="<?php echo $product_detail['name']; ?>" />
									<input type='hidden' id='supplierAddress' value="<?php echo $product_detail['address']; ?>" />
									<?php $line_number++;  ?>
								</tr>
							</form><!--product list form end-->

						<?php $i++; endforeach;  ?>
				  </tbody>

				  <?php } else echo "Stocks are sufficient"; ?>
				</table>
			</div>

		</div>

		<div class='panel-footer'>

				<a type='button' href="<?=site_url('Login_controller/warehouse')?>" class='btn btn-default'><i class='fa fa-angle-double-left fa-fw'></i>&nbsp;Return to Home</a>
				<button type='button' onclick="orderProduct();" class='btn btn-default pull-right'><i class='fa fa-check-square-o'></i> Order Products </button>

		</div>

	</div>
</div>



<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
		<li>
			<a href="<?=site_url('Login_controller/warehouse')?>"><i class='fa fa-home fa-fw'></i>&nbsp;Home</a>
		</li>
		<li>
			<a href="<?=site_url('Login_controller/warehouse')?>"><i class='fa fa-eye fa-fw'></i>&nbsp;Product Watch</a>
		</li>
		<li class="active">
			<i class='fa fa-list fa-fw'></i>&nbsp;Watch List
		</li>
	</ol>
</div>


<form method='POST' action='<?=site_url('warehouse_createOrder/orderFromWatchList')?>' name='orderFromWatchList'>
	<table id='queTable'>
		<tr></tr>
	</table>
</form>


<script>
	getSupplierName();


	function orderProduct(){
		try{
				var table = document.getElementById("orderList");
				var rowCount = table.rows.length;

				var qtable = document.getElementById("queTable");
					for(var i=1; i<rowCount; i++){
						var sku = table.rows[i].cells[2].innerHTML;
						var qRow = qtable.insertRow(i);

						//sku
						var cell1 = qRow.insertCell(0);
						var element1 = document.createElement("input");
						element1.type = "hidden";
						element1.name = "sku[]";
						element1.value = sku;
						cell1.appendChild(element1);

						//supplierID
						var supplierId = document.getElementById("supplierId").value;
						var cell2 = qRow.insertCell(1);
						var element2 = document.createElement("input");
						element2.type = "hidden";
						element2.name = "supplierId[]";
						element2.value = supplierId;
						cell2.appendChild(element2);
					}

					document.orderFromWatchList.submit();
				}catch(e){ alert(e); }



		}


	function getSupplierAddress(){
		y = document.getElementById('supplierAddress').value;
		document.getElementById('address').innerHTML = y;
	}
	function getSupplierName(){
		x = document.getElementById('supplierName').value;
		document.getElementById('supplier').innerHTML = x;
	}


</script>
