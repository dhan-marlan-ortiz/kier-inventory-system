<div class='container'>
	<div class='page-header'>
		<div class="row">
		  <div class="col-lg-6">
			<span class=' text-success h3'><i class='fa fa-home fa-fw'></i> Home </span>
		  </div><!-- /.col-lg-6 -->
		  <div class="col-lg-6">
				<form method='post' action="<?=site_url('warehouse/watchList')?>">
					<button type='submit' class='btn btn-default pull-right'><i class='fa fa-angle-double-left fa-fw'></i>&nbsp;Return to Watch List</button>
					<input type='hidden' name='supplier' value="<?php echo $product['supplier']; ?>">
				</form>
		  </div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
	</div>
</div>

<div class='container'>
<div>
	<div class='row'>
		<div class='col-sm-6'>
			<div class='panel'>
				<table class='table'>
						<thead>
							<tr>
								<th colspan='2'><h4><span class='text-primary'>Product Detail</span></h4></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>Product Name</th><td><?php echo $product['product_name']; ?></td>
							</tr>
							<tr>
								<th>Category</th><td><?php echo $product['product_type']; ?></td>
							</tr>
							<tr>
								<th>Manufacturer</th><td><?php echo $product['manufacturer']; ?></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>

		<div class='col-sm-6'>
			<div class='panel'>
				<table class='table'>
						<thead>
							<tr>
								<th colspan='2'><h4><span class='text-primary'>Product Variant</span></h4></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>Stock-keeping UNit (SKU)</th><td><?php echo $product['sku']; ?></td>
							</tr>
							<tr>
								<th>Description</th><td><?php echo $product['product_description']; ?></td>
							</tr>
							<tr>
								<th>Supplier</th><td><?php echo $product['name']; ?></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class='row'>
		<div class='col-sm-12'>
			<div class='panel'>
				<table class='table'>
						<thead>
							<tr>
								<th><h4><span class='text-default'>Unit</span></h4></th>
								<th><h4><span class='text-primary'>Avalable Stocks</span></h4></th>
								<th><h4><span class='text-primary'>Product Watch</span></h4></th>
								<th><h4><span class='text-primary'>Buying Cost</span></h4></th>
								<th><h4><span class='text-primary'>Selling Price</span></h4></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>Box</th>
								<td><?php echo number_format($product['box_stock']); ?></td>
								<td><?php echo number_format($product['box_watch']); ?></td>
								<td><?php echo number_format($product['box_cost'],2); ?></td>
								<td><?php echo number_format($product['box_price'],2); ?></td>
							</tr>
							<tr>
								<th>Pack</th>
								<td><?php echo number_format($product['pack_stock']); ?></td>
								<td><?php echo number_format($product['pack_watch']); ?></td>
								<td><?php echo number_format($product['pack_price'],2); ?></td>
								<td><?php echo number_format($product['pack_cost'],2); ?></td>
							</tr>
							<tr>
								<th>Piece</th>
								<td><?php echo number_format($product['piece_stock']); ?></td>
								<td><?php echo number_format($product['piece_watch']); ?></td>
								<td><?php echo number_format($product['piece_cost'],2); ?></td>
								<td><?php echo number_format($product['piece_price'],2); ?></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>
	</div>

	</div>
</div>



	<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
		<li>
			<a href="<?=site_url('Login_controller/warehouse')?>"><i class='fa fa-home fa-fw'></i>&nbsp;Home</a>
		</li>
		<li>
			<a href="<?=site_url('Login_controller/warehouse')?>"><i class='fa fa-eye fa-fw'></i>&nbsp;Product Watch</a>
		</li>
		<li class="active">
			<i class='fa fa-list fa-fw'></i>&nbsp;Watch List
		</li>
		<li class="active">
			<i class='fa fa-info-circle fa-fw'></i>&nbsp;Product Details
		</li>
	</ol>
</div>
