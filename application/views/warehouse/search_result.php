<div class='container'>
	<div class='page-header'>

		<div class='row'>

			<div class='col-sm-6'>
				<span class=' text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
			</div>

			<div class='col-sm-6'>
				<form method='POST' action="<?=site_url('warehouse/search_product')?>"> <!--search form-->

					<div class="input-group">
					  <input type="search" name='search_key' class="form-control" placeholder="Enter search key word" required>
					  <div class="input-group-btn">
							<button id='filter' type='button' class="btn btn-default" data-toggle="modal" data-target=".searchModal"><i class='fa fa-filter fa-fw'></i>&nbsp;Filter</button>
							<button class="btn btn-default" type="submit"><i class='fa fa-search fa-fw'></i>&nbsp;Search</button>
					  </div>
					</div>

					<div class="modal fade searchModal" tabindex="-1" role="dialog">
					  <div class="modal-dialog modal-sm">
						<div class="modal-content">
							<div class='modal-header'>
								<span class='h3 text-primary'> Filter Search</span>
							</div>
							<div class='modal-body'>
								<div onclick="filter('All');" data-dismiss="modal" class="radio"><label class="radio-inline"><input type="radio" name="searchFilter" id="inlineRadio1" value="all" checked> All </label></div>
								<div onclick="filter('SKU');" data-dismiss="modal" class="radio"><label class="radio-inline"><input type="radio" name="searchFilter" id="inlineRadio2" value="sku"> Stock-Keeping Unit (SKU) </label></div>
								<div onclick="filter('Category');" data-dismiss="modal" class="radio"><label class="radio-inline"><input type="radio" name="searchFilter" id="inlineRadio3" value="product_type"> Product Type</label></div>
								<div onclick="filter('Name');" data-dismiss="modal" class="radio"><label class="radio-inline"><input type="radio" name="searchFilter" id="inlineRadio4" value="product_name"> Product Name</label></div>
								<div onclick="filter('Description');" data-dismiss="modal" class="radio"><label class="radio-inline"><input type="radio" name="searchFilter" id="inlineRadio5" value="product_description"> Product Description</label></div>
								<div onclick="filter('Manufacturer');" data-dismiss="modal" class="radio"><label class="radio-inline"><input type="radio" name="searchFilter" id="inlineRadio6" value="manufacturer"> Manufacturer</label></div>
								<div onclick="filter('Invoice');" data-dismiss="modal" class="radio"><label class="radio-inline"><input type="radio" name="searchFilter" id="inlineRadio7" value="supplier"> Supplier </label></div>
								<div onclick="filter('Invoice');" data-dismiss="modal"  class="radio"><label class="radio-inline"><input type="radio" name="searchFilter" id="inlineRadio8" value="invoice"> Invoice Number</label></div>
							</div>
							<div class='modal-footer'>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					  </div>
					</div>
					<script>
						function filter(key){
							document.getElementById('filter').innerHTML = key;
						}

					</script>

			</form>
		</div>

		</div>

	</div>
</div>





	<div class='container'>
	<div class=''>

		<div class='row'>
			<div class='col-sm-6'>
				<h4><span class='text-primary'>Search Results for&nbsp;</span><span class='text-success'> <?php echo '"'.$search_key.'"'; ?></span></h4>
			</div>
			<div class='col-sm-6'>
				<div class='btn-group  pull-right'>
					<a class='btn btn-default' onclick="printContent('printThis');"><span><i class='fa fa-print fa-fw'></i>&nbsp;Print</span></a>
					<a class='btn btn-default' href="<?=site_url('warehouse/inventory')?>"><span><i class='fa fa-close fa-fw'></i>&nbsp;Close</span></a>
				</div>
			</div>
		</div>



		<div class='table table-responsive' style='max-height: 350px; overflow: auto'>
			<table class="table table-fixed table-condensed table-striped">

			<?php if(isset($search_result)){ ?>

			  <thead>
				<tr>
				  <th class="text-success">#</th>
				  <th class="text-success">Type</th>
				  <th class="text-success">SKU</th>
				  <th class="text-success">Name</th>
					<th class="text-success">Box</th>
				  <th class="text-success">Pack</th>
				  <th class="text-success">Piece</th>
				  <th class="text-success">Details</th>
				</tr>
			  </thead>

			<?php } ?>

			  <tbody>
				<?php
					$line_number = 1;
					if(!isset($search_result))
					{
						echo "<br><strong'>&nbsp;Sorry, </strong>no product matched your search. Please try again.";
					}
					else
					{
						$i = 1;
						foreach ($search_result as $result):
				?>
						<form method='POST' action= "<?=site_url("warehouse/product_information")?>" role='form'>
							<tr>
								<td> <?php echo $line_number; ?></td>
								<td> <?php echo $result['product_type']; ?> </td>
								<td> <?php echo $result['sku']; ?> </td>
								<td> <?php echo $result['product_name']; ?> </td>
								<td> <?php echo $result['box_stock']; ?> </td>
								<td> <?php echo $result['pack_stock']; ?> </td>
								<td> <?php echo $result['piece_stock']; ?> </td>

								<td class="col-xs-1 col-sm-1"><button type='submit' class='btn btn-link input-sm'>View</button></td>
								<input type='hidden' name='id' value="<?php echo $result['product_id']; ?>" />
						</form><!--product list form end-->
								<?php $line_number++;  ?>
							</tr>

				<?php 	$i++;
						endforeach;
				?>

				<?php } // end else?>

			  </tbody>
			</table>

	  </div>
	  <h4><span class='text-primary'>Total Results Count:&nbsp;</span><span class='text-success'><?php echo $line_number-1;  ?></span></h4>

    </div>
</div>

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
	  <li><a href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</a></li>
	  <li class="active"><i class='fa fa-search fa-fw'></i>&nbsp; Search</li>
	</ol>
</div>

<div class='hidden' id='printThis'>
	<h1><span class='text-primary'>Search Results for&nbsp;</span><span class='text-success'> <?php echo '"'.$search_key.'"'; ?></span></h1>

	<table class="table table-condensed table-striped">
		<?php if(isset($search_result)){
					$line_number = 1;
		?>
			<thead>
				<tr>
					<th class="text-success">#</th>
					<th class="text-success">Type</th>
					<th class="text-success">SKU</th>
					<th class="text-success">Name</th>
					<th class="text-success">Box</th>
					<th class="text-success">Pack</th>
					<th class="text-success">Piece</th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($search_result as $result): ?>

				<tr>
					<td> <?php echo $line_number; ?></td>
					<td> <?php echo $result['product_type']; ?> </td>
					<td> <?php echo $result['sku']; ?> </td>
					<td> <?php echo $result['product_name']; ?> </td>
					<td> <?php echo $result['box_stock']; ?> </td>
					<td> <?php echo $result['pack_stock']; ?> </td>
					<td> <?php echo $result['piece_stock']; ?> </td>
				</tr>

				<?php $line_number++; endforeach;  } ?>
			</tbody>
		</table>
</div>
