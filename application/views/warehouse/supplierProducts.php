<?php
  foreach($products as $p):
    $supplier = $p['name'];
    $address = $p['address'];
  endforeach;
 ?>

<div class='container'>
  <div class='page-header'>
    <h1><i class='fa fa-shopping-cart'></i> Supplier Product List</h1>
    <h4>
      <address>
        <strong class='text-primary'><?php echo $supplier; ?></strong><br><span class='text-info'><?php echo $address; ?></span>
      </address>
    </h4>
  </div>
</div>

<div class="container">
	<div id='productList'>

		<!-- SEARCH -->
		<div class='row'>
			<div class='container-fluid'>
				<div class='col-sm-12'>
					<div class="form-group">
						<div class='input-group'>
							<span class='input-group-addon text-primary'><i class='fa fa-search'></i></span>
							<input class="search form-control" placeholder="Search SKU, name, category, supplier, manufacturer" />

							<div class="input-group-btn">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class='fa fa-filter'></i> Sort <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class='sort' data-sort='sku'><a>SKU</a></li>
									<li class='sort' data-sort='name'><a>Product Name</a></li>
									<li class='sort' data-sort='type'><a>Category</a></li>
									<li class='sort' data-sort='supplier'><a>Supplier</a></li>
									<li class='sort' data-sort='manufacturer'><a>Manufacturer</a></li>
								</ul>
                <button type='button' class='btn btn-default'><i class='fa fa-plus'></i> Add More Products</button>
                <button onclick="printContent('printThis');" type='button' class='btn btn-default'><i class='fa fa-print'></i> Print All</button>
                <a href="<?=site_url('warehouse/new_supplier') ?>" class='btn btn-default'><i class='fa fa-angle-double-left'></i> Return</a>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>


	<div class="row">
		<div class="col-sm-12">
			<div style='max-height: 450px; overflow:auto' class='container-fluid'>

					<!-- HEADER -->
					<div class='list-group'>
						<div class='list-group-item active'>
							<div class='row'>
								<div class='col-sm-2  col-xs-2'>
										<strong>SKU</strong>
								</div>
								<div class='col-sm-5  col-xs-5'>
										<strong>Product</strong>
								</div>
								<div class='col-sm-2  col-xs-2'>
										<strong>Category</strong>
								</div>
								<div class='col-sm-1 col-xs-1'>
										<strong>Box</strong>
								</div>
								<div class='col-sm-1 col-xs-1'>
										<strong>Pack</strong>
								</div>
								<div class='col-sm-1 col-xs-1'>
										<strong>Piece</strong>
								</div>
							</div>
						</div>
					</div>


					<div class='list list-group'>

            <!-- BODY -->
						<?php $i=1; foreach ($products as $product_detail): ?>
							<a class="list-group-item  <?php if($i%2==0){ echo 'list-group-item-info'; } ?> "  href="<?=site_url('warehouse/product_information/'.$product_detail['product_id'])?>">
								<div class="row" >
									<div class='col-sm-2  col-xs-2 sku'>
											<?php echo $product_detail['sku']; ?>
									</div>
									<div class='col-sm-5  col-xs-5 name'>
											<?php echo $product_detail['product_name']; ?>
									</div>
									<div class='col-sm-2  col-xs-2 type'>
											<?php echo $product_detail['product_type']; ?>
									</div>
									<div class='col-sm-1 col-xs-1'>
											<?php echo $product_detail['box_stock']; ?>
									</div>
									<div class='col-sm-1 col-xs-1'>
											<?php echo $product_detail['pack_stock']; ?>
									</div>
									<div class='col-sm-1 col-xs-1'>
											<?php echo $product_detail['piece_stock']; ?>
									</div>
									<span class='hidden supplier'>
											<?php echo $product_detail['name']; ?>
									</span>
									<span class='hidden manufacturer'>
											<?php echo $product_detail['manufacturer']; ?>
									</span>
								</div>
							</a>
						<?php $i++; endforeach ?>
					</div>






		    </div>
	  </div>
	</div>
	</div><!-- datatable -->
</div>

<!--FOOTER-->
<div class="container">
	<hr>
	<ol class="breadcrumb pull-right">
    <li><a><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</a></li>
    <li><a><i class='fa fa-truck fa-fw'></i>&nbsp; Suppliers</a></li>
    <li class="active"><i class='fa fa-shopping-cart fa-fw'></i>&nbsp; Products</li>
	</ol>
</div>


<script>
var options = {
valueNames: ['sku','name','type', 'supplier', 'manufacturer']
};

var products = new List('productList', options);
</script>


<div id="printThis" class='hidden'>

    <div class='page-header'>
      <h1><i class='fa fa-shopping-cart'></i> Supplier Product List</h1>
      <h4>
        <address>
          <strong class='text-primary'><?php echo $supplier; ?></strong><br><span class='text-info'><?php echo $address; ?></span>
        </address>
      </h4>
    </div>

    <div class="row">
  		<div class="col-sm-12">

          <table class='table'>
            <thead>
              <tr>
                <th>#</th>
                <th>Product</th>
                <th>SKU</th>
                <th>Category</th>
                <th>Manufacturer</th>
                <th>Box</th>
                <th>Piece</th>
                <th>Pack</th>
              </tr>
            </thead>

          <tbody>

              <?php $i=1; foreach ($products as $product_detail): ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $product_detail['product_name']; ?></td>
                  <td><?php echo $product_detail['sku']; ?></td>
                  <td><?php echo $product_detail['product_type']; ?></td>
                  <td><?php echo $product_detail['manufacturer']; ?></td>
                  <td><?php echo $product_detail['box_stock']; ?></td>
                  <td><?php echo $product_detail['pack_stock']; ?></td>
                  <td><?php echo $product_detail['piece_stock']; ?></td>
                </tr>
              <?php $i++; endforeach ?>

          </tbody>
        </table>

      </div>

    </div>
	</div>
