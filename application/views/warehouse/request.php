<!-- This is inbox page -->

<div class='container'>
	<div class='page-header'>
		<div class='row'>
			<div class='col-sm-4'>
				<span class='text-success h3'><i class='fa fa-check-square-o fa-fw'></i> Request </span>
			</div>
			<div class='col-sm-8'>
				<div class='btn-group pull-right'>
					<a href="<?=site_url('warehouse_request/create')?>" class="btn btn-default" > <i class='fa fa-list-alt fa-fw'>&nbsp;</i>Create</a>
					<a href="<?=site_url('warehouse_request/inbox')?>" class="btn btn-default active"> <i class='fa fa-inbox fa-fw'>&nbsp;</i>Inbox</a>
					<a href="<?=site_url('warehouse_request/outbox')?>" class="btn btn-default"> <i class='fa fa-send-o fa-fw'>&nbsp;</i>Outbox</a>
					<a href="#" class="btn btn-default"> <i class='fa fa-recycle fa-fw'>&nbsp;</i>Trash</a>
				</div>
			</div>
		</div>



		</div>  <!-- end container-->
	</div>  <!-- end page header-->
</div>

<div class='container'>
	<div class='table table-responsive' style='max-height: 350px; overflow: auto'>
		<table class='table table-condensed'>
			<thead>
				<tr>
					<th class='text-success'> <span  text-success'> # </th>
					<th class='text-success'> <span  text-success'>  </th>
					<th class='text-success'> <span  text-success'> Sender </th>
					<th class='text-success'> <span  text-success'> Subject </th>
					<th class='text-success'> <span  text-success'> Address </th>
					<th class='text-success'> <span  text-success'> Date </th>
					<th class='text-success'> <span class='text-success'></span></th>
				</tr>
			</thead>
			<tbody>
				<?php if(isset($inbox)){
						$line = 1;
						foreach($inbox as $inboxHeader): ?>
							<form method='POST' action= "<?=site_url("warehouse_request/inboxContent")?>" role='form'>
								<tr <?php if($inboxHeader['isOpened'] == 1){ echo "class='active'"; } ?> >
									<td> <?php echo $line; ?> </td>
									<td> <?php if($inboxHeader['isOpened'] == 0){
													echo "Unread";
												}else{
													echo "Read";
												} ?>
									</td>
									<td> <?php echo $inboxHeader['firstname']." ".$inboxHeader['lastname']; ?> </td>
									<td> <?php echo $inboxHeader['orderSubject']; ?> </td>
									<td> <?php echo $inboxHeader['area']; ?> </td>
									<td> <?php echo $inboxHeader['request_date']; ?> </td>
									<td><button type='submit' value="<?php echo $inboxHeader['request_id']; ?>" name='request_id' class='btn btn-primary'>Open</button></td>
								</tr>

				<?php 	$line++;
						endforeach; } ?>
					</form>
			</tbody>
		</table>
	</div>
</div>


  <div class="container">

    <ol class="breadcrumb pull-right">
	  <li><a href="#"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</a></li>
	  <li class="active">Inbox </li>
	</ol>

  </div>
