<?php
	foreach ($request as $request_item):
		$subject = $request_item['orderSubject'];
		$recipient = $request_item['orderRecipient'];
		$date = $request_item['request_date'];
		$location = $request_item['deliveryLocation'];
	endforeach;

	if(isset($supplier)){
		foreach ($supplier as $supplier_data):
			$address = $supplier_data['address'];
		endforeach;
	}else{ $address = ""; }

	$formType = 'Supplier';
	if($recipient=='Kier Enterprises Gubat' || $recipient=='Kier Enterprises Irosin' || $recipient=='Kier Enterprises Casiguran'){
		$formType = 'Branch';
	}
?>

<div class='container' >
	<div class='page-header'>
		<span class='text-success h3'><i class='fa fa-check-square-o fa-fw'></i> Request </span>
		<div class="btn-group pull-right" role="group" >
		  <a type='button' class='btn btn-default' href="<?=site_url("warehouse_request/outbox")?>"><i class='fa fa-angle-double-left'></i>&nbsp;Return </a>
		  <button type='button' class='btn btn-default' onclick="printContent('printThis')"><i class='fa fa-print'></i>&nbsp;Print</button>
		  <button type='button' class='btn btn-default'><i class='fa fa-mail-forward'></i>&nbsp;Forward</button>
		  <button type='button' class='btn btn-default'><i class='fa fa-edit'></i>&nbsp;Edit</button>
		  <a type='button' href="<?=site_url("warehouse_request/deleteRequest/".$request_id)?>" class='btn btn-default'><i class='fa fa-trash'></i>&nbsp;Delete</a>
		</div>
	</div>
</div>

<div class='container'>
	<div class='panel panel-default' id='printThis'>

		<div class='panel-heading'>


			<div class='row'>
				<div class='col-sm-12'>
					<h2 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i> Request Form</h2>
				</div>
			</div>

			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>To</strong></div>
				<div class='col-sm-10 col-xs-10'><?php echo $recipient; ?><br><?php echo $address; ?></div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Request No.</strong></div>
				<div class='col-sm-10 col-xs-10'><?php echo $request_id; ?></div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Date</strong></div>
				<div class='col-sm-10 col-xs-10'><?php echo $new_date_format = date('F d, Y', strtotime($date)); ?></div>
			</div>

		</div>

		<div class='panel-body'>
			<table class='table table-condensed table-striped table-hover' id='productTable'>
				<thead>
					<tr>
						<th><span class='text-success'>#</span></th>
						<th><span class='text-success'>Product</span></th>
						<th><span class='text-success'>SKU</span></th>
						<th><span class='text-success'>Category</span></th>
						<th><span class='text-success'>Box</span></th>
						<th><span class='text-success'>Pack</span></th>
						<th><span class='text-success'>Piece</span></th>
						<?php if($formType=='Branch'){ ?>
							<th><span class='text-success'>Status</span></th>
						<?php } ?>


						<th><span class='text-success'>Notes</span></th>
					</tr>
				</thead>

				<tbody>
				 <?php if(isset($orderedList)){ ?>
					<?php $line_number = 1;
					foreach ($orderedList as $request_item): ?>
						<tr>
							<td><?php echo $line_number; ?></td>
							<td><?php echo $request_item['product_name']; ?> </td>
							<td><?php echo $request_item['sku']; ?> </td>
							<td><?php echo $request_item['product_type']; ?> </td>
							<td><?php echo $request_item['box']; ?> </td>
							<td><?php echo $request_item['pack']; ?> </td>
							<td><?php echo $request_item['piece']; ?> </td>
							<?php if($formType=='Branch'){ ?>
									<td><?php echo $request_item['status']; ?> </td>
							<?php } ?>

							<td><?php echo $request_item['orderMessage']; ?> </td>
						</tr>
					<?php $line_number++;
					endforeach ?>

				<?php } ?>
				 </tbody>

			</table>
		</div>

		</div><!-- end panel-->
	</div><!-- end container-->


<!-- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -->

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
	  <li><a><i class='fa fa-check-square-o fa-fw'></i>&nbsp;Request</a></li>
	  <li><a><i class='fa fa-send fa-fw'></i>&nbsp;Outbox</a></li>
	  <li class="active">&nbsp;Open Request</li>
	</ol>
</div>




<script>
	getInvoiceSummary();


	function getInvoiceSummary(){

		var table = document.getElementById("productTable");
		var rowCount = table.rows.length-1;

		document.getElementById('rowCount').innerHTML = rowCount;
	}

</script>
