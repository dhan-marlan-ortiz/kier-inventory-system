
<!-- HEADER -->
<div class='container'>
	<br/>
	<div class='panel'>
		<ul class="nav nav-tabs" role="tablist">
			<li><span class=' text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span></li>
			<li class="pull-right <?PHP if( isset($supplier) OR isset($invoice_exist) OR isset($return_invoice) OR isset($return_supplier) ){ echo 'active'; } ?>" role="presentation"><a href="#new" role="tab" data-toggle="tab"><i class='fa fa-plus'></i> New</a></li>
			<li class="pull-right <?PHP if( !isset($supplier) AND !isset($invoice_exist) AND !isset($return_invoice) AND !isset($return_supplier) ){ echo 'active'; } ?>" role="presentation" class="active"><a href="#existing" role="tab" data-toggle="tab"><i class='fa fa-list'></i> Invoices</a></li>
		</ul>
	</div>
</div>


<!-- MAIN BODY -->
<div class='container'>
  <div class="tab-content">

		<div role="tabpanel" class="tab-pane <?PHP if( !isset($supplier) AND !isset($invoice_exist) AND !isset($return_invoice) AND !isset($return_supplier) ){ echo 'active'; } ?>" id="existing">
			<div class='row'>
				<div class='col-sm-12'>
					<div class='panel panel-default' id='invoiceList'>

						<div class='panel-heading'>
							<div class='row'>
			          <div class='col-sm-6'>
									<h4 class='text-primary'><strong>Invoices</strong></h4>
								</div>
								<div class='col-sm-6'>
									<div class='input-group'>
										<input class="search form-control" placeholder="Search" />
										<div class='input-group-btn'>
												<!-- SORT -->
												<div class="btn-group">
												  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    Sort <span class="caret"></span>
												  </button>
												  <ul class="dropdown-menu">
														<li class="sort" data-sort="listNumber"><a>Invoice No.</a></li>
														<li class="sort" data-sort="listSupplier"><a>Supplier</a></li>
														<li class="sort" data-sort="sysDate"><a>Date</a></li>
													</ul>
												</div>
											
											<a class="btn btn-default" href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-remove'></i> Close</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class='panel-body' >
							<ul class='list list-unstyled list-group'>
							<?php
								if(isset($invoices)){
									$i=1;
			            foreach($invoices as $in): ?>
										<li class='list-group-item'>
											<a href="<?=site_url('warehouse/open_invoice/'.$in['invoice_num'])?>">
								      <div class='row' style='border: solid 1px white' >
												<div class='listNumber col-sm-3 col-xs-3'>
													<?php echo "#".$in['invoice_num']; ?>
												</div>
												<div class='listSupplier col-sm-7 col-xs-7'>
													<?php echo $in['supplier']; ?>
												</div>
												<div class='listDate col-sm-2 col-xs-2'>
													<?php echo $in['date']; ?>
												</div>
											</div>
											<div class='sysDate hidden'>
												<?php echo $in['sysDate']; ?>
											</div>
											</a>
										</li>
								<?php $i++; endforeach; } ?>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane <?PHP if( isset($supplier) OR isset($invoice_exist) OR isset($return_invoice) OR isset($return_supplier) ){ echo 'active'; } ?>" id="new">
			<div class='row'>
				<div class='col-sm-4 col-sm-offset-4'>
					<div class='panel panel-default'>

						<div class='panel-heading'>
							<h4 class='text-primary'><strong>Add New Invoice</strong></h4>
						</div>

						<div class='panel-body'>
							<form method='POST' action="<?=site_url('warehouse/delivery2')?>">
							  <div class="form-group">
								<label class='control-label'>Invoice No.:</label>
								<input name='invoice' type="text" class="form-control" <?php if(isset($return_invoice)){ echo "value='$return_invoice'"; }?> required />
							  </div>
							  <div class="form-group">
								<label class='control-label'>Supplier Name: </label>
									<span class='pull-right'>
										<a data-toggle="modal" data-target=".searchSupplier">Select from list</a> |
										<a onclick="addSupplier('');">Clear</span>
									</span>
								<textarea type="text" name='supplier' class="form-control" id='selectedSupplier' required /><?php if(isset($return_supplier)){ echo $return_supplier; } ?></textarea>
							  </div>

							  <div class='row'>
								<div class='col-sm-6'>
									<div class="form-group">
										<a href="<?=site_url('warehouse/inventory')?>" class='btn btn-default btn-block'>Cancel</a>
									</div>
								</div>
								<div class='col-sm-6'>
									<div class="form-group">
										<button type='submit' class='btn btn-primary btn-block'>Next</button>
									</div>
								</div>
							  </div>
							  <?php if(isset($supplier)){ ?>
								<div class='row'>
									<div class='col-sm-12'>
										<div class='alert alert-warning alert-dismissible' role='alert'>
										  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
										  Please select supplier from list.
										</div>
									</div>
								</div>
								<?php } ?>
							</form>

							<?php if(isset($invoice_exist)){?>
								<div class="alert alert-warning alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong>Invoice No. already exist!</strong> Please try another.
									<p><a> Click here to view exsisting invoice. </a></p>
								</div>
							<?php } ?>
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
	  <li><a href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</a></li>
	  <li class="active"><i class='fa fa-truck fa-fw'></i>&nbsp; Invoice</li>
	</ol>
</div>

<div class="modal fade searchSupplier" tabindex="-1" role="dialog">
	<div id="users">
	  <div class="modal-dialog modal-md">
		<div class="modal-content">

			<div class='modal-header'>
				<span class='h3 text-primary'> <strong>Select Supplier</strong> <button class="sort btn btn-default pull-right" data-sort="name"><i class='fa fa-sort-alpha-desc'></i>&nbsp;Sort</button></span>
			</div>

			<div class='modal-body'>
				<input class="search form-control input-lg" placeholder="Search supplier" />

				<div class='table table-responsive' style='max-height: 300px; overflow: auto'>
					<div class="list list-group">
						<?php if(isset($suppliers)){ foreach($suppliers as $supplier_data): ?>
							<a class='list-group-item' style='border:none;' onclick="addSupplier('<?php echo $supplier_data['name']; ?>');" data-dismiss="modal">
								<span class="name text-primary"><?php echo $supplier_data['name']; ?></span>
							</a>

						<?php endforeach; } ?>
					</div>
				</div>
			</div>

			<div class='modal-footer'>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div>
	  </div>
  </div>
</div>



<script>

	function addSupplier(supplierName){ document.getElementById('selectedSupplier').innerHTML = supplierName;}
	var options = { valueNames: ['name'] };
	var userList = new List('users', options);


	var list = { valueNames: ['listDate', 'listSupplier', 'listNumber', 'sysDate'] };
	var invoices = new List('invoiceList', list);

</script>
