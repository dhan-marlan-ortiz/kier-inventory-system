
<div class='container'>
	<div class='page-header'>

		<div class='row'>

			<div class='col-sm-3'>
				<span class=' text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
			</div>

			<div class='col-sm-9'>
				<ul class="list-inline pull-right">
		  		<li><a href="<?=site_url('warehouse/new_product')?>" ><i class='fa fa-shopping-cart'></i> Register Product</a></li>
					<li><a href="<?=site_url('warehouse/delivery1')?>"><i class='fa fa-list-alt'></i> Invoices</a></li>
					<li><a href="#"><i class='fa fa-shoppping-cart'></i> Pull-Outs</a></li>
					<li><a href="<?=site_url('warehouse/new_supplier')?>" ><i class='fa fa-truck'></i> Suppliers</a></li>
					<li><a href="<?=site_url('warehouse/new_manufacturer')?>"><i class='fa fa-building-o'></i> Manufacturers</a></li>
					<li><a href="<?=site_url('warehouse/category')?>"><i class='fa fa-tag'></i> Product Category</a></li>
				</ul>
			</div>

		</div>

	</div>
</div>






<!--PRODUCT LIST-->
<div class="container">
	<div id='productList'>

		<!-- SEARCH -->
		<div class='row'>
			<div class='container-fluid'>
				<div class='col-sm-6'>
					<div class="form-group">
						<div class='input-group'>
							<span class='input-group-addon text-primary'><i class='fa fa-search'></i></span>
							<input class="search form-control" placeholder="Search SKU, name, category, supplier, manufacturer" />

							<div class="input-group-btn">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class='fa fa-filter'></i> Sort <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class='sort' data-sort='sku'><a>SKU</a></li>
									<li class='sort' data-sort='name'><a>Product Name</a></li>
									<li class='sort' data-sort='type'><a>Category</a></li>
									<li class='sort' data-sort='supplier'><a>Supplier</a></li>
									<li class='sort' data-sort='manufacturer'><a>Manufacturer</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	<div class="row">
		<div class="col-sm-12">
			<div style='max-height: 450px; overflow:auto' class='container-fluid'>

					<!-- HEADER -->
					<div class='list-group'>
						<div class='list-group-item active'>
							<div class='row'>
								<div class='col-sm-2  col-xs-2'>
										<strong>SKU</strong>
								</div>
								<div class='col-sm-5  col-xs-5'>
										<strong>Product</strong>
								</div>
								<div class='col-sm-2  col-xs-2'>
										<strong>Category</strong>
								</div>
								<div class='col-sm-1 col-xs-1'>
										<strong>Box</strong>
								</div>
								<div class='col-sm-1 col-xs-1'>
										<strong>Pack</strong>
								</div>
								<div class='col-sm-1 col-xs-1'>
										<strong>Piece</strong>
								</div>
							</div>
						</div>
					</div>


					<div class='list list-group'>



						<!-- BODY -->
						<?php $i=1; foreach ($product as $product_detail): ?>
							<a class="list-group-item  <?php if($i%2==0){ echo 'list-group-item-info'; } ?> "  href="<?=site_url('warehouse/product_information/'.$product_detail['product_id'])?>">
								<div class="row" >
									<div class='col-sm-2  col-xs-2 sku'>
											<?php echo $product_detail['sku']; ?>
									</div>
									<div class='col-sm-5  col-xs-5 name'>
											<?php echo $product_detail['product_name']; ?>
									</div>
									<div class='col-sm-2  col-xs-2 type'>
											<?php echo $product_detail['product_type']; ?>
									</div>
									<div class='col-sm-1 col-xs-1'>
											<?php echo $product_detail['box_stock']; ?>
									</div>
									<div class='col-sm-1 col-xs-1'>
											<?php echo $product_detail['pack_stock']; ?>
									</div>
									<div class='col-sm-1 col-xs-1'>
											<?php echo $product_detail['piece_stock']; ?>
									</div>
									<span class='hidden supplier'>
											<?php echo $product_detail['name']; ?>
									</span>
									<span class='hidden manufacturer'>
											<?php echo $product_detail['manufacturer']; ?>
									</span>
								</div>
							</a>
						<?php $i++; endforeach ?>
					</div>






		    </div>
	  </div>
	</div>
	</div><!-- datatable -->
</div>








<!--FOOTER-->
<div class="container">
	<hr>
	<ol class="breadcrumb pull-right">
		<li class="active"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</li>
	</ol>
</div>


<script>
var options = {
valueNames: ['sku','name','type', 'supplier', 'manufacturer']
};

var products = new List('productList', options);
</script>
