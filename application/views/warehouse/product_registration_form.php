<div class='container'>
	<div class='page-header'>
		<span class=' text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
	</div>
</div>

<div class='container'>
<div class=' panel panel-default'>
	<!-- HEADER -->
	<div class='panel-heading'>

				<h2 class='text-primary'><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Product Registration</h2>
				<div class='row'>
					<div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'>
						<address>
							<?php foreach ($supplierInfo as $supplierData):
									echo "<strong>".$supplierData['name']."</strong><br/>";
									echo $supplierData['address'];
								endforeach; ?>
						</address>
					</div>
				</div>

	</div>



<div class='panel-body'>

	<!-- FORM START -->
	<form method='post' action="<?=site_url('warehouse/product_registration')?>" class='form-horizontal'>
	<input type='hidden' name='supplier' value="<?php echo $supplierId; ?>">

	<!-- PRODUCT DETAILS AND PRODUCT VARIANTS ROW -->
	<div class='row'>
		<div class='col-sm-6'>
				<div class='page-header'>
					<span class='text-primary h4'> Product Details </span>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Product Name</label>
					<div class="col-sm-7">
						<input type="text" name="product_name" class="form-control" placeholder="e.g. Cream-o" pattern="[^'\x22]+" required <?php if(isset($error) || isset($registration_success)){ echo "value='$product_name'";} ?> >
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Category</label>
					<div class="col-sm-7">
						<input type="text" list='category' name="product_type" class="form-control" placeholder="e.g. Grocery" pattern="[^'\x22]+" required <?php if(isset($error) || isset($registration_success)){ echo "value='$product_type'";} ?> >
						<datalist id="category">
							<?php
								if(isset($category)){
									foreach($category as $c):
										echo "<option value='".$c['product_type']."'>";
									endforeach;
								}
							?>
						</datalist>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Manufacturer</label>
					<div class="col-sm-7">
						<input type="text" list='manufacturers' name="manufacturer" class="form-control" placeholder="e.g Universal Robina Corporation" <?php if(isset($error) || isset($registration_success)){ echo "value='$manufacturer'";} ?> >
						<datalist id="manufacturers">
							<?php
								if(isset($manufacturers)){
									foreach($manufacturers as $m):
										echo "<option value='".$m['name']."'>";
									endforeach;
								}
							?>
						</datalist>

					</div>


				</div>
			</div>



			<div class='col-sm-6'>
				<div class='page-header'>
					<span class='text-primary h4'> Product Variants </span>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">SKU</label>
					<div class="col-sm-7">
						<input name='sku' class='form-control' type='text' placeholder='e.g. DM3426' required  />
					</div>
				</div>

				<div class="form-group">
						<label class="col-sm-4 control-label">Product Description</label>
						<div class="col-sm-7">
							<textarea name='product_description' title="Invalid input" class='form-control' placeholder='e.g. Deluxe 33gx10x20'><?php if(isset($error) || isset($registration_success)){ echo $product_description;} ?></textarea>
						</div>
				</div>

		</div>
</div>


	<!-- PRODUCT PRICING AND WATCH LEVEL ROW -->
	<div class='row'>
		<div class='col-sm-6'>

				<div class='page-header'>
					<span class='text-primary h4'> Product Pricing </span>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Buying Cost</label>
					<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='box_cost' class='form-control' placeholder='Box' <?php if(isset($error)){ echo "value='$box_cost'";} ?> > </div>
					<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='pack_cost' class='form-control' placeholder='Pack' <?php if(isset($error)){ echo "value='$pack_cost'";} ?> > </div>
					<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='piece_cost' class='form-control' placeholder='Piece' <?php if(isset($error)){ echo "value='$piece_cost'";} ?> > </div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Selling Price</label>
					<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='box_price' class='form-control' placeholder='Box' <?php if(isset($error)){ echo "value='$box_price'";} ?> > </div>
					<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='pack_price' class='form-control' placeholder='Pack' <?php if(isset($error)){ echo "value='$pack_price'";} ?> > </div>
					<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='piece_price' class='form-control' placeholder='Piece' <?php if(isset($error)){ echo "value='$piece_price'";} ?> > </div>
				</div>

		</div>

		<div class='col-sm-6'>
				<div class='page-header'>
					<span class='text-primary h4'> Watch Level </span>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Watch Level</label>
					<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='box_watch' class='form-control' placeholder='Box' <?php if(isset($error)){ echo "value='$box_watch'";} ?> > </div>
					<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='pack_watch' class='form-control' placeholder='Pack' <?php if(isset($error)){ echo "value='$pack_watch'";} ?> > </div>
					<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='piece_watch' class='form-control' placeholder='Piece' <?php if(isset($error)){ echo "value='$piece_watch'";} ?> > </div>
				</div>
		</div>
	</div>







</div><!-- END PANEL BODY -->

	<div class='panel-footer'>
		<div class='row'>

			<div class='col-sm-2'>
				<a href="<?=site_url('warehouse/new_product')?>" class='btn btn-default  btn-block'><i class='glyphicon glyphicon-chevron-left'></i>&nbsp;Back </a>
			</div>
			<div class='col-sm-2 col-sm-offset-6'>
				<a href="<?=site_url('warehouse/inventory')?>" class='btn btn-default  btn-block'><i class='glyphicon glyphicon-remove'></i>&nbsp;Close </a>
			</div>
			<div class='col-sm-2'>
				<button class='btn btn-primary btn-block' type='submit'><i class='fa fa-save'></i>&nbsp;Save</button>
			</div>

		</div>
	</div>

</form>
</div><!-- END PANEL DEFAULT -->
</div> <!-- END CONTAINER -->

<div class='container'>
	<?php if(isset($registration_success)){ ?> <div class="alert alert-success" role="alert"> <?php echo "<p class='text-center'><strong> Success! </strong>".$product_name." is successfully added to inventory.</p>" ?> </div> <?php } ?>
	<?php if(isset($error_name)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_name; ?> </div> <?php } ?>
	<?php if(isset($error_type)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_type; ?> </div> <?php } ?>
	<?php if(isset($error_manufacturer)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_manufacturer; ?> </div> <?php } ?>
	<?php if(isset($error_sku)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_sku; ?> </div> <?php } ?>
	<?php if(isset($error_description)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_description; ?> </div> <?php } ?>
	<?php if(isset($error_supplier)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_supplier; ?> </div> <?php } ?>
	<?php if(isset($error_box_cost)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_box_cost; ?> </div> <?php } ?>
	<?php if(isset($error_pack_cost)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_pack_cost; ?> </div> <?php } ?>
	<?php if(isset($error_piece_cost)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_piece_cost; ?> </div> <?php } ?>
	<?php if(isset($error_box_price)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_box_price; ?> </div> <?php } ?>
	<?php if(isset($error_pack_price)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_pack_price; ?> </div> <?php } ?>
	<?php if(isset($error_piece_price)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_piece_price; ?> </div> <?php } ?>
	<?php if(isset($error_box_watch)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_box_watch; ?> </div> <?php } ?>
	<?php if(isset($error_pack_watch)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_pack_watch; ?> </div> <?php } ?>
	<?php if(isset($error_piece_watch)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_piece_watch; ?> </div> <?php } ?>
</div>

<div class='container'>
	<ol class="breadcrumb pull-right">
		<li><a href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</a></li>
		<li class="active"><i class='fa fa-th fa-fw'></i>&nbsp; Product Registration</li>
	</ol>
</div>
