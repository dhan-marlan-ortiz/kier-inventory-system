<div class='container'>
	<div class='page-header'>
		<span class='text-success h3'><i class='fa fa-check-square-o fa-fw'></i> Request </span>
		<div class='btn-group pull-right'>
			<a href="<?=site_url('warehouse_request/create')?>" class="btn btn-default" > <i class='fa fa-list-alt fa-fw'>&nbsp;</i>Create</a>
			<a href="<?=site_url('warehouse_request/inbox')?>" class="btn btn-default "> <i class='fa fa-inbox fa-fw'>&nbsp;</i>Inbox</a>
			<a href="<?=site_url('warehouse_request/outbox')?>" class="btn btn-default active"> <i class='fa fa-send-o fa-fw'>&nbsp;</i>Outbox</a>
			<a href="#" class="btn btn-default"> <i class='fa fa-recycle fa-fw'>&nbsp;</i>Trash</a>
		</div>  <!-- end container-->
	</div>  <!-- end page header-->
</div>

<div class='container'>

	<div class='row'>
		<div class="col-sm-5 col-sm-offset-7">

		</div>
	</div>

	<div class='table-responsive'>
		<table class='table table-condensed table-striped table-hover'>
			<thead>
				<tr>
					<th><span class='text-success'>#</span></th>
					<th><span class='text-success'>Subject</span></th>
					<th><span class='text-success'>Recipient</span></th>
					<th><span class='text-success'>Date</span></th>
					<th><span class='text-success'></span></th>
				</tr>
			</thead>
			<?php if(!isset($outbox)){ }else{ //if result is not empty ?>
			<tbody><?php $line_number = 1;	$i=0; foreach ($outbox as $outbox_item): ?>
				<form method='POST' action= "<?=site_url('warehouse_request/open_outbox')?>" role='form'>

					<tr>
						<td><?php echo $line_number; ?></td>
						<td><?php echo $outbox_item['orderSubject']; ?> </td>
						<td><?php echo $outbox_item['orderRecipient']; ?> </td>
						<td><?php echo $outbox_item['request_date']; ?> </td>
						<td><button type='submit' name='submit' class='btn btn-default'><i class='fa fa-folder-open-o fa-fw'></i>&nbsp;Open</button></td>
						<input type='hidden' name="request_id" value="<?php echo $outbox_item['request_id']; ?>" />
						<input type='hidden' name="recipient" value="<?php echo $outbox_item['orderRecipient']; ?>" />
					</tr>
				</form><!--product list form end-->
				<?php $line_number++; $i++; endforeach ?>
			 </tbody>
			 <?php } // result end?>
		</table>
	</div><!--table responsive end-->

</div>


  <div class="container">
    <ol class="breadcrumb pull-right">
	  <li><a href="#"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</a></li>
	  <li class="active">Outbox </li>
	</ol>
  </div>
