<div class='page-header'>
	<div class='container'>
		<span class=' text-success h3'><i class='fa fa-cube'></i> Inventory Watch Report </span>
		<div class='btn-group pull-right'>
			<a type='button'href="<?=site_url('warehouse_report/landingPage')?>"  class='btn btn-default'><i class='fa fa-angle-double-left'></i> Return</a>
		</div>
	</div>
</div>	

<div class="container">
	  
		<div class='table table-responsive' style='max-height: 350px; overflow: auto'>
			<table class="table table-fixed table-condensed">
				  <?php if(isset($product)){  ?>
				  
				  <thead>
					<tr>
						<th>#</th><th>Supplier</th><th>Products</th>
					</tr>
				  </thead>
				  <tbody>
					<?php $line_number = 1; ?>	
					
					<?php $i=1; foreach ($product as $product_detail): ?>
					
						<form method='POST' action= "<?=site_url("warehouse/watchList")?>" role='form'>		
							<tr>
								<td><?php echo $line_number; ?></td>
								<td><?php echo $product_detail['name']; ?> </td>
								<td><button type='submit' name='details' class='btn btn-link input-sm'>View</button></td>
								<input type='hidden' name='supplier' value="<?php echo $product_detail['supplier']?>">
								<?php $line_number++;  ?> 
							</tr>
						</form><!--product list form end-->						
								
					<?php $i++; endforeach;  ?>
				  </tbody>
				  
				  <?php } else echo "Stocks are sufficient"; ?>
			</table>
		</div>
	  
	</div>