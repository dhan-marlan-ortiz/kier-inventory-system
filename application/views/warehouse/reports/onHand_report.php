<div class='page-header'>
	<div class='container'>
		<span class=' text-success h3'><i class='fa fa-cube'></i> Inventory Stock on Hand Report</span>
		<div class='btn-group pull-right'>
			<a type='button'href="<?=site_url('warehouse_report/landingPage')?>"  class='btn btn-default'><i class='fa fa-angle-double-left'></i> Return</a>
			<button type="button" class="btn btn-default " data-toggle="modal" data-target=".filtersModal"><i class='fa fa-filter'></i> Set Filters</button>
			<button type='button' name='print' onclick="printContent('printThis');" class='btn btn-default'><i class='fa fa-download'></i>&nbsp;Download&nbsp;/&nbsp;<i class='fa fa-print'></i>&nbsp;Print</button>&nbsp;

		</div>
	</div>
</div>


<div class='container'>


	<div class='table table-responsive' style='max-height: 350px; overflow: auto' id='printThis'>



					<h2 class='text-primary'><strong>Kier Enterprises</strong>
						<small class='pull-right' id='date'>All</small>
					</h2>
					<h3 class=>Inventory Stock on Hand Report</h3>

					<strong>Category : </strong><span id='category'>All</span> |
					<strong>Supplier : </strong><span id='supplier'>All</span> |
					<strong>Manufacturer : </strong><span id='manufacturer'>All</span>


        <table class="table table-fixed table-condensed" >
          <thead class='text-success'>
            <tr>
				<th>#</th><th>Product</th><th>SKU</th><th>Category</th>
				<th>Box</th><th>Pack</th><th>Piece</th>
            </tr>
          </thead>
          <tbody>
            <?php $line_number = 1; ?>
			<?php $i=1; foreach ($product as $product_detail): ?>

				<form method='POST' action= "<?=site_url("warehouse/product_information")?>" role='form'>
					<tr>
						<td><?php echo $line_number; ?></td>
						<td><?php echo $product_detail['product_name']; ?> </td>
						<td><?php echo $product_detail['sku']; ?> </td>
						<td><?php echo $product_detail['product_type']; ?> </td>
						<td><?php echo $product_detail['box_stock']; ?> </td>
						<td><?php echo $product_detail['pack_stock']; ?> </td>
						<td><?php echo $product_detail['piece_stock']; ?> </td>
					</tr>
						<input type='hidden' name='id' value="<?php echo $product_detail['product_id']?>">
						<?php $line_number++;  ?>
				</form><!--product list form end-->

			<?php $i++; endforeach ?>
          </tbody>
        </table>
    </div>

</div>







<div class="modal fade filtersModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<form method='POST' action="<?=site_url('warehouse_report/onHand_report')?>">
		<div class="modal-dialog modal-md">
			<div class="modal-content">

				<div class='modal-header'>
					<h3 class="modal-title">Select Filters</h3>
				</div>
				<div class='modal-body'>
					<div class='row'>

						<div class='col-sm-4'>
							<div class="form-group">
								<label>Supplier</label>
								<input type="text" name='supplier'
								<?php if(isset($filterInfo['filterSupplier']))
								{
								if($filterInfo['filterSupplier'] != '#'){ echo "value=".$filterInfo['filterSupplier']; }
								} ?>
								class="form-control" placeholder="Supplier Name" id="inputSupplier" >
							</div>
						</div>
						<div class='col-sm-4'>
							<div class="form-group">
								<label>Manufacturer</label>
								<input type="text" name='manufacturer'
								<?php if(isset($filterInfo['filterManufacturer'])){
								if($filterInfo['filterManufacturer'] != '#'){ echo "value=".$filterInfo['filterManufacturer']; }
								} ?> class="form-control" placeholder="Manufacturer Name" id="inputManufacturer">
							</div>
						</div>
						<div class='col-sm-4'>
							<div class="form-group">
								<label>Category</label>
								<input type="text" name='category'
								<?php if(isset($filterInfo['filterCategory'])){
								if($filterInfo['filterCategory'] != '#'){  echo "value=".$filterInfo['filterSupplier']; }
								} ?> class="form-control" placeholder="Category" id="inputCategory" >
							</div>
						</div>


					</div>
				</div>
				<div class='panel-footer'>
					<div class='row'>
						<div class='col-sm-4 col-sm-offset-4'>
							<button type='submit' name='filter' class='btn btn-success btn-block'><i class='fa fa-filter'></i>&nbsp;Filter</button>
						</div>
					</div>
				</div>

			</div>
		</div>
	</form>
</div>







<script language = "javascript">
	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth();
	var day = d.getDate();
	var monthInWords = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
	document.getElementById("date").innerHTML = monthInWords[month]+' '+day+', '+year;

	if(document.getElementById("inputCategory").value == ""){
		document.getElementById("category").innerHTML = "All";
	}else{
		document.getElementById("category").innerHTML = document.getElementById("inputCategory").value;

	}
		if(document.getElementById("inputManufacturer").value == ""){
			document.getElementById("manufacturer").innerHTML = "All";
		}else{
			document.getElementById("manufacturer").innerHTML = document.getElementById("inputManufacturer").value;
		}

			if(document.getElementById("inputSupplier").value == ""){
				document.getElementById("supplier").innerHTML = "All";
			}else{
				document.getElementById("supplier").innerHTML = document.getElementById("inputSupplier").value;
			}
</script>
