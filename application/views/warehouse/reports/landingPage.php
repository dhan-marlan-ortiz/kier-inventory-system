<div class='container'>
	<div class='page-header'>
		<span class=' text-success h3'><i class='fa fa-pie-chart fa-fw'></i> Reports </span>
	</div>
</div>

<div class='container'>

	<div class='row'>
		<div class='col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3'>

			<div class="list-group">
				<a href="<?=site_url('warehouse_report/onHand_report')?>" class="list-group-item">
					<i class="fa fa-cube fa-3x fa-pull-left fa-border text-primary"></i>
					<h4 class="list-group-item-heading"><span class='text-primary'>Inventory Stock on Hand<span></h4>
					<p class="list-group-item-text">
						View your stock levels and stock values for each product.
						Filter by category, manufacturer, supplier.
					</p>
				</a>
			</div>

			<div class="list-group">
				<a href="#" class="list-group-item">
					<i class="fa fa-calendar fa-3x fa-pull-left fa-border text-primary"></i>
					<h4 class="list-group-item-heading"><span class='text-primary'>Monthly Inventory Report<span></h4>
					<p class="list-group-item-text">
						View the volume of received and released products on a monthly period.
						Filter by category, manufacturer, supplier.
					</p>
				</a>
			</div>

			<div class="list-group">
				<a href="<?=site_url('warehouse_report/watch_report')?>" class="list-group-item">
					<i class="fa fa-eye fa-3x fa-pull-left fa-border text-primary"></i>
					<h4 class="list-group-item-heading"><span class='text-primary'>Product Watch</span></h4>
					<p class="list-group-item-text">
						View a list of product in the inventory that are low in stock.
					</p>
					<br/>
				</a>
			</div>


			<div class="list-group">
				<a href="#" class="list-group-item">
					<i class="fa fa-truck fa-3x fa-pull-left fa-border text-primary"></i>
					<h4 class="list-group-item-heading"><span class='text-primary'>Delivery History Report</span></h4>
					<p class="list-group-item-text">
						View all your delivery invoices and product out-slips by time period and supplier
					</p>
				</a>
			</div>
		</div>
	</div>

</div>
