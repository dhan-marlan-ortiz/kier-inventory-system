iv class='row'>
		<div class='col-sm-7  panel panel-default'>
			<form method="POST" action="<?=site_url('warehouse/product_registration')?>" class="form-horizontal" role='form'>
			  <div class='page-header'>
				  <span class='text-primary h4'> Product Details </span>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">Product Name</label>
				<div class="col-sm-7">
				  <input type="text" name="product_name" class="form-control" placeholder="e.g. Cream-o" pattern="[^'\x22]+" required <?php if(isset($error) || isset($registration_success)){ echo "value='$product_name'";} ?> >
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">Product Type</label>
				<div class="col-sm-7">
				  <input type="text" name="product_type" class="form-control" placeholder="e.g. Biscuit" pattern="[^'\x22]+" required <?php if(isset($error) || isset($registration_success)){ echo "value='$product_type'";} ?> >
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">Manufacturer</label>
				<div class="col-sm-7">
				  <input type="text" name="manufacturer" class="form-control" placeholder="e.g Universal Robina Corporation" <?php if(isset($error) || isset($registration_success)){ echo "value='$manufacturer'";} ?> >
				</div>
			  </div>
			  <div class='page-header'>
				  <span class='text-primary h4'> Product Variants </span>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">SKU</label>
				<div class="col-sm-4">
				  <input name='sku' class='form-control' type='text' placeholder='e.g. DM3426' required  >
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">Product Description</label>
				<div class="col-sm-7">
				  <textarea name='product_description' title="Invalid input" class='form-control' placeholder='e.g. Deluxe 33gx10x20'><?php if(isset($error) || isset($registration_success)){ echo $product_description;} ?></textarea>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">Supplier</label>
				<div class="col-sm-7">
				  <input type='text' name='supplier' title="Invalid input" class='form-control' placeholder='e.g. Best Corporation' <?php if(isset($error) || isset($registration_success)){ echo "value='$supplier'";} ?>   >
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">Buy Cost</label>
				<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='box_cost' class='form-control' placeholder='Box' <?php if(isset($error)){ echo "value='$box_cost'";} ?> > </div>
				<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='pack_cost' class='form-control' placeholder='Pack' <?php if(isset($error)){ echo "value='$pack_cost'";} ?> > </div>
				<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='piece_cost' class='form-control' placeholder='Piece' <?php if(isset($error)){ echo "value='$piece_cost'";} ?> > </div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">Selling Price</label>
				<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='box_price' class='form-control' placeholder='Box' <?php if(isset($error)){ echo "value='$box_price'";} ?> > </div>
				<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='pack_price' class='form-control' placeholder='Pack' <?php if(isset($error)){ echo "value='$pack_price'";} ?> > </div>
				<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='piece_price' class='form-control' placeholder='Piece' <?php if(isset($error)){ echo "value='$piece_price'";} ?> > </div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">Watch Level</label>
				<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='box_watch' class='form-control' placeholder='Box' <?php if(isset($error)){ echo "value='$box_watch'";} ?> > </div>
				<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='pack_watch' class='form-control' placeholder='Pack' <?php if(isset($error)){ echo "value='$pack_watch'";} ?> > </div>
				<div class='col-md-3'><input type='number' min=0 max=9999999 step=0.01 name='piece_watch' class='form-control' placeholder='Piece' <?php if(isset($error)){ echo "value='$piece_watch'";} ?> > </div>
			  </div>
			  <hr>
			  <div class="form-group">
				<div class='col-sm-3 col-sm-offset-3 col-xs-4'>
					<button type='submit' class='btn btn-primary btn-block'><i class='fa fa-save fa-fw'></i>&nbsp; Save </button>
				</div>
			</form><!--form end-->	
				<div class='col-sm-3 col-xs-4'>
					<form method="POST" action="<?=site_url('warehouse/new_product')?>" role='form'>
						<button type='submit' class='btn btn-primary btn-block'><i class='fa fa-eraser fa-fw'></i>&nbsp; Clear </button>
					</form>
				</div>
				<div class='col-sm-3 col-xs-4'>
					<form method="POST" action="<?=site_url('warehouse/inventory')?>" role='form'>
						<button type='submit' class='btn btn-primary btn-block'><i class='fa fa-remove fa-fw'></i>&nbsp; Close </button>
					</form>	
				</div>
			  </div>
			
		</div>
		<div class='col-sm-4 col-sm-offset-1  panel panel-default'>
			
				<div class='page-header'>
					<span class='text-primary h4'> Recently Added Products </span>
				</div>
				<div>
					This panel shows recently added products
				</div><br>
		</div><!--col 2 end-->
	</div><!--row end-->
	
	<div class='row'>
		<?php if(isset($registration_success)){ ?> <div class="alert alert-success" role="alert"> <?php echo "<p class='text-center'><strong> Success! </strong>".$product_name." is successfully added to inventory.</p>" ?> </div> <?php } ?>
		<?php if(isset($error_name)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_name; ?> </div> <?php } ?>
		<?php if(isset($error_type)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_type; ?> </div> <?php } ?>
		<?php if(isset($error_manufacturer)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_manufacturer; ?> </div> <?php } ?>
		<?php if(isset($error_sku)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_sku; ?> </div> <?php } ?>
		<?php if(isset($error_description)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_description; ?> </div> <?php } ?>
		<?php if(isset($error_supplier)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_supplier; ?> </div> <?php } ?>
		<?php if(isset($error_box_cost)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_box_cost; ?> </div> <?php } ?>
		<?php if(isset($error_pack_cost)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_pack_cost; ?> </div> <?php } ?>
		<?php if(isset($error_piece_cost)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_piece_cost; ?> </div> <?php } ?>
		<?php if(isset($error_box_price)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_box_price; ?> </div> <?php } ?>
		<?php if(isset($error_pack_price)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_pack_price; ?> </div> <?php } ?>
		<?php if(isset($error_piece_price)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_piece_price; ?> </div> <?php } ?>
		<?php if(isset($error_box_watch)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_box_watch; ?> </div> <?php } ?>
		<?php if(isset($error_pack_watch)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_pack_watch; ?> </div> <?php } ?>
		<?php if(isset($error_piece_watch)){ ?> <div class="alert alert-danger" role="alert"> <?php echo $error_piece_watch; ?> </div> <?php } ?>
	</div>