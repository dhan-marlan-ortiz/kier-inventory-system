<div class='page-header'>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-2 col-xs-2'>
				<span class='text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory</span>
			</div>
			<div class='col-sm-2 col-xs-2'>
				<a class='btn btn-default' href="<?=site_url('warehouse/new_product')?>"><i class='fa fa-plus fa-fw'></i>&nbsp;New Product</a>
			</div>
			<div class='col-sm-4  col-xs-4'>
				<div class="input-group">
				  <input type="text" class="form-control" placeholder="Search for...">
				  <span class="input-group-btn">
					<button class="btn btn-default" type="button">Go!</button>
				  </span>
				</div>
			</div>
			<div class='col-sm-4 col-xs-4'>
				<ol class="breadcrumb pull-right">
				  <li class="active"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</li>
				</ol>
			</div>
		</div>  <!-- end container-->
	</div>  <!-- end page header-->
</div>