<?php
foreach ($inboxContentHeader as $inboxContentHeaderData):
	$firstname = $inboxContentHeaderData['firstname'];
	$lastname = $inboxContentHeaderData['lastname'];
	$account_id = $inboxContentHeaderData['account_id'];
	$subject = $inboxContentHeaderData['orderSubject'];
	$date = $inboxContentHeaderData['request_date'];
	$location = $inboxContentHeaderData['deliveryLocation'];
endforeach;
?>


<div class='container' >
	<div class='page-header'>
		<span class='text-success h3'><i class='fa fa-check-square-o fa-fw'></i> Request </span>
		<div class="btn-group pull-right" role="group" >
		  <a type='button' class='btn btn-default' href="<?=site_url("warehouse_request/inbox")?>"><i class='fa fa-angle-double-left'></i>&nbsp;Return </a>
		  <button type='button' class='btn btn-default' onclick="printContent('printThis')"><i class='fa fa-print'></i>&nbsp;Print Form</button>
			<button type='button' class='btn btn-default' onclick="printContent('pickList')"><i class='fa fa-print'></i>&nbsp;Pick list</button>

		  <!-- <button type='button' class='btn btn-default'><i class='fa fa-mail-forward'></i>&nbsp;Forward</button> -->
		  <!-- <button type='button' class='btn btn-default'><i class='fa fa-edit'></i>&nbsp;Edit</button> -->
		  <!-- <a type='button' href="<?=site_url("warehouse_request/deleteRequest/".$request_id)?>" class='btn btn-default'><i class='fa fa-trash'></i>&nbsp;Delete</a> -->
		</div>
	</div>
</div>

<div class='container'>
	<div class='panel panel-default'>

		<div class='panel-heading'>
			<div class='row'>
				<div class='col-sm-12'>
					<h2 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i> Request Form</h2>
				</div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Sender</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $firstname." ".$lastname; ?></div>
				<div class='col-sm-2 col-xs-2'><strong>Location</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $location; ?></div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Date</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $new_date_format = date('F d, Y', strtotime($date)); ?></div>
				<div class='col-sm-2 col-xs-2'><strong>Request No.</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $request_id; ?></div>
			</div>
		</div>

		<div class='panel-body'>
			<table class='table table-condensed table-striped table-hover' id='productTable'>
				<thead>
					<tr>
						<th colspan='4'><span></span></th>
						<th colspan='3' class='text-center'><span class='text-primary '>Requested</span></th>
						<th colspan='3' class='text-center'><span class='text-success '>Approved</span></th>
						<th colspan='3' class='text-center'><span class='text-success '></span></th>
					</tr>
					<tr>
						<th><span>#</span></th>
						<th><span>Product</span></th>
						<th><span>SKU</span></th>
						<th><span>Category</span></th>
						<th><span class='text-primary'>Box</span></th>
						<th><span class='text-primary'>Pack</span></th>
						<th><span class='text-primary'>Piece</span></th>
						<th><span class='text-success'>Box</span></th>
						<th><span class='text-success'>Pack</span></th>
						<th><span class='text-success'>Piece</span></th>
						<th><span>Status</span></th>
						<th><span>Notes</span></th>
						<th><span>Update</span></th>
					</tr>
				</thead>
				 <?php if(isset($inboxContent)){ ?>
				 <tbody>
					 <?php  $line = 1; foreach($inboxContent as $inboxContentData): ?>
						 <tr>
							 <td> <?php echo $line; ?> </td>
							 <td> <?php echo $inboxContentData['product_name']; ?> </td>
							 <td> <?php echo $inboxContentData['sku']; ?> </td>
							 <td> <?php echo $inboxContentData['product_type']; ?> </td>
						 	 <td class='text-primary'> <?php echo $inboxContentData['box']; ?> </td>
							 <td class='text-primary'> <?php echo $inboxContentData['pack']; ?> </td>
							 <td class='text-primary'> <?php echo $inboxContentData['piece']; ?> </td>
							 <td class='text-success'> <?php if($inboxContentData['approved_box'] > 0){ echo $inboxContentData['approved_box']; }?> </td>
							 <td class='text-success'> <?php if($inboxContentData['approved_pack'] > 0){ echo $inboxContentData['approved_pack']; }?></td>
							 <td class='text-success'> <?php if($inboxContentData['approved_piece'] > 0){ echo $inboxContentData['approved_piece']; }?></td>
							 <td> <?php echo $inboxContentData['status']; ?> </td>
							 <td> <?php echo $inboxContentData['orderMessage']; ?> </td>
							 <input type='hidden' name='id' value="<?php echo $inboxContentData['id']; ?>" />

							 <td class='text-center'><a data-toggle="modal" data-target=".updateStatus<?php echo $line ?>"><i class='fa fa-edit'></i></a></td>
						 </tr>

						 <!-- STATUS MODAL -->




							 <div class="modal fade updateStatus<?php echo $line ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
								 <form method='POST' action="<?=site_url('warehouse_request/inboxContent')?>">

							   <div class="modal-dialog modal-md">
							     <div class="modal-content">
										 <div class="modal-header">
							         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							         <h4 class="modal-title"><?php echo $inboxContentData['product_name']; ?></h4>
							       </div>
										 <div class="modal-body">
											 		<div class='row'>
														<div class='col-sm-4'>
															<div class="form-group">
														    <label>Box</label>
														    <input type="number" value="<?php echo $inboxContentData['approved_box']; ?>" name='approved_box' class="form-control" placeholder="Approved Box" min=0 step=1 />
														  </div>
														</div>
														<div class='col-sm-4'>
															<div class="form-group">
														    <label >Pack</label>
														    <input type="number" value="<?php echo $inboxContentData['approved_pack']; ?>" name='approved_pack' class="form-control" placeholder="Approved Pack" min=0 step=1 />
														  </div>
														</div>
														<div class='col-sm-4'>
															<div class="form-group">
														    <label >Piece</label>
														    <input type="number" value="<?php echo $inboxContentData['approved_piece']; ?>" name='approved_piece' class="form-control" placeholder="Approved Piece" min=0 step=1 />
														  </div>
														</div>
													</div>
													<input type='hidden' name='request_id' value="<?php echo $request_id; ?>" />
				 								 	<input type='hidden' name='id' value="<?php echo $inboxContentData['id']; ?>" />
													<input type='hidden' name='product_id' value="<?php echo $inboxContentData['product_id']; ?>" />
											</div>
								      <div class="modal-footer">
												<div class="btn-group btn-group-justified" role="group" aria-label="...">
												  <div class="btn-group" role="group">
												    <button type="submit" value='Pending' name='updatePending'  class="btn btn-warning">Pending</button>
												  </div>
												  <div class="btn-group" role="group">
												    <button type="submit" value='Disapproved' name='updateDisapproved' class="btn btn-danger" >Disapproved</button>
												  </div>
												  <div class="btn-group" role="group">
												    <button type="submit" value='Approved' name='updateApproved' class="btn btn-success">Approved</button>
												  </div>
												</div>
											</div>

							     </div>
							   </div>
								 </form>
							 </div>

						<!-- END UPDATE STATUS MODAL -->

					 <?php $line++; endforeach; ?>
				 </tbody>
				 <?php } ?>

			</table>
		</div>

		</div><!-- end panel-->
	</div><!-- end container-->


	<?php if(isset($status_update)){
					if($status_update == "insufficient"){
						echo "
						<div class='container'>
						<div class='alert alert-danger alert-dismissible' role='alert'>
					  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
					  	<strong>Request approval failed!</strong> Your stocks are insufficent.
						</div></div>";
					}
					if($status_update == "approved"){
						echo "
						<div class='container'>
						<div class='alert alert-success alert-dismissible' role='alert'>
					  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
					  	<strong>Request has been successfully APPROVED.</strong> Stock level updated.
						</div></div>";
					}
					if($status_update == "disapproved"){
						echo "
						<div class='container'>
						<div class='alert alert-success alert-dismissible' role='alert'>
					  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
					  	<strong>Request has been successfully DISAPPROVED.</strong> Stock level updated.
						</div></div>";
					}
					if($status_update == "pending"){
						echo "
						<div class='container'>
						<div class='alert alert-warning alert-dismissible' role='alert'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
							<strong>Request is now PENDING.</strong> Stock level not updated.
						</div></div>";
					}
	} ?>




	<!-- FOOTER -->
  <div class="container">
    <ol class="breadcrumb pull-right">
	  <li><a href="#"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</a></li>
	  <li><a href="<?=site_url("warehouse_request/inbox")?>"><i class='fa fa-send fa-fw'></i>&nbsp;Inbox</a></li>
	  <li class='active'>Request Form</li>
	</ol>
  </div>




	<!-- PRINT -->
	<div class='hidden' id='printThis'>
		<div class=''>
			<div class='row'>
				<div class='col-sm-12'>
					<h2 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i> Request Form</h2>
				</div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Sender</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $firstname." ".$lastname; ?></div>
				<div class='col-sm-2 col-xs-2'><strong>Location</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $location; ?></div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Date</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $new_date_format = date('F d, Y', strtotime($date)); ?></div>
				<div class='col-sm-2 col-xs-2'><strong>Request No.</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $request_id; ?></div>
			</div>
		</div>
		<hr>
		<table class='table table-condensed table-stripped table-hover table-bordered' id='productTable'>
			<thead>
				<tr>
					<th colspan='4'><span></span></th>
					<th colspan='3' class='text-center'><span class='text-primary '>Requested</span></th>
					<th colspan='3' class='text-center'><span class='text-success '>Approved</span></th>
					<th colspan='2' class='text-center'><span class='text-success '></span></th>
				</tr>
				<tr>
					<th><span>#</span></th>
					<th><span>Product</span></th>
					<th><span>SKU</span></th>
					<th><span>Category</span></th>
					<th><span class='text-primary'>Box</span></th>
					<th><span class='text-primary'>Pack</span></th>
					<th><span class='text-primary'>Piece</span></th>
					<th><span class='text-success'>Box</span></th>
					<th><span class='text-success'>Pack</span></th>
					<th><span class='text-success'>Piece</span></th>
					<th><span>Status</span></th>
					<th><span>Notes</span></th>

				</tr>
			</thead>
			 <?php if(isset($inboxContent)){ ?>
			 <tbody>
				 <?php  $line = 1; foreach($inboxContent as $inboxContentData): ?>
					 <tr>
						 <td> <?php echo $line; ?> </td>
						 <td> <?php echo $inboxContentData['product_name']; ?> </td>
						 <td> <?php echo $inboxContentData['sku']; ?> </td>
						 <td> <?php echo $inboxContentData['product_type']; ?> </td>
						 <td class='text-primary'> <?php echo $inboxContentData['box']; ?> </td>
						 <td class='text-primary'> <?php echo $inboxContentData['pack']; ?> </td>
						 <td class='text-primary'> <?php echo $inboxContentData['piece']; ?> </td>
						 <td class='text-success'> <?php if($inboxContentData['approved_box'] > 0){ echo $inboxContentData['approved_box']; } ?> </td>
						 <td class='text-success'> <?php if($inboxContentData['approved_pack'] > 0){ echo $inboxContentData['approved_pack']; } ?></td>
						 <td class='text-success'> <?php if($inboxContentData['approved_piece'] > 0){ echo $inboxContentData['approved_piece']; } ?></td>
						 <td> <?php echo $inboxContentData['status']; ?> </td>
						 <td> <?php echo $inboxContentData['orderMessage']; ?> </td>

					 </tr>
				 <?php $line++; endforeach; ?>
			 </tbody>
			 <?php } ?>

		</table>
	</div>

	<!-- PICK LIST -->


	<div class='hidden' id='pickList'>
		<div class=''>
			<div class='row'>
				<div class='col-sm-12'>
					<h2 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i> Pick list</h2>
				</div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Sender</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $firstname." ".$lastname; ?></div>
				<div class='col-sm-2 col-xs-2'><strong>Location</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $location; ?></div>
			</div>
			<div class='row'>
				<div class='col-sm-2 col-xs-2'><strong>Date</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $new_date_format = date('F d, Y', strtotime($date)); ?></div>
				<div class='col-sm-2 col-xs-2'><strong>Request No.</strong></div>
				<div class='col-sm-4 col-xs-4'><?php echo $request_id; ?></div>
			</div>
		</div>
		<hr>
		<table class='table table-condensed table-stripped table-hover table-bordered' id='productTable'>
			<thead>
				<tr>
					<th colspan='4'><span></span></th>
					<th colspan='3' class='text-center'><span class='text-primary '>Requested</span></th>
					<th colspan='3' class='text-center'><span class='text-success '>Picked</span></th>
					<th class='text-center'><span class='text-success '></span></th>
				</tr>
				<tr>
					<th><span>#</span></th>
					<th><span>Product</span></th>
					<th><span>SKU</span></th>
					<th><span>Category</span></th>
					<th><span class='text-primary'>Box</span></th>
					<th><span class='text-primary'>Pack</span></th>
					<th><span class='text-primary'>Piece</span></th>
					<th><span class='text-success'>Box</span></th>
					<th><span class='text-success'>Pack</span></th>
					<th><span class='text-success'>Piece</span></th>

					<th><span>Notes</span></th>

				</tr>
			</thead>
			 <?php if(isset($inboxContent)){ ?>
			 <tbody>
				 <?php  $line = 1; foreach($inboxContent as $inboxContentData): ?>
					 <tr>
						 <td> <?php echo $line; ?> </td>
						 <td> <?php echo $inboxContentData['product_name']; ?> </td>
						 <td> <?php echo $inboxContentData['sku']; ?> </td>
						 <td> <?php echo $inboxContentData['product_type']; ?> </td>
						 <td class='text-primary'> <?php echo $inboxContentData['box']; ?> </td>
						 <td class='text-primary'> <?php echo $inboxContentData['pack']; ?> </td>
						 <td class='text-primary'> <?php echo $inboxContentData['piece']; ?> </td>
						 <td class='text-success'> <?php if($inboxContentData['approved_box'] > 0){echo $inboxContentData['approved_box']; }?> </td>
						 <td class='text-success'> <?php if($inboxContentData['approved_pack'] > 0){echo $inboxContentData['approved_pack']; }?></td>
						 <td class='text-success'> <?php if($inboxContentData['approved_pack'] > 0){echo $inboxContentData['approved_piece']; }?></td>

						 <td> <?php echo $inboxContentData['orderMessage']; ?> </td>

					 </tr>
				 <?php $line++; endforeach; ?>
			 </tbody>
			 <?php } ?>

		</table>
	</div>
