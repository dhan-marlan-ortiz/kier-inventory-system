<style> .short{ max-width: 80px; min-width: 80px; } 	</style>

<form method="POST" action="<?=site_url('warehouse/delivery_submit')?>" id="orderList" name='delivery_invoice'>

	<div class='container panel panel-default'>
		<div class='page-header'>
			<?php foreach ($suppliers as $supplier): ?>
				<h2 class='text-primary'><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Delivery Invoice</h2>
				<div class='row'>
					<div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'>
						<address>
						  <strong id="supplier"><?php echo $supplier['name']; ?></strong><br>
						  <?php echo $supplier['address']; ?>
						</address>
					</div>
					<div class='col-xs-6 col-sm-8 col-md-8 col-lg-8'>
						<dl class="dl-horizontal pull-right">
						  <dt id="invoice">Invoice No. : </dt><dd><?php echo $invoice; ?></dd>
						  <dt>Date : </dt><dd id="date"></dd>
						</dl>
					</div>
				</div>

				<input type='hidden' name='delivery_supplier' value='<?php echo $supplier['name']; ?>' />
				<input type='hidden' name='delivery_address' value='<?php echo $supplier['address']; ?>' />
				<input type='hidden' name='delivery_invoice' value='<?php echo $invoice; ?>' />
				<input type='hidden' name='delivery_date' id='deliveryDate' />

			<?php endforeach; ?>
		</div>

		<div class='panel panel-default'>
			<div class='panel-heading'>
				<h4>
					<span class='text-primary'>Products</span>
					<span class='pull-right'>
						<a class='btn btn-link' data-toggle="modal" data-target=".selectSupplierProduct"><i class='fa fa-plus-circle'></i>&nbsp;Add Product</a>
						<a class='btn btn-link' onclick="deleteProduct('productTable'); getTotals();"><i class='fa fa-times-circle'></i>&nbsp;Remove Product</a>
					</span>
				</h4>
			</div>
			<div class='panel-body'>
				<div class='table-responsive'>
					<table class="table" id='productTable'>
						<tr class='text-success bg-info '>
							<th>Select</th><th>SKU</th><th>Product Name</th><th>Description</th><th>Quantity</th><th>Unit</th><th>Unit Cost</th><th>Discount</th><th>Sub Total</th>
							<th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th>
						</tr>
					</table>
				</div>
			</div>
			<div class='panel-footer'>
				<div class='row'>

					<div class='col-sm-2 text-primary'>
						Product Count
					</div>
					<div class='col-sm-2'>
						<strong  id='totalCount'> </strong>
					</div>

					<div class='col-sm-2 text-primary'>
						Total Discount
					</div>
					<div class='col-sm-2' >
						<strong id='totalDiscount'></strong>
					</div>

					<div class='col-sm-2 text-danger'>
						Total Cost
					</div>
					<div class='col-sm-2' >
						<strong id='totalCost'></strong>
					</div>
				</div>
			</div>
		</div>

		<div class='panel'>
			<div class='row'>

				<div class='col-sm-2 '>
					<a class="btn btn-default btn-block" href="<?=site_url('warehouse/inventory')?>" ><i class='fa fa-remove'></i>&nbsp;Cancel</a>
				</div>
				<div class='col-sm-2 col-sm-offset-6'>
					<button class="btn btn-default btn-block" type='submit' name='back'><i class='fa fa-hand-o-left'></i>&nbsp;Back</button>
				</div>
				<div class='col-sm-2'>
					<a href="javascript: submitForm()" name='submit' class="btn btn-primary btn-block"><i class='fa fa-save'></i>&nbsp;Save</a>
				</div>

			</div>
		</div>


</div>
</form>

<!-- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -->

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
	  <li><a href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</a></li>
	  <li><a href="<?=site_url('warehouse/delivery1')?>"><i class='fa fa-truck fa-fw'></i>&nbsp; Delivery</a></li>
	  <li class="active"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp; Delivery Invoice</li>
	</ol>
</div>

<!--SEARCH PRODUCT-->
<div class="modal fade selectSupplierProduct" tabindex="-1" role="dialog">

	  <div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div id="users">

				<div class='modal-body'>
					<div class='container-fluid'>

						<div class='page-header'>
							<div class='row'>
								<div class='col-sm-6'>
									<h1 class='text-primary'>Select Product</h1>
								</div>
								<div class='col-sm-6'>
									<div class="input-group">
									  <input class="search form-control" placeholder="Search product name or SKU" />
									  <span class="input-group-btn">
										<button class="sort btn btn-default" data-sort="name"><i class='fa fa-sort-alpha-desc'></i>&nbsp;Sort</button>
									  </span>
									</div><!-- /input-group -->
								</div>
							</div>
						</div>

						<div class='panel panel-default'>
							<?php if(isset($supplierProduct)){ ?>

							<div class='panel-heading'>
								<div class=' container-fluid'>
									<div class='row'>
										<div class="col-sm-2 col-xs-2 col-md-2 col-lg-2"><strong>SKU</strong></div>
										<div class="col-sm-4 col-xs-4 col-md-4 col-lg-4"><strong>Product Name</strong></div>
										<div class="col-sm-3 col-xs-3 col-md-3 col-lg-3"><strong>Description</strong></div>
										<div class="col-sm-2 col-xs-2 col-md-2 col-lg-2"><strong></strong></div>
									</div>
								</div>
							</div>

							<div class='panel-body'>
								<div class='table-responsive' style='max-height: 300px; overflow: auto'>
									<div class="container-fluid list">
										<?php foreach($supplierProduct as $product_detail): ?>
											<div class='row' style='border-bottom: solid 1px #D3D3D3;'>
												<div class="sku col-sm-2 col-xs-2 col-md-2 col-lg-2"><?php echo $product_detail['sku']; ?></div>
												<div class="name col-sm-4 col-xs-4 col-md-4 col-lg-4"><?php echo $product_detail['product_name']; ?></div>
												<div class="col-sm-3 col-xs-3 col-md-3 col-lg-3"><?php echo $product_detail['product_description']; ?></div>
												<div class="col-sm-2 col-xs-2 col-md-2 col-lg-2">
													<a 	data-dismiss="modal" data-toggle="modal" data-target=".billing"
														onclick="getProductInfo('<?php echo $product_detail["sku"]; ?>', '<?php echo $product_detail["product_name"]; ?>', '<?php echo $product_detail["product_description"]; ?>');"
														class='btn btn-link pull-right'>
														<i class='fa fa-plus-circle'></i>&nbsp;Add
													</a>
												</div>
											</div>
										<?php  endforeach; ?>
									</div>
								</div>
							</div>

							<?php }else { ?> No Products <?php } ?>
						</div>

						<div class='row'>
							<div class='col-sm-12'>
								<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
							</div>
						</div>

						</div><!--end container fluid-->
					</div><!--end modal body-->

			</div><!---end user-->
		</div>
	  </div>

</div><!--end search product-->

<!--BILLING-->
<div class="modal billing" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">

				<div class='container-fluid'>
					<div class='page-header'>
						<div class='panel panel-default'>
							<table class='table'>
								<tr><th id='addSku'>SKU</th><td id='item_sku'></td></tr>
								<tr><th id='addName'>Product Name</th><td id='item_name'></td></tr>
								<tr><th id='addDescription'>Description</th><td id='item_description'></td></tr>
							</table>
						</div>
					</div>
					<div class='panel panel-default'>
						<table class='table'>
							<tr>
								<th class='col-sm-6 col-md-6 col-lg-6'>Quantity</th>
								<td class='col-sm-6 col-md-6 col-lg-6'><input type='number' id='quantity' step='1' min='0' class='form-control' placeholder='Product Quantity' required/></td>
							</tr>
							<tr><th class='col-sm-6 col-md-6 col-lg-6'>Unit</th>
								<td class='col-sm-6 col-md-6 col-lg-6'>
									<select id='unit' class='form-control'>
										<option>Box</option>
										<option>Pack</option>
										<option>Piece</option>
									</select>
								</td>
							</tr>
							<tr>
								<th class='col-sm-6 col-md-6 col-lg-6'>Unit Price</th>
								<td class='col-sm-6 col-md-6 col-lg-6'><input id='price' type="number" step='1' min='0' class='form-control' placeholder="Product Unit Price" required /></td>
							</tr>
							<tr>
								<th class='col-sm-6 col-md-6 col-lg-6'>Discount Amount</th>
								<td class='col-sm-6 col-md-6 col-lg-6'><input id='discount' class='form-control' type="number" step='1' min='0' value='0' placeholder="Product Discount Amount" /></td>
							</tr>
							<tr>
								<th class='col-sm-6 col-md-6 col-lg-6'>Sub Total</th>
								<td class='col-sm-6 col-md-6 col-lg-6'>
									<div class="input-group">
									  <span class="input-group-btn">
										<a class='btn btn-default' onclick='getSubTotal();'>Get Sub Total</a>
									  </span>
									  <input id='subTotal' type="number" class='form-control' step='1' min='0'  placeholder="0" required />
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div class='panel panel-default'>
						<div class="btn-group btn-group-justified" role="group" aria-label="...">
						  <div class="btn-group" role="group">
							<button type="button" onclick='clearBillingFields();' class="btn btn-default" data-dismiss="modal"><i class='fa fa-remove'></i>&nbsp;Cancel</button>
						  </div>
						  <div class="btn-group" role="group">
							<a onclick='addProduct();' class="btn btn-success" data-dismiss="modal"><i class='fa fa-check'></i>&nbsp;Ok</a>
						  </div>
						</div>
					</div>
				 </div>
		</div>
	</div>
</div>

<!--MODAL INVOICE SUMMARY-->
<div class="modal fade modalInvoiceSummary" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
	  <table class="table" id='invoiceSummaryTable'>
		<tr class='text-success'>
			<th>#</th><th>SKU</th><th>Product Name</th><th>Description</th><th>Quantity</th><th>Unit</th><th>Unit Cost</th><th>Discount</th><th>Sub Total</th>
		</tr>
	  </table>
	</div>
  </div>
</div>

<script type="text/javascript">
	function submitForm(){
		var table = document.getElementById("productTable");
		var rowCount = table.rows.length;

		if (rowCount > 1){
			document.delivery_invoice.submit();
		}else
			alert("Please Select Products");
	}

	function disp(){
		var show = $('.modalInvoiceSummary').modal('show');
	}

	function getInvoiceSummary(){

		var table = document.getElementById("productTable");
		var rowCount = table.rows.length-1;
			for(var i=1; i<=rowCount; i++){
				for(var j=1; j<=8; j++){

					var table2 = document.getElementById("invoiceSummaryTable");

					var rowCount2 = table2.rows.length;
					var row = table2.insertRow(rowCount2);

					var cell2 = row.insertCell(1);
					cell2.innerHTML = "hello";
				}
			}

			var show = $('.modalInvoiceSummary').modal('show');
	}



	function clearBillingFields(){
		document.getElementById("quantity").value = "";
		document.getElementById("price").value = "";
		document.getElementById("discount").value = "";
		document.getElementById("subTotal").value = "";
	}

	function getSubTotal(){
		var quantity = document.getElementById("quantity").value;
		var price = document.getElementById("price").value;
		var discount = document.getElementById("discount").value;
		var total = quantity*price-discount;
		document.getElementById("subTotal").value = total;
	}

	function getProductInfo(sku, name, desc){
		document.getElementById("item_name").innerHTML = name;
		document.getElementById("item_description").innerHTML = desc;
		document.getElementById("item_sku").innerHTML = sku;

	}

	function getTotals(){
		var totalCost = 0;
		var totalDiscount = 0;

		var table = document.getElementById("productTable");
		var rowCount = table.rows.length-1;

		for(var i=1; i<=rowCount; i++){
			totalCost = totalCost + parseFloat(table.rows[i].cells[8].innerHTML);
			totalDiscount = totalDiscount + parseFloat(table.rows[i].cells[7].innerHTML);
		}

		

		document.getElementById("totalCount").innerHTML = rowCount;
		document.getElementById("totalDiscount").innerHTML = totalDiscount;
		document.getElementById("totalCost").innerHTML = totalCost;
	}

	function addProduct(){

			var table = document.getElementById("productTable");

			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);

			//checkbox
			var cell1  = row.insertCell(0);
			var element1 = document.createElement("input");
			element1.type = "checkbox";
			element1.name = "checkbox[]";
			cell1.appendChild(element1);

			//SKU
			var cell2 = row.insertCell(1);
			cell2.innerHTML = document.getElementById("item_sku").innerHTML;

			//NAME
			var cell3 = row.insertCell(2);
			cell3.innerHTML = document.getElementById("item_name").innerHTML;

			//DESC
			var cell4 = row.insertCell(3);
			cell4.innerHTML = document.getElementById("item_description").innerHTML;

			//QUANTITY
			var cel5 = row.insertCell(4);
			cel5.innerHTML = document.getElementById("quantity").value;


			//UNIT
			var cell6 = row.insertCell(5);
			cell6.innerHTML = document.getElementById("unit").value;

			//COST
			var cell7 = row.insertCell(6);
			cell7.innerHTML = document.getElementById("price").value;


			//DISCOUNT
			var cell8 = row.insertCell(7);
			cell8.innerHTML = document.getElementById("discount").value;

			//SUB TOTAL
			var cell9 = row.insertCell(8);
			cell9.innerHTML = document.getElementById("subTotal").value;

			//SKU HIDDEN 10
			var cell10 = row.insertCell(9);
			var element10 = document.createElement("input");
			element10.type = "hidden";
			element10.name = "sku[]";
			element10.value = document.getElementById("item_sku").innerHTML;
			cell10.appendChild(element10);

			//QUANTITY HIDDEN 11
			var cell11 = row.insertCell(10);
			var element11 = document.createElement("input");
			element11.type = "hidden";
			element11.name = "quantity[]";
			element11.value = cel5.innerHTML;
			cell11.appendChild(element11);

			//UNIT HIDDEN 12
			var cell12 = row.insertCell(11);
			var element12 = document.createElement("input");
			element12.type = "hidden";
			element12.name = "unit[]";
			element12.value = document.getElementById("unit").value;
			cell12.appendChild(element12);

			//COST HIDDEN 13
			var cell13 = row.insertCell(12);
			var element13 = document.createElement("input");
			element13.type = "hidden";
			element13.name = "price[]";
			element13.value = document.getElementById("price").value;
			cell13.appendChild(element13);

			//DISCOUNT HIDDEN 14
			var cell14 = row.insertCell(13);
			var element14 = document.createElement("input");
			element14.type = "hidden";
			element14.name = "discount[]";
			element14.value = document.getElementById("discount").value;
			cell14.appendChild(element14);

			//SUBTOTAL HIDDEN 15
			var cell15 = row.insertCell(14);
			var element15 = document.createElement("input");
			element15.type = "hidden";
			element15.name = "subtotal[]";
			element15.value = cell9.innerHTML;
			cell15.appendChild(element15);

			//DATE HIDDEN 16
			var cell16 = row.insertCell(15);
			var element16 = document.createElement("input");
			element16.type = "hidden";
			element16.name = "date[]";
			element16.value = document.getElementById("date").innerHTML;
			cell16.appendChild(element16);

			//INVOICE HIDDEN 17
			var cell17 = row.insertCell(16);
			var element17 = document.createElement("input");
			element17.type = "hidden";
			element17.name = "invoice[]";
			element17.value = document.getElementById("invoice").innerHTML;
			cell17.appendChild(element17);

			//SUPPLIER 18
			var cell18 = row.insertCell(17);
			var element18 = document.createElement("input");
			element18.type = "hidden";
			element18.name = "supplier[]";
			element18.value = document.getElementById("supplier").innerHTML;
			cell18.appendChild(element18);

			//FILL EMPTY FIELDS WITH 0
			try{
				var fillRowCount = table.rows.length;
				for(var i=1; i<fillRowCount; i++){
					for(var j=4; j<=8; j++){
						if(table.rows[i].cells[j].innerHTML == ""){
							table.rows[i].cells[j].innerHTML = 0;
						}
					}
				}
			}catch(e){
				alert(e);
			}

			getTotals();
			clearBillingFields();
		}

		function deleteProduct(tableID){
			try{
				var table = document.getElementById(tableID);
				var rowCount = table.rows.length;

					for(var i=0; i<rowCount; i++){
						var row = table.rows[i];
						var chkbox = row.cells[0].childNodes[0];

						if(null != chkbox && true == chkbox.checked){
							table.deleteRow(i);
							rowCount--;
							i++;
						}
					}
				}catch(e){
					alert(e);
				}

		}







	function addSupplier(supplierName){ document.getElementById('selectedSupplier').innerHTML = supplierName;}
	var options = { valueNames: ['sku', 'name'] };
	var userList = new List('users', options);

	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth();
	var day = d.getDate();
	var monthInWords = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
	document.getElementById("date").innerHTML = monthInWords[month]+' '+day+', '+year;
	document.getElementById("deliveryDate").value = monthInWords[month]+' '+day+', '+year;

</script>
