<!-- HEADER -->
<div class='container'>
	<div class='page-header'>
		<span class=' text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
  </div>
</div>

<div class='container'>
  <div class='row'>
    <div class='col-sm-6 col-sm-offset-3'>


            <table class='table'>
              <thead><tr><th colspan=2><h4 class='text-primary'><strong>Product Category</strong></h4></th></tr></thead>
              <tbody>
                  <?php if(isset($category)){
													$i=1;
                          foreach($category as $c){
                            echo "<tr>";
                            echo "<td>".$c['product_type']."</td>";
                            echo "<td><a data-toggle='modal' data-target='.edit".$i."'><i class='fa fa-edit'></i> Edit</a></td>";
                            echo "</tr>";
									?>
														<form method='post' action="<?=site_url('warehouse/Category')?>">
															<div class="modal fade edit<?=$i?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
															  <div class="modal-dialog modal-sm">
															    <div class="modal-content">
																		<div class="modal-header">
																			 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																			 <h4 class="modal-title" id="myModalLabel"><?=$c['product_type']?></h4>
																		 </div>

																		 <div class='modal-body'>
																			 <input type='text' name='newCategory' class='form-control' placeholder='Please enter new category' required />
																		 </div>

																		 <div class='panel-footer'>
																			 <button data-dismiss='modal' class='btn btn-default'><i class='fa fa-remove'></i> Close</button>
																			 <input type='hidden' name='oldCategory' value="<?=$c['product_type']?>" />
																			 <input type='submit' name='edit' class='btn btn-success pull-right' value="Save" />
																		 </div>

															    </div>
															  </div>
															</div>
													</form>
									<?php
													$i++;
                          }
                        } ?>
                </tbody>
              </table>
              <a href="<?=site_url('warehouse/inventory')?>" class="btn btn-default"><i class='fa fa-remove'></i> Close</a>
            </form>
        </div>
      </div>
</div>

<!-- Small modal -->
