

<form method="POST" action="<?=site_url('warehouse/delivery_submit')?>" id="orderList" name='delivery_invoice'> 
	
	<div class='container panel panel-default'>
		<div class='page-header'>
			
				<h2 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i>&nbsp;Request Form</h2>
				<div class='row'>
					<div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'>
						<address>
						  <strong> <!-- SUPPLIER NAME --> </strong><br>
						  <!-- SUPPLIER ADDRESS -->
						</address>
					</div>
					<div class='col-xs-6 col-sm-8 col-md-8 col-lg-8'>
						<dl class="dl-horizontal pull-right">
						  <dt id="invoice">Request No. : </dt><dd> <!-- REQUEST NO. --> </dd>
						  <dt>Date : </dt> <!-- DATE --> <dd></dd>
						</dl>
					</div>
				</div>
				
		</div>
		
		<div class='panel panel-default'>
			<div class='panel-heading'>
				<h4>
					<span class='text-primary'>Products</span>
					<span class='pull-right'>
						<!-- ADD PRODUCT BUTTON -->
						<!-- ADD DELETE PRODUCT BUTTON -->
					</span>
				</h4>
			</div>
			<div class='panel-body'>
				<div class='table-responsive'>
					<table class="table" id='productTable'>
						<tr class='text-success bg-info '>
							<th>Select</th><th>SKU</th><th>Product Name</th><th>Description</th><th>Quantity</th><th>Unit</th><th>Unit Cost</th><th>Discount</th><th>Sub Total</th>
							<th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th>
						</tr>
					</table>
				</div>
			</div>
			<div class='panel-footer'>
			</div>
		
			<div class='panel'>
				<!--BACK AND NEXT BUTTON-->
			</div>
		</div>
		
	
	</div>
</form>

<!-- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -->

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
	  <li><a href="<?=site_url('warehouse/inventory')?>"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory</a></li>
	  <li><a href="<?=site_url('warehouse/delivery1')?>"><i class='fa fa-truck fa-fw'></i>&nbsp; Delivery</a></li>
	  <li class="active"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp; Delivery Invoice</li>
	</ol>
</div>







