
<!-- SELECT ORDER TYPE -->
<div class="container">
	<div class='row'>
		<div class='col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4'>	

			<div class='page-header'>
				<span class=' text-primary h2'>Order Type </span>
			</div>

			<div class='panel'>
				<form method='post' action='<?=site_url('warehouse_createOrder/selectSupplier')?>'>
					<button type='submit' name='supplier' value='supplier' class='btn btn-primary btn-block btn-lg'>Order From Supplier</button>
				</form>
			</div>

			<div class='panel'>
				<form method='post' action='<?=site_url('warehouse_createOrder/selectBranch')?>'>
					<button type='submit' name='branch' value='branch' class='btn btn-success btn-block btn-lg'>Pull-Out From Branch</button>
				</form>
			</div>

		</div>
	</div>
</div>
	
<div class="container">
	<ol class="breadcrumb pull-right">
		<li><a href="#"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</a></li>
		<li><a href="#"><i class='fa fa-list-alt'></i>&nbsp;Create Order</a></li>
		<li class="active">Order Type </li>
	</ol>
</div>

	