<!--PAGE HEADER-->
<div class='container' >
	<div class='page-header'>
		<div class='row'>
			<div class='col-sm-12'>
				<span class='text-success h3'><i class='fa fa-check-square-o fa-fw'></i> Request </span>
				<div class='btn-group pull-right'>
					<a href="<?=site_url('warehouse_request/create')?>" class="btn btn-default active" > <i class='fa fa-list-alt fa-fw'>&nbsp;</i>Create</a>
					<a href="<?=site_url('warehouse_request/inbox')?>" class="btn btn-default"> <i class='fa fa-inbox fa-fw'>&nbsp;</i>Inbox</a>
					<a href="<?=site_url('warehouse_request/outbox')?>" class="btn btn-default"class="btn btn-default"> <i class='fa fa-send-o fa-fw'>&nbsp;</i>Outbox</a>
					<a href="#" class="btn btn-default"> <i class='fa fa-recycle fa-fw'>&nbsp;</i>Trash</a>
				</div>
			</div>
		</div>
	</div>
</div>
