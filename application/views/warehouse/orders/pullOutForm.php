<style> .short{ max-width: 80px; min-width: 80px; } </style>



<form method='post' action="<?=site_url('warehouse_createOrder/submitOrder')?>" name='productForm'>

	<div class='container panel panel-default'>
		<div class='page-header'>

				<h2 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i>&nbsp;Request Form</h2>
				<div class='row'>
					<div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'>
						<div class='row'>
							<div class='col-sm-1'>
								<strong class='text-primary'>To: </strong>
							</div>
							<div class='col-sm-11'>
								<address>	<strong><?php echo $orderReceiver;  ?> </strong>	</address>
								<input type='hidden' name='supplier' value="<?php echo $orderReceiver; ?>" />
							</div>
						</div>
					</div>
					<div class='col-xs-6 col-sm-8 col-md-8 col-lg-8'>
						<dl class="dl-horizontal pull-right">
							<dt id="invoice" class='text-primary'>Request No. : </dt><dd> <?php echo $requestNo = uniqid(); ?> </dd>
							<input type='hidden' name='requestNo' value='<?php echo $requestNo; ?>' />
							<dt class='text-primary'>Date : </dt> <!-- DATE --> <dd id='date'></dd>
						</dl>
					</div>
				</div>

		</div>

		<div class='panel panel-default'>
			<div class='panel-heading'>
				<h4>
					<span class='text-primary'>Products</span>
					<span class='pull-right'>
						<a type="button" class="btn btn-link" data-toggle="modal" data-target="#addProductModal">
						  <i class='fa fa-plus-circle'></i>&nbsp;Add Product
						</a>
						<a class='btn btn-link' type='button' onclick="deleteRow('dataTable')"><i class='fa fa-remove'></i>&nbsp;Remove </a>
					</span>
				</h4>
			</div>
			<div class='panel-body'>
				<div class='table-responsive'>
					<table  class='table table-condensed table-hover' id="dataTable">
						<tr class='bg-info'>
							<th></th>
							<th><span class='text-success'>Name</span></th>
							<th><span class='text-success'>SKU</span></th>
							<th><span class='text-success'>Category</span></th>
							<th><span class='text-success'>Box</span></th>
							<th><span class='text-success'>Pack</span></th>
							<th><span class='text-success'>Piece</span></th>
							<th><span class='text-success'>Note</span></th>
							<th></th>
						</tr>
					</table>
				</div>
			</div>
			<div class='panel-footer'>
			</div>
		</div>

		<div class='panel'>
			<div class='row'>
				<div class='col-sm-2 '>
					<a class="btn btn-default btn-block" href="<?=site_url('warehouse_request/inbox')?>" ><i class='fa fa-remove'></i>&nbsp;Cancel</a>
				</div>
				<div class='col-sm-2 col-sm-offset-6'>
					<a href="<?=site_url('warehouse_createOrder/selectSupplier')?>" class='btn btn-default btn-block' type='submit'><i class='glyphicon glyphicon-chevron-left pull-left'></i>&nbsp;Back </a>
				</div>
				<div class='col-sm-2'>
					<button class='btn btn-primary  btn-block' type="button" onClick="javascript:next();"> Next&nbsp;<i class='glyphicon glyphicon-chevron-right'></i></button>
				</div>
			</div>
		</div>

	</div>

</form>

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
	  <li><a href="<?=site_url('')?>"><i class='fa fa-check-square-o fa-fw'></i>&nbsp; Request</a></li>
	  <li><a href=""><i class='fa fa-list-alt fa-fw'></i>&nbsp; Create Request </a></li>
	  <li class='active'>Request Form</li>
	</ol>
</div>

<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div id="users">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel"><i class='fa fa-shopping-cart'></i> Product List</h4>
	      </div>
	      <div class="modal-body">

						<div class="list-group" style='max-height: 300px; overflow: auto'>

							<div class='container-fluid'>
								<div class='list-group-item row'>
									<div class='col-sm-4 col-xs-4'><strong>	Product </strong></div>
									<div class='col-sm-2 col-xs-2'><strong> SKU </strong></div>
									<div class='col-sm-2 col-xs-2'><strong> Category </strong></div>
									<div class='col-sm-1 col-xs-1'><strong> Box </strong></div>
									<div class='col-sm-1 col-xs-1'><strong> Pack </strong></div>
									<div class='col-sm-1 col-xs-1'><strong> Piece </strong></div>
									<div class='col-sm-1 col-xs-1'><strong> Action </strong></div>
								</div>
							</div>

							<div class='list'>

								<?php $line_number = 1;  $i=0; foreach($product as $product_detail): ?>
									<div class='container-fluid'>
										<div class="list-group-item row">
											<div class='name col-sm-4 col-xs-4'><?php echo $name = $product_detail['product_name']; ?></div>
											<div class='sku col-sm-2 col-xs-2'><?php echo $sku = $product_detail['sku']; ?></div>
											<div class='type col-sm-2 col-xs-2'><?php echo $type = $product_detail['product_type']; ?></div>
											<div class='col-sm-1 col-xs-1'><?php echo $product_detail['box_stock']; ?></div>
											<div class='col-sm-1 col-xs-1'><?php echo $product_detail['pack_stock']; ?></div>
											<div class='col-sm-1 col-xs-1'><?php echo $product_detail['piece_stock']; ?></div>
											<?php	$id = $product_detail['product_id'];	?>
											<div class='sku col-sm-1 col-xs-1'>
												<a class="btn btn-link " data-dismiss='modal' onclick="addRow('dataTable', '<?php echo $sku; ?>',  '<?php echo $name; ?>',  '<?php echo $type; ?>', '<?php echo $id; ?>')">
													Add
												</a>
											</div>
										</div>
									</div>

								<input type='hidden' name='id' value="<?php echo $product_detail['product_id']?>">
								<?php $line_number++; $i++; endforeach ?>

							</div>
						</div>
					</div>
	      <div class="modal-footer">
					<div class="input-group">
						<span class="input-group-addon"><i class='fa fa-search'></i> Search</span>
						<input type="search" class="search form-control" placeholder="Search product, SKU, or Category">
					</div>
	      </div>
			</div>
    </div>
  </div>
</div>










<script language="javascript">
var options = { valueNames: [ 'name', 'sku', 'type' ] };
var userList = new List('users', options);


	    function next()
	   {
		 var rows = document.getElementById("dataTable").rows.length;
		 if(rows > 1){
			 document.productForm.submit();

		 }
		 else{
			alert('No product selected.');
		 }

	   }


		function enableNext() {
            document.getElementById("next").disabled = false;
        }

		function addRow(tableID, sku, name, type, id){
			var table = document.getElementById(tableID);

			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);

			//checkbox
			var cell1  = row.insertCell(0);
			var element1 = document.createElement("input");
			element1.type = "checkbox";
			element1.name = "checkbox[]";
			cell1.appendChild(element1);

			//SKU
			var cell2 = row.insertCell(1);
			cell2.innerHTML = name;

			//name
			var cell3 = row.insertCell(2);
			cell3.innerHTML = sku;

			//type
			var cell4 = row.insertCell(3);
			cell4.innerHTML = type;

			//box
			var cell5 = row.insertCell(4);
			var element5 = document.createElement("input");
				element5.type = "number";
				element5.name = "box[]";
				element5.placeholder = 0;
				element5.step = 1;
				element5.min = 0;
				element5.className = "form-control short input-sm"
			cell5.appendChild(element5);


			// pack
			var cell6 = row.insertCell(5);
			var element6 = document.createElement("input");
				element6.type = "number";
				element6.name = "pack[]";
				element6.placeholder = 0;
				element6.step = 1;
				element6.min = 0;
				element6.className = "form-control short input-sm"
			cell6.appendChild(element6);

			// piece
			var cell7 = row.insertCell(6);
			var element7 = document.createElement("input");
			element7.type = "number";
			element7.name = "piece[]";
				element7.placeholder = 0;
				element7.step = 1;
				element7.min = 0;
				element7.className = "form-control short input-sm"
			cell7.appendChild(element7);


			//note
			var cell8 = row.insertCell(7);
			var element8 = document.createElement("input");
			element8.type = "text";
			element8.name = "note[]";
				element8.placeholder = "Note";
				element8.className = "form-control input-sm"
			cell8.appendChild(element8);

			//id
			var cell9 = row.insertCell(8);
			var element9 = document.createElement("input");
			element9.type = "hidden";
			element9.name = "product_id[]";
			element9.value = id;
			cell9.appendChild(element9);
		}

		function deleteRow(tableID){
			try{
				var table = document.getElementById(tableID);
				var rowCount = table.rows.length;

					for(var i=0; i<rowCount; i++){
						var row = table.rows[i];
						var chkbox = row.cells[0].childNodes[0];

						if(null != chkbox && true == chkbox.checked){
							table.deleteRow(i);
							rowCount--;
							i++;

						}
					}
				}catch(e){
					alert(e);
				}
		}







		//get current date

		var d = new Date();
		var year = d.getFullYear();
		var month = d.getMonth();
		var day = d.getDate();
		var monthInWords = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
		document.getElementById("date").innerHTML = monthInWords[month]+' '+day+', '+year;
		document.getElementById("deliveryDate").value = monthInWords[month]+' '+day+', '+year;

	</script>
