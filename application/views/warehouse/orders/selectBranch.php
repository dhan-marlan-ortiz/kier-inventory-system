

<form method='post' action="<?=site_url('warehouse_createOrder/pullOutForm')?>">
	<div class='container'>
		<div class='row'>
			<div class='col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4'>

				<div class='page-header'>
					<span class=' text-primary h2'>Select Store Branch </span>
				</div>

				<div class='panel'>
					<select class="form-control " name='orderReceiver' id="branch" required>
						<option value='' disabled selected>Where do you want your request to be sent?</option>
						<option value='Kier Enterprises Irosin'>Kier Enterprises Irosin</option>
						<option value='Kier Enterprises Gubat'>Kier Enterprises Gubat</option>
						<option value='Kier Enterprises Casiguran'>Kier Enterprises Casiguran</option>

					</select>
				</div>

				<div class='row'>
					<div class='col-sm-6 col-xs-6 col-md-6'>
						<a href="<?=site_url('warehouse_createOrder/create')?>" class='btn btn-default btn-block' type='submit'><i class='glyphicon glyphicon-chevron-left pull-left'></i>&nbsp;Back </a>
					</div>
					<div class='col-sm-6 col-xs-6 col-md-6'>
						<button class='btn btn-primary btn-block' type='submit'> Next&nbsp;<i class='glyphicon glyphicon-chevron-right'></i></button>
					</div>
				</div>

			</div>
		</div>
	</div>
</form>

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
		<li><a href="#"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</a></li>
		<li><a href="#"><i class='fa fa-list-alt'></i>&nbsp;Create Order</a></li>
		<li class="active">Select Branch </li>
	</ol>
</div>
