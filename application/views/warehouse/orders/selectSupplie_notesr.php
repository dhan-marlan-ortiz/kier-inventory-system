 <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
			  <h4 class="panel-title">
				<span class='text-primary'>
				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				  <i class='fa fa-shopping-cart'></i>&nbsp;<span class='text-primary'>Supplier Products</span>
				</a>
				</span>
			  </h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			  <div class="panel-body">
				<div class='table table-responsive' style='max-height: 350px; overflow: auto'>
					<table class="table table-condensed">
					  <?php if(isset($supplierProduct)){ ?>
					  <thead>
						<tr>
						  <th><span class='text-success'>#</span></th>				<th><span class='text-success'>Type</span></th>	
						  <th><span class='text-success'>SKU</span></th>			<th><span class='text-success'>Name</span></th>	
						  <th><span class='text-success'>Description</span></th>	<th><span class='text-success'>Order</span></th>			
						  <th><span class='text-success'>Details</span></th>
						</tr>
					  </thead>
					
						
					  <tbody><?php $line_number = 1;  $i=0; foreach($supplierProduct as $product_detail): ?>
						
							<tr>
								<td><?php echo $line_number; ?></td>								<td><?php echo $product_detail['product_type']; ?> </td>
								<td><?php echo $product_detail['sku']; ?> </td>						<td><?php echo $product_detail['product_name']; ?> </td>
								<td><?php echo $product_detail['product_description']; ?> </td>		
											
								
								<?php
									$sku = $product_detail['sku'];						$name = $product_detail['product_name'];	
									$desc = $product_detail['product_description'];		$id = $product_detail['product_id'];
								?>
								<td><button type="button" class="btn btn-link" onclick="addRow('dataTable', '<?php echo $sku; ?>',  '<?php echo $name; ?>',  '<?php echo $desc; ?>', '<?php echo $id; ?>')"><i class='fa fa-shopping-cart fa-fw'></i></button></td>
								<td><button type='button' class='btn btn-link' data-toggle="tooltip" data-placement="bottom" title="View details"><i class='fa fa-folder-open-o fa-fw'></i></button></td>
								<input type='hidden' name='id' value="<?php echo $product_detail['product_id']?>">
								<input type='hidden' name='supplier' value="<?php echo $product_detail['name']?>">
							</tr><?php $line_number++; $i++; endforeach ?>
						
					  </tbody>
					<?php	}else{ ?>
						<tbody>
							<tr><td><h4>Sorry, no products found.</h4></td></tr>
						</tbody>
					<?php } ?>  
					  
					</table>
				</div> <!--div table end-->
			  </div>
			</div>
		  </div>