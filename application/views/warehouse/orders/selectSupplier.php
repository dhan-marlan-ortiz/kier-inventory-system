

<form method='post' action="<?=site_url('warehouse_createOrder/supplierProduct')?>">
	<div class='container'>
		<div class='row'>
			<div class='col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4'>

				<div class='page-header'>
					<span class=' text-primary h2'>Select Supplier </span>
				</div>

				<div class='panel'>
					<textarea type="text" name='supplier' class="form-control" data-toggle="modal" data-target=".supplierList" placeholder='Click here to view list of suppliers' id='selectedSupplier' style='cursor: pointer' readonly /></textarea>
					<input type='hidden' name='id' id='supplierId' />
				</div>

				<div class='row'>
					<div class='col-sm-6 col-xs-6 col-md-6'>
						<a href="<?=site_url('warehouse_createOrder/create')?>" class='btn btn-default btn-block' type='submit'><i class='glyphicon glyphicon-chevron-left pull-left'></i>&nbsp;Back </a>
					</div>
					<div class='col-sm-6 col-xs-6 col-md-6'>
						<button class='btn btn-primary btn-block' type='submit'> Next&nbsp;<i class='glyphicon glyphicon-chevron-right'></i></button>
					</div>
				</div>

			</div>
		</div>
	</div>
</form>

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
		<li><a href="#"><i class='fa fa-pencil-square-o fa-fw'></i>&nbsp;Request</a></li>
		<li><a href="#"><i class='fa fa-list-alt'></i>&nbsp;Create Order</a></li>
		<li class="active">Select Supplier </li>
	</ol>
</div>

<!-- search modal -->
<div class="modal fade supplierList" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-md">
	<div class='panel panel-success'>

		<div id="users">

			<div class='panel-heading'>
				<div class='row'>
					<div class='col-sm-10'>
					  <input type="text" class="form-control col-sm-10 search" placeholder="Search Supplier">
					 </div>
					<div class='col-sm-2'>
						<button class="btn btn-default btn-block sort" data-sort='name' type="button"><i class='fa fa-sort-alpha-asc'></i>&nbsp;Sort</button>
					</div>
				</div>
			</div>

			<div class='panel-body'>
				<div class='table-responsive' style='max-height: 250px; overflow: auto'>
					<div class="list list-group">
						<?php if(isset($suppliers)){ foreach($suppliers as $supplier_data): ?>
							<a class='list-group-item' style='border:none;' onclick="addSupplier('<?php echo $supplier_data['name']; ?>', '<?php echo $supplier_data['id']; ?>');" data-dismiss="modal">
								<i class='fa fa-plus-square-o'></i>&nbsp;&nbsp;&nbsp;<span class="name text-primary"><?php echo $supplier_data['name']; ?></span>
							</a>

						<?php endforeach; } ?>
					</div>
				</div>
			</div>

			<div class='panel-footer'>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div><!--end users-->

	</div>
</div>
</div>

<script>
	function addSupplier(name, id){
		document.getElementById('selectedSupplier').innerHTML = name;
		document.getElementById('supplierId').value = id;
	}



	var options = {
	  valueNames: [ 'name']
	};

	var userList = new List('users', options);

	function hide(obj){
		var el = document.getElementById(obj);
		el.style.display = 'none';
	}

	function show(obj){
		var el = document.getElementById(obj);
		el.style.display = 'block';
	}


</script>
