<br/>

<?php
	foreach ($request as $request_item):
		$subject = $request_item['orderSubject'];
		$message = $request_item['orderMessage'];
		$recipient = $request_item['orderRecipient'];
		$date = $request_item['request_date'];
		$status = $request_item['status'];
		$location = $request_item['deliveryLocation'];
	endforeach;
?>

<form method='post' action="<?=site_url('warehouse_request/outbox')?>">

	<div class='container'>
		<div class='panel panel-default' id='printThis'>

			<div class='panel-heading'>
				<div class='row'>
					<div class='col-sm-12'>
						<h2 class='text-primary'><i class='fa fa-check-square-o fa-fw'></i> Request Form <small class='pull-right'>#<?php echo $requestNo; ?></small></h2>
					</div>
				</div>
				<div class='row'>
					<div class='col-sm-1 col-xs-1'>
						<strong class='pull-right'>Date:</strong>
					</div>
					<div class='col-sm-11 col-xs-11'>
						<strong id='date'></strong>
					</div>
				</div>
				<div class='row'>
					<div class='col-sm-1 col-xs-1'>
						<strong class='pull-right'>To:</strong>
					</div>
					<div class='col-sm-11 col-xs-11'>
						<address>
							<strong><?php echo $recipient; ?> </strong>
							<?php if(isset($supplier)){ foreach($supplier as $s):echo "<br/>".$s['address']; endforeach; } ?>
						</address>
					</div>
				</div>
			</div>

			<div class='panel-body'>
				<table class='table table-condensed table-striped table-hover' id='productTable'>
					<thead>
						<tr>
							<th><span class='text-success'>#</span></th>
							<th><span class='text-success'>Product</span></th>
							<th><span class='text-success'>SKU</span></th>
							<th><span class='text-success'>Category</span></th>
							<th><span class='text-success'>Box</span></th>
							<th><span class='text-success'>Pack</span></th>
							<th><span class='text-success'>Piece</span></th>
							<th><span class='text-success'>Notes</span></th>
						</tr>
					</thead>

					<tbody>
						<?php $line_number = 1;
						foreach ($orderedList as $request_item): ?>
							<tr>
								<td><?php echo $line_number; ?></td>
								<td><?php echo $request_item['product_name']; ?> </td>
								<td><?php echo $request_item['sku']; ?> </td>
								<td><?php echo $request_item['product_type']; ?> </td>
								<td><?php echo $request_item['box']; ?> </td>
								<td><?php echo $request_item['pack']; ?> </td>
								<td><?php echo $request_item['piece']; ?> </td>
								<td><?php echo $request_item['orderMessage']; ?> </td>
							</tr>
						<?php $line_number++;
						endforeach ?>
					 </tbody>

				</table>
			</div>

			</div><!-- end panel-->
		</div><!-- end container-->



	<div class='container'>
		<div class='panel'>
			<div class='row'>
				<div class='col-sm-2 '>
					<button type='button' id='modifyBtn' class='btn btn-default btn-block' data-toggle="collapse" href=".showOnModify">
						<i class='fa fa-edit'></i>&nbsp;Modify
					</button>
				</div>
				<div class='col-sm-2 col-sm-offset-6'>
					<button type='button' id='printBtn' name='print' onclick="printContent('printThis');" class='btn btn-default btn-block'><i class='fa fa-download'></i>&nbsp;Download&nbsp;/&nbsp;<i class='fa fa-print'></i>&nbsp;Print</button>&nbsp;
				</div>
				<div class='col-sm-2'>
					<button type='submit' id='finishBtn' class='btn btn-success btn-block' ><i class='fa fa-check'></i>&nbsp;Finish</button>
				</div>
			</div>
		</div>
	</div>

</form>

<!-- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -- MAIN END -- -->

<div class='container'>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Request form has been successfully created.</strong> You can check your outbox.
	</div>
</div>

<!--FOOTER-->
<div class="container">
	<ol class="breadcrumb pull-right">
	  <li><a href="<?=site_url('')?>"><i class='fa fa-check-square-o fa-fw'></i>&nbsp; Request</a></li>
	  <li><a href=""><i class='fa fa-list-alt fa-fw'></i>&nbsp; Create Request </a></li>
	  <li class='active'>Finished Request Form</li>
	</ol>
</div>




<script language='javascript' >


		//get current date
		var d = new Date();
		var year = d.getFullYear();
		var month = d.getMonth();
		var day = d.getDate();
		var monthInWords = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
		document.getElementById("date").innerHTML = monthInWords[month]+' '+day+', '+year;




</script>
