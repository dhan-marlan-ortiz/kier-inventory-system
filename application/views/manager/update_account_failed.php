</head>
	<body>
		<div class='page-header'>
			<div class='container'>
				<div class='row'>
					<div class='col-sm-4'>
						<span class='text-success h3'>Update Account </span>
					</div>
					<div class='col-sm-8'>
						<ol class="breadcrumb pull-right">
						  <li><a href="<?=site_url('login_controller/manager')?>"><i class='fa fa-home fa-fw'></i>&nbsp; Home</a></li>
						  <li><a href="<?=site_url('manager/existing_account')?>"><i class='fa fa-home fa-fw'></i>&nbsp; Accounts </a></li>
						  <li class="active"><i class='fa fa-edit fa-fw'></i>&nbsp; Edit</li>
						</ol>
					</div>
				</div>
			</div> <!--end container-->
		</div> <!--end page header-->
		
		<div class='container'>
			<div class='row'>
				<form method='POST' action="<?=site_url('manager/edit_account')?>" role='form'>
					<div class='col-sm-6'>
						<div class='table-responsive'>
							<table class='table table-striped table-condensed'>
								<thead>
									<tr>
										<th><span class='h4 text-primary'>User Profile</span></th><th>
											<button type='submit' class='pull-right btn btn-link'><i class='fa fa-save fa-fw'></i>&nbsp; Save</button>
											<button href="<?=site_url('manager/edit_account')?>" class='pull-right btn btn-link' ><i class='fa fa-remove fa-fw'></i>&nbsp; Cancel</button>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr class='active'><th><i class='fa fa-graduation-cap fa-fw'></i>&nbsp; First name</th><td><input type='text' class='form-control input-sm' name='firstname' value="<?php echo $firstname; ?>" size='50' required ></td></tr>
									<tr><th><i class='fa fa-smile-o fa-fw'></i>&nbsp; Last name</th><td><input type='text' class='form-control  input-sm' name='lastname' value="<?php echo $lastname; ?>" size='50' required></td></tr>
									<tr class='active'><th><i class='fa fa-barcode fa-fw'></i>&nbsp; Employee ID</th><td><input type='text' class='form-control  input-sm' name='account_id' value="<?php echo $account_id; ?>" size='50' required></td></tr>
									<tr><th><i class='fa fa-black-tie fa-fw'></i>&nbsp; Position</th><td><input type='text' class='form-control  input-sm' name='position' value="<?php echo $position; ?>"></td></tr>
									<tr class='active'><th><i class='fa fa-institution fa-fw'></i>&nbsp; Branch/Office</th><td>
										<select class="form-control " name='area' id="branch" required>
											<option value="" disabled selected><span style='color:#777'>Select location</span></option></option>
											<option value="Warehouse">Warehouse</option>
											<option value="Main Office">Main Office</option>
											<option value="Kier Enterprises Irosin">Kier Enterprises Irosin</option>
											<option value="Kier Enterprises Gubat">Kier Enterprises Gubat</option>
											<option value="Kier Enterprises Casiguran">Kier Enterprises Casiguran</option>
										  </select>
									</tr>
									<tr><th><i class='fa fa-user fa-fw'></i>&nbsp; Username</th><td><input type='text' class='form-control  input-sm' name='username' value="<?php echo $username; ?>" size='50' required></td></tr>
									<tr class='active'><th><i class='fa fa-briefcase fa-fw'></i>&nbsp; Account Type</th>
													   <td><select class="form-control" name='account_type' required>
															<option value="">Select type</option><option value="Sub-Office">Branch Officer</option>
															<option value="Warehouse">Warehouse Officer</option><option value="Manager">Manager</option></select></td></tr>
									<tr><th><i class='fa fa-sun-o fa-fw'></i>&nbsp; Status</th>
										<td><select class="form-control input-sm" name='status'><option>Active</option><option>Inactive</option></select></td>
									</tr>
									<tr class='active'><th><i class='fa fa-lock fa-fw'></i>&nbsp; New Password</th><td><input type='password' class='form-control  input-sm' name='password' size='50' required></td></tr>
									<tr><th><i class='fa fa-lock fa-fw'></i>&nbsp; Confirm Password</th><td><input type='password' class='form-control  input-sm' name='passcon' size='50' required></td></tr>
									<input type='hidden' name='user_id' value='<?php echo $user['account_id']; ?>'>
								</tbody>
							</table>
							
						</div>	
					</div>
					<div class='col-sm-6'>
						<div class='container-fluid'>
						<?php
						echo "<div class='row'><div class='col-sm-10 col-sm-offset-2'><div class='alert alert-danger' role='alert'>
								<strong> Possible errors: </strong><ul>" ;
							if($err == 1){
								echo "<li>Firstname should not exceed 50 characters. </li>";
								echo "<li>Firstname field should not be empty. </li>";
							}else if($err == 2){
								echo "<li>Lastname should not exceed 50 characters. </li>";
								echo "<li>Lastname field should not be empty. </li>";
							}else if($err == 3){
								echo "<li>Employee ID can not contains anything other than alpha-numeric characters. </li>";
								echo "<li>Employee ID should not exceed 50 characters. </li>";
								echo "<li>Employee ID field should not be empty. </li>";
							}else if($err == 4){
								echo "<li>Position should not exceed 50 characters. </li>";
								echo "<li>Position field should not be empty. </li>";
							}else if($err == 5){
								echo "<li>Branch/Office should not exceed 50 characters. </li>";
								echo "<li>Branch/Office field should not be empty. </li>";
							}else if($err == 6){
								echo "<li>Username can not contains anything other than alpha-numeric characters, underscores or dashes. </li>";
								echo "<li>Username should not exceed 50 characters. </li>";
								echo "<li>Username field should not be empty. </li>";
							}else if($err == 7){
								echo "<li>Passwords did not match. </li>";
								echo "<li>Password should not exceed 50 characters. </li>";
								echo "<li>Password field should not be empty. </li>";
							}else if($err == 8){
								echo "<li>Passwords did not match. </li>";
								echo "<li>Password Confirm field should not be empty. </li>";
							}
							echo "</ul></div></div></div>";	
						?>
					</div>
					</div>
				</form>
			</div>	
		</div><!-- end container-->
		
			
		
									