NOTES:
<td><?php echo $user_item['username'] ?></td>
							<td><a  role="button" data-toggle="collapse" href="#collapseExample<?php echo $i; ?>" aria-expanded="false" aria-controls="collapseExample">
									<i class='fa fa-eye fa-fw'> </i>
								</a>
								<div class="collapse" id="collapseExample<?php echo $i; ?>"><?php echo $user_item['password'] ?></div></td>
							</td>
							
							<td><?php echo $user_item['position']; ?></td>
							<td><?php echo $user_item['area']; ?></td>
							
							
							
							
							
							
							
							
							===================
							
							
							<div class="modal fade bs-example-modal-sm<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
								  <div class="modal-dialog modal-md">
									<div class="modal-content">
										<div class='modal-body'>
										  <div class="list-group">
											<div href="#" class="list-group-item">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title text-info" id="gridSystemModalLabel">
													<span class='lead'>Account Details</span> &nbsp;&nbsp;<small><a href="<?=site_url('manager/update_account?uu='.$user_item["account_id"].'')?>"  > <i class='fa fa-edit fa-fw'></i>&nbsp; Edit </a></small>
												</h4>
											</div>
											<a href="#" class="list-group-item">
												<h5><i class='fa fa-graduation-cap fa-fw'></i>&nbsp; <?php echo $user_item['firstname']; ?>  <small class='pull-right'>First name</small></h5>
											</a>
											<a href="#" class="list-group-item">
												<h5><i class='fa fa-smile-o fa-fw'></i>&nbsp; <?php echo $user_item['lastname']; ?>  <small class='pull-right'>Last name</small></h5>
											</a>
											<a href="#" class="list-group-item">
												<h5><i class='fa fa-barcode fa-fw'></i>&nbsp; <?php echo $user_item['account_id']; ?>  <small class='pull-right'>Employee ID</small></h5>
											</a>
											<a href="#" class="list-group-item">
												<h5><i class='fa fa-black-tie fa-fw'></i>&nbsp; <?php echo $user_item['position']; ?>  <small class='pull-right'>Position</small></h5>
											</a>
											<a href="#" class="list-group-item">
												<h5><i class='fa fa-institution fa-fw'></i>&nbsp; <?php echo $user_item['area']; ?>  <small class='pull-right'>Branch/Office</small></h5>
											</a>
											<a href="#" class="list-group-item">
												<h5><i class='fa fa-user fa-fw'></i>&nbsp; <?php echo $user_item['username']; ?>  <small class='pull-right'>Username</small></h5>
											</a>
											<a href="#" class="list-group-item">
												<h5><i class='fa fa-briefcase fa-fw'></i>&nbsp; <?php echo $user_item['account_type']; ?>  <small class='pull-right'>Account type</small></h5>
											</a>
											<a href="#" class="list-group-item">
												<h5><i class='fa fa-sun-o fa-fw'></i>&nbsp; 
													<?php if($user_item['status'] == 'Active'){
														echo "<strong class='text-success'>Active</strong>";
													}else{
														echo "<strong class='text-danger'>Not active</strong>";
													}?>  <small class='pull-right'>Status</small></h5>
											</a>
											<div class="list-group-item">
												<h5><i class='fa fa-lock fa-fw'></i>&nbsp; 
												<a  role="button" data-toggle="collapse" href="#collapseExample<?php echo $i; ?>" aria-expanded="false" aria-controls="collapseExample">
													Show/hide password <small class='pull-right'>Password</small></h5>
												</a>
												<div class="collapse" id="collapseExample<?php echo $i; ?>"><?php echo $user_item['password'] ?></div>
											</div>
										  </div>
										  
										</div>
									</div>
								  </div>
								</div>