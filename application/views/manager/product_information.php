<div class='page-header'>
	<div class='container'>
		<div class='row'>
			<span class='text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
			<ol class="breadcrumb pull-right">
			  <li class="active"><i class='fa fa-cubes fa-fw'></i>&nbsp; Inventory </a></li>
			  <li><a href="<?=site_url('manager/inventory')?>"><i class='fa fa-shopping-cart fa-fw'></i>&nbsp; Products </a></li>
			  <li class="active"><i class='fa fa-info-circle fa-fw'></i>&nbsp; Product Information </li>
			</ol>
		</div>
	</div>  <!-- end container-->
</div>  <!-- end page header-->

<div class='container'>
	<div class='panel'>
		<div class='panel-heading'>
			<div class="row">
			  <div class="col-lg-6">
				<h4 class='text-primary'><i class='fa fa-shopping-cart'></i>&nbsp;Product Information </h4>
			  </div><!-- /.col-lg-6 -->
			  <div class="col-lg-6">
				<a href="<?=site_url('manager/inventory')?>" class='btn btn-default pull-right'><i class='fa fa-angle-double-left fa-fw'></i>&nbsp;Return </a>
			  </div><!-- /.col-lg-6 -->
			</div><!-- /.row -->
		</div>
		<div class='panel-body'>
			<table class='table table-bordered'>
				<!---------- PRODUCT DETAILS  ---------->
				<thead>
					<tr>
						<th colspan='5' class='text-primary'>Product Details</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th><i class='fa fa-genderless'></i> Product Name</th>	<td colspan='3'><?php echo $product['product_name']; ?></td>
					</tr>
					<tr>
						<th><i class='fa fa-genderless'></i> Product Type</th>	<td colspan='3'><?php echo $product['product_type']; ?></td>
					</tr>
					<tr>
						<th><i class='fa fa-genderless'></i> Manufacturer</th>	<td colspan='3'><?php echo $product['manufacturer']; ?></td>
					</tr>
					<tr><td colspan='5'>&nbsp;</td></tr>
				</tbody>
				<!-- -------- PRODUCT VARIANTS ---------->
				<thead>
					<tr>
						<th colspan='5' class='text-primary'>Product Variants</th>
					</tr>
				</thead>	
				<tbody>
					<tr>
						<th><i class='fa fa-genderless'></i> Stock-Keeping Unit (SKU)</th>	<td colspan='3'><?php echo $product['sku']; ?></td>
					</tr>
					<tr>
						<th><i class='fa fa-genderless'></i> Product Description</th>	<td colspan='3'><?php echo $product['product_description']; ?></td>
					</tr>
					<tr>
						<th><i class='fa fa-genderless'></i> Supplier</th>	<td colspan='3'><?php echo $product['name']; ?></td>
					</tr>
					<tr><td colspan='5'>&nbsp;</td></tr>
					<tr><td>&nbsp;</td>	<th class='text-center'>Box</th>	<th class='text-center'>Pack</th>	<th class='text-center'>Piece</th></tr>
					<tr>
						<th><i class='fa fa-genderless'></i> Available Stocks</th><td class='text-right'><?php echo $product['box_stock']; ?></td><td class='text-right'><?php echo $product['pack_stock']; ?></td><td class='text-right'><?php echo $product['piece_stock']; ?></td>
					</tr>
					<tr>
						<th><i class='fa fa-genderless'></i> Watch Level</th><td class='text-right'><?php echo $product['box_watch']; ?></td><td class='text-right'><?php echo $product['pack_watch']; ?></td><td class='text-right'><?php echo $product['piece_watch']; ?></td>
					</tr>
					<tr>
						<th><i class='fa fa-genderless'></i> Buying Cost</th><td class='text-right'><?php echo $product['box_cost']; ?></td><td class='text-right'><?php echo $product['pack_cost']; ?></td><td class='text-right'><?php echo $product['piece_cost']; ?></td>
					</tr>
					<tr>
						<th><i class='fa fa-genderless'></i> Selling Price</th><td class='text-right'><?php echo $product['box_price']; ?></td><td class='text-right'><?php echo $product['pack_price']; ?></td class='text-right'><td class='text-right'><?php echo $product['piece_price']; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>


