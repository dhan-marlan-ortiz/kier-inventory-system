<div class='page-header'>
	<div class='container'>
		<div class='row'>
			<span class='text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
			
			<ol class="breadcrumb pull-right">
			  <li class="active"><i class='fa fa-cube fa-fw'></i>&nbsp; Inventory </li>
			  <li class="active"><i class='fa fa-eye fa-fw'></i>&nbsp; Product Watch </li>
			</ol>
		</div>
	</div>  <!-- end container-->
</div>  <!-- end page header-->

<div class='container'>
	<div class='panel'>
		
		<div class='panel-heading'>
			<h4 class='text-primary'><i class='fa fa-eye'></i> Product Watch</h4>
		</div>
		
		<div class='panel-body'>
			<table class="table table-fixed table-condensed">
				  <?php if(isset($product)){  ?>
				  
				  <thead>
					<tr>
						<th>#</th><th>Supplier</th><th>Products</th>
					</tr>
				  </thead>
				  <tbody>
					<?php $line_number = 1; ?>	
					
					<?php $i=1; foreach ($product as $product_detail): ?>
					
						<form method='POST' action= "<?=site_url("manager/watchList")?>" role='form'>		
							<tr>
								<td><?php echo $line_number; ?></td>
								<td><?php echo $product_detail['name']; ?> </td>
								<td><button type='submit' name='details' class='btn btn-link input-sm'>View</button></td>
								<input type='hidden' name='supplier' value="<?php echo $product_detail['supplier']?>">
								<?php $line_number++;  ?> 
							</tr>
						</form><!--product list form end-->						
								
					<?php $i++; endforeach;  ?>
				  </tbody>
				  
				  <?php } else echo "Stocks are sufficient"; ?>
				</table>
		</div>
	</div>
</div>
				
				
