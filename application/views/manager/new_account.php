</head>
<body>



<div class='page-header'>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-4'>
				<span class='text-success h3'> New Account </span>
			</div>
			<div class='col-sm-8'>
				<ol class="breadcrumb pull-right">
				  <li><a href="<?=site_url('login_controller/manager')?>"><i class='fa fa-home fa-fw'></i>&nbsp; Home</a></li>
				  <li><a href="<?=site_url('manager/existing_account')?>"><i class='fa fa-user fa-fw'></i>&nbsp; Accounts</a></li>
				  <li class="active"><i class='fa fa-plus fa-fw'></i>&nbsp; New</li>
				</ol>
			</div>
		</div>
	</div>  <!-- end container-->
</div>  <!-- end page header-->

<form class="form-horizontal" method='POST' action='<?=site_url('manager/create_account')?>'>
<div class='container'>
	<div class='row'>
		<div class='col-sm-6 col-sm-offset-2'>
			 
			  <div class="form-group ">
				
				<label for="name" class="col-sm-2 control-label"><i class='fa fa-graduation-cap fa-lg fa-fw'></i></label>
				
				<!--First name-->
				<div class="col-sm-5">
					
				<input name='firstname' type="name" class="form-control" id="name" placeholder="First name"  size='50' 
					<?php if(isset($old_values)){ echo "value='".$old_values['firstname']."'"; } ?> required>
				</div>
				
				<!--Last name-->
				<div class="col-sm-5">
				  <input name='lastname' type="name" class="form-control" id="name" placeholder="Last name"  size='50' 
				  <?php if(isset($old_values)){ echo "value='".$old_values['lastname']."'"; } ?> required>
				</div>
			  </div>
			  
			  
			  <div class="form-group">
				<label for="company" class="col-sm-2 control-label"><i class='fa fa-black-tie fa-lg fa-fw'></i></label>
				
				<!--Employee Number-->
				<div class="col-sm-5">
				  <input name='account_id' type="text" class="form-control" id="company" placeholder="Employee ID" size='50'
				  <?php if(isset($old_values)){ echo "value='".$old_values['account_id']."'"; } ?> required>
				</div>
				
				<!--Position`-->
				<div class="col-sm-5">
				  <input name='position' type="text" class="form-control" id="company" placeholder="Position" size='50' 
				  <?php if(isset($old_values)){ echo "value='".$old_values['position']."'"; } ?> required>
				</div>
			  </div>
			  
			  
			  <div class="form-group">
			    <label for="branch" class="col-sm-2 control-label"><i class='fa fa-institution fa-lg fa-fw'></i></label>
				
				<!--Branch/Office-->
				<div class="col-sm-10">
				  <select class="form-control " name='area' id="branch" required>
					<option value="" disabled selected><span style='color:#777'>Branch/Office</span></option></option>
					<option value="Warehouse">Warehouse</option>
					<option value="Main Office">Main Office</option>
					<option value="Kier Enterprises Irosin">Kier Enterprises Irosin</option>
					<option value="Kier Enterprises Gubat">Kier Enterprises Gubat</option>
					<option value="Kier Enterprises Casiguran">Kier Enterprises Casiguran</option>
				  </select>
				</div>
			  </div>
			
			  <div class="form-group">
				<label for="username" class="col-sm-2 control-label"><i class='fa fa-user fa-lg fa-fw'></i></label>
				
				<!--User name-->
				<div class="col-sm-5">
					<input name='username' type="text" class="form-control"  id="username" placeholder="Username" size='50' 
					<?php if(isset($old_values)){ echo "value='".$old_values['username']."'"; } ?> required>
					
				</div>
				
				<!--Account type-->
				<div class="col-sm-5">
					<select class="form-control" name='account_type' required>
						<option value="">Select type</option>
						<option value="Sub-Office">Branch Officer</option>
						<option value="Warehouse">Warehouse Officer</option>
						<option value="Manager">Manager</option>
					</select>
				</div>
			  </div>
			  
			  <div class="form-group">
				<label for="password" class="col-sm-2 control-label"><i class='fa fa-lock fa-lg fa-fw'></i></label>
				
				<!--Password-->
				<div class="col-sm-5">
				  <input name='password' type="password" class="form-control" id="password" placeholder="Password" size='50'  required>
				</div>
				
				<!--Confirm Password-->
				<div class="col-sm-5">
				  <input name='passcon' type="password" class="form-control" id="password" placeholder="Re-enter password" size='50' required>
				</div>
			  </div>
			  <hr>
			  
			  <div class="form-group">
				<label for="submit_new_account" class="col-sm-2 control-label"><i class='fa fa-database fa-lg fa-fw'></i></label>
					<div class='col-sm-10'>
					
						<div class="btn-group btn-group-justified" role="group" aria-label="...">
						
							<div class="btn-group" role="group">
							   <a href="<?=site_url('manager/existing_account')?>" type="button" class="btn btn-danger"><i class='fa fa-close fa-lg fa-fw'></i>Close</a>
							</div>
							<div class="btn-group" role="group">
							   <a href="<?=site_url('manager/new_account')?>" type="button" class="btn btn-info"><i class='fa fa-eraser fa-lg fa-fw'></i>Clear</a>
							</div>
							<div class="btn-group" role="group">
							   <button type="submit" class="btn btn-primary"><i class='fa fa-save fa-lg fa-fw'></i>Save</button>
							</div>
					   </div>
				   </div>
			  
			  </div>
			<?php if(isset($old_values)){ ?>
			<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
			<?php } ?>	
			
			<?php if(isset($success)){ ?>
				<div class="alert alert-success" role="alert"><strong>Well done!</strong> New account successfully created.</div>
			<?php } ?>	
			
			
		</div><!--end logical col-->
		
	</div><!--end row-->
	
	
	
</div><!--end container-->
</form>

<?php echo form_open('form'); ?>



