</head>
	<body>
	
		<div class='page-header'>
			<div class='container'>
				<div class='row'>
					<div class='col-sm-4'>
						<span class='text-success h3'>Update Account </span>
					</div>
					<div class='col-sm-8'>
						<form method='POST' action="<?=site_url('manager/view_account')?>">
							<ol class="breadcrumb pull-right">
							  <li><a href="<?=site_url('login_controller/manager')?>"><i class='fa fa-home fa-fw'></i>&nbsp; Home</a></li>
							  <li><a href="<?=site_url('manager/existing_account')?>"><i class='fa fa-user fa-fw'></i>&nbsp; Accounts</a></li>
							  <li>	
									<button type='submit' class='btn btn-link' href="<?=site_url('manager/return_view_account')?>"><i class='fa fa-eye fa-fw'></i>&nbsp; View Account </button>
									<input type='hidden' name='id' value="<?php echo $user['account_id']; ?>">
									<input type='hidden' name='username' value="<?php echo $user['username']; ?>">
							  </li>
							  <li class="active"><i class='fa fa-edit fa-fw'></i>&nbsp; Update</li>
							</ol>
						</form>
					</div>
				</div>
			</div> <!--end container-->
		</div> <!--end page header-->
		
		
			<div class='container'>
				<div class='row'>
					<div class='col-sm-6 col-sm-offset-3'>
						<form method='POST' action= "<?=site_url('manager/edit_account_submit')?>" role='form'>
							<div class='table-responsive panel panel-default'>
								<table class='table'>
									<tbody>
										<tr>
											<th><i class='fa fa-graduation-cap fa-fw'></i>&nbsp; First name</th>
											<td><input type='text' name='firstname' value="<?php echo $user['firstname']; ?>"  size='50' class='form-control input-sm' required /></td>
										</tr>
										<tr>
											<th><i class='fa fa-smile-o fa-fw'></i>&nbsp; Last name</th>
											<td><input type='text' name='lastname' value="<?php echo $user['lastname']; ?>"  size='50' class='form-control input-sm' required /></td>
										</tr>
										<tr>
											<th><i class='fa fa-barcode fa-fw'></i>&nbsp; Employee ID</th>
											<td><input type='text' name='account_id' value="<?php echo $user['account_id']; ?>" size='50' class='form-control input-sm' required /></td>
										</tr>
										<tr>
											<th><i class='fa fa-black-tie fa-fw'></i>&nbsp; Position</th>
											<td><input type='text' name='position' value="<?php echo $user['position']; ?>"  size='50' class='form-control input-sm' required /></td>
										</tr>
										<tr>
											<th><i class='fa fa-institution fa-fw'></i>&nbsp; Branch/Office</th>
											<td>
												<select class="form-control " name='area' id="branch" required>
													<option value="" disabled selected><span style='color:#777'>Select location</span></option></option>
													<option value="Warehouse">Warehouse</option>
													<option value="Main Office">Main Office</option>
													<option value="Kier Enterprises Irosin">Kier Enterprises Irosin</option>
													<option value="Kier Enterprises Gubat">Kier Enterprises Gubat</option>
													<option value="Kier Enterprises Casiguran">Kier Enterprises Casiguran</option>
												  </select>
											</td>
											
										</tr>
										<tr>
											<th><i class='fa fa-user fa-fw'></i>&nbsp; Username</th>
											<td><input type='text' name='username' value="<?php echo $user['username']; ?>" size='50' class='form-control input-sm' required /></td>
										</tr>
										<tr>
											<th><i class='fa fa-briefcase fa-fw'></i>&nbsp; Account Type</th>
											<td><select class="form-control" name='account_type' required>
												<option value="">Select type</option>
												<option value="Sub-Office">Branch Officer</option>
												<option value="Warehouse">Warehouse Officer</option>
												<option value="Manager">Manager</option>
											</select></td>
										</tr>
										<tr>
											<th><i class='fa fa-sun-o fa-fw'></i>&nbsp; Status</th>
											<td><select  name='status' class='form-control input-sm'>
												<option>Active</option>
												<option>Inactive</option>
											</select></td>
										</tr>
										<tr>
											<th><i class='fa fa-lock fa-fw'></i>&nbsp; New Password</th>
											<td><input type='password' name='password' value="<?php echo $user['password']; ?>" size='50' class='form-control input-sm' required /></td>
										</tr>
										<tr>
											<th><i class='fa fa-lock fa-fw'></i>&nbsp; Confirm Password</th>
											<td><input type='password' name='passcon' placeholder='Re-Enter New Password' size='50' class='form-control input-sm' required /></td>
										</tr>
										
									</tbody>
								</table>
							</div>	
							<div class='col-sm-3 col-sm-offset-3'>
								<input type='hidden' name='id' value="<?php echo $user['id']; ?>">
								<button class='btn btn-primary btn-block' type='submit'><i class='fa fa-save fa-fw'></i> Save </button>
							</div>
						</form>					
						<div class='col-sm-3'>
							<form method='POST' action="<?=site_url('manager/return_view_account')?>">
								<button type='submit' class='btn btn-danger  btn-block' href="<?=site_url('manager/view_account')?>"><i class='fa  fa-close fa-fw'></i> Cancel </button>
								<input type='hidden' name='id' value="<?php echo $user['id']; ?>">
								<input type='hidden' name='username' value="<?php echo $user['username']; ?>">
							</form>
						</div>
					</div>	<!--end col-->
					<div class='col-sm-3'>
						<?php  if(isset($user['err'])){ ?>
								<div class="alert alert-danger" role="alert">
									<div class='h4'> Error! </div>
									<span> <?php echo $user['err']; ?></span>
								</div>
						<?php } ?>
					</div>
				</div>	<!--end row-->
			</div><!-- end container-->
	<br>
	
		