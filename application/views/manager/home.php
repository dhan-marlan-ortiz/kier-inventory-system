	<?php  $fname = $this->session->userdata('fname'); ?>
	<?php  $utype = $this->session->userdata('utype'); ?>
	<?php  $uid = $this->session->userdata('uid'); ?>

</head>
	<body>
	<div class=''>
		<!-- header -->


		<nav class="navbar navbar-default box-shadow">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="<?=site_url('login_controller/manager')?>"><span class='text-primary'>Kier Enterprises</span></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">

				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class='fa fa-cube fa-fw'></i>&nbsp; Inventory <span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href="<?=site_url("manager/inventory")?>"><i class='fa fa-shopping-cart fa-fw'></i>&nbsp; Products</a></li>
					<li><a href="<?=site_url("manager/productWatch")?>"><i class='fa fa-eye fa-fw'></i>&nbsp; Product Watch</a></li>

				  </ul>
				</li>

				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class='fa fa-pie-chart fa-fw'></i>&nbsp; Reports <span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href="#"><i class='fa fa-calendar fa-fw'></i>&nbsp; Monthly Reports</a></li>
					<li><a href="#"><i class='fa fa-shopping-cart fa-fw'></i>&nbsp; On-Hand Product Count</a></li>

				  </ul>
				</li>


				 <li <?php if($active=='accounts'){echo "class='active'";} ?>><a href="<?=site_url('manager/existing_account')?>"><i class='fa fa-user fa-fw'></i>&nbsp; Accounts</a></li>




				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class='fa fa-paw fa-fw'></i>&nbsp; Activity Log <span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href="#"><i class='fa fa-shopping-cart fa-fw'></i>&nbsp; Products</a></li>
					<li><a href="#"><i class='fa fa-users fa-fw'></i>&nbsp; Users</a></li>

				  </ul>
				</li>

			  </ul>

			  <ul class="nav navbar-nav navbar-right">
				<li ><a href="<?=site_url('Login_controller/logout')?>"><span  class='text-danger'><i class='fa fa-power-off fa-fw'></i> Logout</a></span></li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>

	</div>
