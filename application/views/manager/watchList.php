<div class='page-header'>
	<div class='container'>
		<div class='row'>
			<span class='text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
			
			<ol class="breadcrumb pull-right">
			  <li class="active"><i class='fa fa-cube fa-fw'></i>&nbsp;Inventory </li>
			  <li><a href="<?=site_url('manager/productWatch')?>"><i class='fa fa-eye fa-fw'></i>&nbsp;Product Watch </a></li>
			  <li class="active"><i class='fa fa-list fa-fw'></i>&nbsp;Watch List </li>
			</ol>
		</div>
	</div>  <!-- end container-->
</div>  <!-- end page header-->

<div class='container'>
	<div class='panel'>
		
		<div class='panel-heading'>
			<div class='row'>
				<div class='col-sm-6'>
					<h4 class='text-primary'><i class='fa fa-list'></i> Watch List </h4>
				</div>
				<div class='col-sm-6'>
					<a href="<?=site_url('manager/productWatch')?>" class='btn btn-default pull-right'><i class='fa fa-angle-double-left fa-fw'></i>&nbsp;Return </a>
				</div>
			</div>
			<div class='row'>
				<div class='col-sm-12'>
					<address>
					  <strong id='supplier'></strong><br>
					  <span id='address'></span>
					</address>
				</div>
			</div>
				
			
		</div>
		
		<div class='panel-body'>
			<table class="table table-fixed table-condensed">
				  <?php if(isset($product)){  ?>
				  
				  <thead>
					<tr>
						<th>#</th><th>Type</th><th>SKU</th><th>Name</th><th>Description</th>
						<th>Box</th><th>Pack</th><th>Piece</th><th>Details</th>
					</tr>
				  </thead>
				  <tbody>
					<?php $line_number = 1; ?>	
					
					<?php $i=1; foreach ($product as $product_detail): ?>
					
						<form method='POST' action= "<?=site_url("manager/watchList_productInfo")?>" role='form'>		
							<tr>
								<td><?php echo $line_number; ?></td>
								<td><?php echo $product_detail['product_type']; ?> </td>
								<td><?php echo $product_detail['sku']; ?> </td>
								<td><?php echo $product_detail['product_name']; ?> </td>
								<td><?php echo $product_detail['product_description']; ?> </td>
								<td><?php echo $product_detail['box_stock']; ?> </td>
								<td><?php echo $product_detail['pack_stock']; ?> </td>
								<td><?php echo $product_detail['piece_stock']; ?> </td>
								<td><button type='submit' name='details' class='btn btn-link input-sm'>View</button></td>
								
								<input type='hidden' name='id' value="<?php echo $product_detail['product_id']?>">
								<input type='hidden' name='supplierId' value="<?php echo $product_detail['supplier']?>">
								<?php $line_number++;  ?> 
							</tr>
						</form><!--product list form end-->						
									<span class='hidden' id='supplierName'><?php echo $product_detail['name']; ?><span>
									<span class='hidden' id='supplierAddress'><?php echo $product_detail['address']; ?><span>
					<?php $i++; endforeach;  ?>
				  </tbody>
				  
				  <?php } else echo "Stocks are sufficient"; ?>
				</table>
				
		</div>
	</div>
</div>

<script>
	getSupplierName();
	getSupplierAddress();
	
	function getSupplierAddress(){
		y = document.getElementById('supplierAddress').innerHTML;
		document.getElementById('address').innerHTML = y;
	}
	function getSupplierName(){
		x = document.getElementById('supplierName').innerHTML;
		document.getElementById('supplier').innerHTML = x;
	}
</script>
				
