<div class='page-header'>
	<div class='container'>
		<div class='row'>
			<span class='text-success h3'><i class='fa fa-cube fa-fw'></i> Inventory </span>
			
			<ol class="breadcrumb pull-right">
			  <li class="active"><i class='fa fa-cube fa-fw'></i>&nbsp;Inventory </li>
			  <li class="active"><i class='fa fa-shopping-cart fa-fw'></i>&nbsp;Products </li>
			</ol>
		</div>
	</div>  <!-- end container-->
</div>  <!-- end page header-->

<div class='container'>
	<div class='panel'>
		<div class='panel-heading'>
			<div class="row">
			  <div class="col-lg-6">
				<h4 class='text-primary'><i class='fa fa-shopping-cart'></i>&nbsp;Products </h4>
			  </div><!-- /.col-lg-6 -->
			  <div class="col-lg-6">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search for...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Search</button>
					</span>
				</div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</div><!-- /.row -->
		</div>
		<div class='panel-body'>
			<div class='table table-responsive' style='max-height: 350px; overflow: auto'>
				<table class="table table-fixed table-condensed">
				  <thead class='text-primary'>
					<tr>
						<th>#</th><th>Type</th><th>SKU</th><th>Name</th><th>Description</th>
						<th>Box</th><th>Pack</th><th>Piece</th><th>Details</th>
					</tr>
				  </thead>
				  <tbody>
					<?php $line_number = 1; ?>	
					<?php $i=1; foreach ($product as $product_detail): ?>
					
						<form method='POST' action= "<?=site_url("manager/product_information")?>" role='form'>		
							<tr>
								<td><?php echo $line_number; ?></td>
								<td><?php echo $product_detail['product_type']; ?> </td>
								<td><?php echo $product_detail['sku']; ?> </td>
								<td><?php echo $product_detail['product_name']; ?> </td>
								<td><?php echo $product_detail['product_description']; ?> </td>
								<td><?php echo $product_detail['box_stock']; ?> </td>
								<td><?php echo $product_detail['pack_stock']; ?> </td>
								<td><?php echo $product_detail['piece_stock']; ?> </td>
								<td><button type='submit' name='details' class='btn btn-info input-sm'>View</button></td>
								<input type='hidden' name='id' value="<?php echo $product_detail['product_id']?>">
								<?php $line_number++;  ?> 
							</tr>
						</form><!--product list form end-->						
								
					<?php $i++; endforeach ?>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
