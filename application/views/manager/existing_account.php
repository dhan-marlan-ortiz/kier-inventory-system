<style> @media print {  thead {display: table-header-group;} } </style>

</head>
<body>
	<div class='page-header'>
		<div class='container'>
			<div class='row'>
				<div class='col-sm-2'>
					<span class='text-success h3'> Accounts </span>
				</div>



				<div class='col-sm-10'>
						<a class='btn btn-default' href="<?=site_url('manager/new_account')?>"><i class='fa fa-plus fa-fw'></i >&nbsp; New Account </a>
						<button onclick="printContent('printThis');" class='btn btn-default'><i class='fa fa-print'></i> Print Accounts</button>
						<ol class="breadcrumb pull-right">
						  <li><a href="<?=site_url('login_controller/manager')?>"><i class='fa fa-home fa-fw'></i>&nbsp; Home</a></li>
						  <li class="active"><i class='fa fa-user fa-fw'></i>&nbsp; Accounts</li>
						</ol>
					</div>
				</div>
		</div>  <!-- end container-->
	</div>  <!-- end page header-->

	<div class='container'>
			<div class='table-responsive'>
				<table class='table table-striped table-condensed'>

					<thead>
						<tr class='text-primary'>
							<th>Employee ID</th><th>Full Name</th><th>Username</th><th>Account Type</th><th>Status</th><th>View</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; foreach ($user as $user_item): ?>
							<form method='POST' action= "<?=site_url('manager/view_account')?>" role='form'>
								<?php $account_id = $user_item['account_id']; ?>

									<tr>
										<td><?php echo $user_item['account_id']; ?></td>
										<td><?php echo $user_item['firstname']." ".$user_item['lastname']; ?></td>
										<td><?php echo $user_item['username']; ?></td>
										<td><?php
												if($user_item['account_type'] == 'Sub-Office'){
													echo "Branch Officer";
												}else
													echo $user_item['account_type'];
											?>
										</td>
										<td><?php if($user_item['status'] == 'Active'){
														echo "<strong class='text-success '>Active</strong>";
													}else
														echo "<strong class='text-danger'>Inactive</strong>";
													?>
										</td>
										<td><button type='submit' class='btn btn-info input-sm'> View </button></td>
										<input type='hidden' name='id' value="<?php echo $user_item['id']?>">
									</tr>
							</form>
						<?php $i++; endforeach ?>
					</tbody>

				</table>
			</div>
	</div><!-- end container-->

	<div class='hidden' id='printThis'>
		<div class='panel'>
			<h3><i class='fa fa-user'></i> User Accounts</h3>
		</div>
		<hr>
		<table class='table table-striped table-condensed'>

			<thead>
				<tr class='text-primary'>
					<th>Employee ID</th><th>Full Name</th><th>Username</th><th>Account Type</th><th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1; foreach ($user as $user_item): ?>
					<form method='POST' action= "<?=site_url('manager/view_account')?>" role='form'>
						<?php $account_id = $user_item['account_id']; ?>

							<tr>
								<td><?php echo $user_item['account_id']; ?></td>
								<td><?php echo $user_item['firstname']." ".$user_item['lastname']; ?></td>
								<td><?php echo $user_item['username']; ?></td>
								<td><?php
										if($user_item['account_type'] == 'Sub-Office'){
											echo "Branch Officer";
										}else
											echo $user_item['account_type'];
									?>
								</td>
								<td><?php if($user_item['status'] == 'Active'){
												echo "<strong class='text-success '>Active</strong>";
											}else
												echo "<strong class='text-danger'>Inactive</strong>";
											?>
								</td>

							</tr>
					</form>
				<?php $i++; endforeach ?>
			</tbody>

		</table>
	</div>


	<script>
		function printContent(printThis){
			var restorepage = document.body.innerHTML;
			var printcontent = document.getElementById(printThis).innerHTML;
			document.body.innerHTML = printcontent;
			window.print();
			document.body.innerHTML = restorepage;
		}
	</script>
