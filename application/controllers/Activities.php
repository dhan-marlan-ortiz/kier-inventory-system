<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Activities extends CI_Controller {
public function __construct() { parent::__construct(); $this->load->database(); $this->load->model('activities_model'); $this->load->helper(array('form', 'url')); $this->load->library('form_validation'); }
public function index(){}
public function getActivities(){ $data['activities'] = $this->activities_model->getDisplayActivities(); $data['title'] = 'Activity Log'; $data['active'] = 'activity_log'; $this->load->view('warehouse/header', $data); $this->load->view('warehouse/navbar', $data); $this->load->view('warehouse/activities/activities', $data); $this->load->view('warehouse/footer'); }
}
