-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2015 at 07:34 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kierinventorysystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `account_id` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `account_type` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `area` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  `id` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_id`, `username`, `password`, `account_type`, `firstname`, `lastname`, `position`, `area`, `status`, `id`) VALUES
('KEI001-W', 'dhan', 'dhan', 'Warehouse', 'Dhan', 'Ortiz', 'Programmer', 'Warehouse', 'Active', 8),
('KEI002-M', 'neck', 'neck', 'Manager', 'Neckelyn', 'France', 'Project Manager', 'Main Office', 'Active', 9),
('KEI002-S', 'Jessica', 'jessica', 'Sub-Office', 'Jessica', 'Gayon', 'Analyst', 'Kier Enterprises Irosin', 'Active', 10),
('KEG003-S', 'jessica2', 'jessica2', 'Sub-Office', 'Jessica 2', 'Gayon 2', 'Analyst', 'Kier Enterprises Gubat', 'Active', 11),
('KEC003-S', 'jessica3', 'jessica3', 'Sub-Office', 'Jessica 3', 'Gayon 3', 'Analyst', 'Kier Enterprises Casiguran', 'Active', 12),
('KIE10008', 'jess', 'asa', 'Sub-Office', 'jessv2', 'gayon', 'secretatry', 'Main Office', 'Inactive', 14);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_old`
--

CREATE TABLE `invoice_old` (
  `invoice_num` varchar(200) NOT NULL,
  `date` varchar(20) NOT NULL,
  `supplier` varchar(200) NOT NULL,
  `invoice_sku` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(5) NOT NULL,
  `price` float NOT NULL,
  `discount` float NOT NULL,
  `subtotal` float NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `sysDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_old`
--

INSERT INTO `invoice_old` (`invoice_num`, `date`, `supplier`, `invoice_sku`, `quantity`, `unit`, `price`, `discount`, `subtotal`, `invoice_id`, `sysDate`) VALUES
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000033', 5, 'Box', 280.71, 0, 1, 1, '2015-04-20 00:00:00'),
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000584', 5, 'Box', 325.71, 0, 1, 2, '2015-04-20 00:00:00'),
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000587', 3, 'Box', 364.29, 0, 1, 3, '2015-04-20 00:00:00'),
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000496', 5, 'Box', 546.43, 0, 2, 4, '2015-04-20 00:00:00'),
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000564', 1, 'Box', 576.43, 0, 576.43, 5, '2015-04-20 00:00:00'),
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000151', 5, 'Box', 334.29, 0, 1, 6, '2015-04-20 00:00:00'),
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000152', 3, 'Box', 448.93, 0, 1, 7, '2015-04-20 00:00:00'),
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000197', 1, 'Box', 387.86, 0, 387.86, 8, '2015-04-20 00:00:00'),
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000336', 1, 'Box', 750, 0, 750, 9, '2015-04-20 00:00:00'),
('122994', 'April 20, 2015', 'ACCELERATED DISTRIBUTION INCORPORATED', 'FG000153', 1, 'Box', 304.29, 0, 304.29, 10, '2015-04-20 00:00:00'),
('150407', 'April 7, 2015', 'BAMBINO TRADING', '157', 24, 'Piece', 7.5, 5, 171, 11, '2015-04-07 00:00:00'),
('150407', 'April 7, 2015', 'BAMBINO TRADING', '159', 24, 'Piece', 10.3, 5, 234.84, 12, '2015-04-07 00:00:00'),
('150407', 'April 7, 2015', 'BAMBINO TRADING', '99', 12, 'Piece', 64.6, 5, 736.44, 13, '2015-04-07 00:00:00'),
('150407', 'April 7, 2015', 'BAMBINO TRADING', '161', 24, 'Piece', 10.3, 5, 234.84, 14, '2015-04-07 00:00:00'),
('150407', 'April 7, 2015', 'BAMBINO TRADING', '102', 12, 'Piece', 38.75, 5, 441.75, 15, '2015-04-07 00:00:00'),
('150407', 'April 7, 2015', 'BAMBINO TRADING', '103', 12, 'Piece', 69.25, 5, 789.45, 16, '2015-04-07 00:00:00'),
('150407', 'April 13, 2015', 'BAMBINO TRADING', '271', 24, 'Piece', 4, 10, 86.4, 17, '2015-04-07 00:00:00'),
('150407', 'April 7, 2015', 'BAMBINO TRADING', '272', 24, 'Piece', 6, 10, 129.6, 18, '2015-04-07 00:00:00'),
('150407', 'April 7, 2015', 'BAMBINO TRADING', '282', 12, 'Piece', 4.5, 10, 48.6, 19, '2015-04-07 00:00:00'),
('150407', 'April 7, 2015', 'BAMBINO TRADING', '285', 1, 'Box', 366, 10, 329.4, 20, '2015-04-07 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55210', 1, 'Box', 602.05, 0, 602.05, 21, '2015-05-12 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55211', 1, 'Box', 602.05, 0, 602.05, 22, '2015-05-12 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55160', 1, 'Box', 520.45, 0, 520.45, 23, '2015-05-12 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55161', 1, 'Box', 520.45, 0, 520.45, 24, '2015-05-12 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55332', 3, 'Box', 385.71, 0, 1, 25, '2015-05-12 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55333', 3, 'Box', 385.71, 0, 1, 26, '2015-05-12 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55598', 12, 'Piece', 11.9, 0, 142.8, 27, '2015-05-12 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55599', 12, 'Piece', 11.9, 0, 142.8, 28, '2015-05-12 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55661', 12, 'Piece', 11.9, 0, 142.8, 29, '2015-05-12 00:00:00'),
('190848', 'May 12, 2015', 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', '55680', 12, 'Piece', 11.9, 0, 142.8, 30, '2015-05-12 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'FRS033', 1, 'Box', 424.29, 0, 424.29, 31, '2015-05-18 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'FGHAR010', 1, 'Box', 888.39, 0, 888.39, 32, '2015-05-18 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'FGHAR008', 1, 'Box', 766.96, 0, 766.96, 33, '2015-05-18 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'FGVNT070', 1, 'Box', 1, 0, 1, 34, '2015-05-18 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'FGPCD002D', 1, 'Box', 1, 0, 1, 35, '2015-05-18 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'FGSBD218', 15, 'Box', 641.25, 0, 9, 36, '2015-05-18 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'FGSBD219', 15, 'Box', 726.96, 0, 10, 37, '2015-05-18 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'PCD003', 1, 'Box', 1, 0, 1, 38, '2015-05-18 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'PCD004', 1, 'Box', 1, 0, 1, 39, '2015-05-18 00:00:00'),
('387460', 'May 18, 2015', 'BONHAUR MARKETING CORPORATION', 'FGSBD217', 5, 'Box', 558.21, 0, 2, 40, '2015-05-18 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC1', 12, 'Piece', 59.2, 0, 710.4, 41, '2015-06-05 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC2', 12, 'Piece', 24.2, 0, 290.4, 42, '2015-06-05 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC3', 1, 'Box', 390, 0, 390, 43, '2015-06-05 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC4', 12, 'Piece', 44.3, 0, 531.6, 44, '2015-06-05 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC5', 12, 'Piece', 26.75, 0, 321, 45, '2015-06-05 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC6', 12, 'Piece', 59.9, 0, 718.8, 46, '2015-06-05 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC7', 24, 'Piece', 24.6, 0, 590.4, 47, '2015-06-05 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC8', 1, 'Box', 362.4, 0, 362.4, 48, '2015-06-05 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC9', 12, 'Piece', 47.6, 0, 571.2, 49, '2015-06-05 00:00:00'),
('7078', 'June 5, 2015', 'FORNIX CORPORATION', 'FC10', 12, 'Piece', 29, 0, 348, 50, '2015-06-05 00:00:00'),
('162699', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', 'RACK0', 5, 'Box', 93.5, 0, 467.5, 51, '2015-10-30 01:00:00'),
('162699', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', 'RACK1', 5, 'Box', 93.5, 0, 467.5, 52, '2015-10-30 01:00:00'),
('162699', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', 'RACK5', 5, 'Box', 93.5, 0, 467.5, 53, '2015-10-30 01:00:00'),
('162699', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', 'RACK6', 5, 'Box', 93.5, 0, 467.5, 54, '2015-10-30 01:00:00'),
('162699', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', 'RACK7', 5, 'Box', 93.5, 0, 467.5, 55, '2015-10-30 01:00:00'),
('162699', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', 'RACK9', 5, 'Box', 93.5, 0, 467.5, 56, '2015-10-30 01:00:00'),
('162699', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', 'RACK10', 5, 'Box', 93.5, 0, 467.5, 57, '2015-10-30 01:00:00'),
('162699', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', 'TOSEN', 2, 'Box', 1182.09, 0, 2364, 58, '2015-10-30 01:00:00'),
('162857', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', '50561', 1, 'Box', 936, 0, 936, 59, '2015-12-19 14:06:01'),
('162857', 'October 30, 2015', 'PAX & FOUND SALES DISTRIBUTOR', '50566', 1, 'Box', 936, 0, 936, 60, '2015-12-19 14:06:01'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1524631', 24, 'Piece', 918, 0, 22032, 61, '2015-07-13 00:00:00'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1509880', 2, 'Box', 758.57, 0, 1517.14, 62, '2015-07-13 00:00:00'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1529608', 12, 'Piece', 1, 0, 22860, 63, '2015-07-13 00:00:00'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1529623', 12, 'Piece', 1, 0, 22860, 64, '2015-07-13 00:00:00'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1529613', 6, 'Piece', 1, 0, 11430, 65, '2015-07-13 00:00:00'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1529699', 12, 'Piece', 1, 0, 18302.2, 66, '2015-07-13 00:00:00'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1535245', 1, 'Box', 1, 0, 1793.57, 67, '2015-07-13 00:00:00'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1535246', 1, 'Box', 990, 0, 990, 68, '2015-07-13 00:00:00'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1536329', 1, 'Box', 1, 0, 1793.57, 69, '2015-07-13 00:00:00'),
('818525', 'July 13, 2015', 'PRIME SELLER MARKETING', '1529628', 1, 'Box', 1, 0, 1317.86, 70, '2015-07-13 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI001', 24, 'Piece', 99.05, 0, 2, 71, '2015-05-05 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI053', 8, 'Piece', 198.84, 0, 1, 72, '2015-05-05 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI004', 12, 'Piece', 99.05, 0, 1, 73, '2015-05-05 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI007', 2, 'Box', 2, 0, 5, 74, '2015-05-05 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI058', 6, 'Piece', 312.32, 0, 3, 75, '2015-05-05 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI008', 24, 'Piece', 99.05, 0, 2, 76, '2015-05-05 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI009', 12, 'Piece', 221.22, 0, 2, 77, '2015-05-05 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI052', 2, 'Box', 2, 0, 5, 78, '2015-05-05 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI046', 6, 'Piece', 797.44, 0, 4, 79, '2015-05-05 00:00:00'),
('571510', 'May 5, 2015', 'PRINCETON ENTERPRISES', 'WPI024', 3, 'Piece', 1, 0, 4, 80, '2015-05-05 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC1', 2, 'Box', 1, 0, 2, 81, '2015-03-27 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC2', 2, 'Box', 690, 0, 1, 82, '2015-03-27 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC3', 2, 'Box', 696, 0, 1, 83, '2015-03-27 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC4', 2, 'Box', 225.69, 0, 451.38, 84, '2015-03-27 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC5', 2, 'Box', 228, 0, 456, 85, '2015-03-27 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC6', 2, 'Box', 259.63, 0, 519.26, 86, '2015-03-27 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC7', 2, 'Box', 225.69, 0, 451.38, 87, '2015-03-27 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC8', 2, 'Box', 259.63, 0, 519.26, 88, '2015-03-27 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC9', 2, 'Box', 225.69, 0, 451.38, 89, '2015-03-27 00:00:00'),
('1136636', 'March 27, 2015', 'RODZON MARKETING CORPORATION', 'RMC10', 2, 'Box', 477.5, 0, 955, 90, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'BSN-961400T', 96, 'Box', 1, 0, 1, 91, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'BTV-961400', 96, 'Box', 925.71, 0, 925.71, 92, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'CCTV-090230', 90, 'Box', 843.74, 0, 843.74, 93, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'BFR-244300T', 24, 'Box', 653.57, 0, 653.57, 94, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'BJR- 961320', 96, 'Box', 642.86, 0, 642.86, 95, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'KTFA-162752', 16, 'Box', 857.14, 0, 857.14, 96, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'TCFL-032100', 32, 'Box', 800, 0, 800, 97, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'FFTP-072240', 72, 'Box', 642.86, 0, 1, 98, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'FJTP- 072250', 72, 'Box', 723.21, 0, 723.21, 99, '2015-03-27 00:00:00'),
('71458', 'March 27, 2015', 'SANITARY CARE PRODUCTS ASIA, INCORPORATED', 'SCR-010288', 288, 'Box', 1, 0, 2, 100, '2015-03-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `logid` int(11) NOT NULL,
  `userid` varchar(20) NOT NULL,
  `action` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`logid`, `userid`, `action`, `description`, `date`) VALUES
(1, 'KEI001-W', 'Login', 'Warehouse Module', '0000-00-00 00:00:00'),
(2, 'KEI001-W', 'Login', 'Warehouse Module', '0000-00-00 00:00:00'),
(3, 'KEI001-W', 'Login', 'Warehouse Module', '0000-00-00 00:00:00'),
(4, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-07 16:54:37'),
(5, 'KEI002-M', 'Login', 'Manager Module', '2015-12-07 16:56:01'),
(6, 'KEI002-S', 'Login', 'Branch Module', '2015-12-07 16:56:16'),
(7, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-07 17:04:48'),
(8, 'KEI001-W', 'Logout', 'Warehouse Module', '2015-12-07 17:04:50'),
(9, 'KEI002-M', 'Login', 'Manager Module', '2015-12-07 17:05:09'),
(10, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-07 19:57:33'),
(11, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-08 06:36:56'),
(12, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-08 13:21:24'),
(13, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-08 18:19:26'),
(14, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-09 00:21:39'),
(15, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-09 08:01:06'),
(16, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-09 13:05:50'),
(17, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-09 13:43:30'),
(18, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-09 18:11:06'),
(19, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-10 09:14:25'),
(20, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-10 11:10:53'),
(21, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-10 14:07:00'),
(22, 'KEI001-W', 'Logout', 'Warehouse Module', '2015-12-10 16:03:50'),
(23, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-10 16:04:09'),
(24, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-10 22:31:42'),
(25, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-10 22:36:22'),
(26, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-10 23:12:22'),
(27, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-11 07:04:15'),
(28, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-12 14:55:59'),
(29, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-12 20:09:33'),
(30, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-12 20:13:46'),
(31, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-13 06:22:19'),
(32, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-13 06:26:35'),
(33, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-13 11:57:09'),
(34, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-13 19:49:45'),
(35, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-13 21:58:16'),
(36, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-14 05:30:30'),
(37, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-15 20:10:41'),
(38, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-16 07:53:40'),
(39, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-18 05:50:13'),
(40, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-18 05:56:22'),
(41, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-18 12:31:31'),
(42, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-18 18:10:56'),
(43, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-18 18:52:53'),
(44, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-19 12:05:24'),
(45, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-19 14:06:24'),
(46, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-19 14:31:00'),
(47, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-19 15:34:47'),
(48, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-19 15:42:46'),
(49, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-19 19:44:23'),
(50, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-19 21:58:28'),
(51, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-20 08:20:44'),
(52, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-20 08:20:44'),
(53, 'KEI002-S', 'Login', 'Branch Module', '2015-12-20 08:45:42'),
(54, 'KEI002-S', 'Logout', 'Branch Module', '2015-12-20 08:45:46'),
(55, 'KEG003-S', 'Login', 'Branch Module', '2015-12-20 08:45:53'),
(56, 'KEI001-W', 'Logout', 'Warehouse Module', '2015-12-20 09:02:29'),
(57, 'KEG003-S', 'Login', 'Branch Module', '2015-12-20 09:02:35'),
(58, 'KEG003-S', 'Logout', 'Branch Module', '2015-12-20 09:57:42'),
(59, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-20 09:57:52'),
(60, 'KEG003-S', 'Logout', 'Branch Module', '2015-12-20 11:31:18'),
(61, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-20 11:31:31'),
(62, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-20 13:28:18'),
(63, 'KEG003-S', 'Login', 'Branch Module', '2015-12-20 16:52:41'),
(64, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-20 17:56:41'),
(65, 'KEI002-S', 'Login', 'Branch Module', '2015-12-20 18:17:34'),
(66, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-20 22:47:41'),
(67, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-20 22:48:21'),
(68, 'KEI002-S', 'Login', 'Branch Module', '2015-12-21 00:10:15'),
(69, 'KEI001-W', 'Logout', 'Warehouse Module', '2015-12-21 00:34:27'),
(70, 'KEI002-S', 'Login', 'Branch Module', '2015-12-21 00:34:33'),
(71, 'KEI002-S', 'Logout', 'Branch Module', '2015-12-21 00:53:28'),
(72, 'KEI002-S', 'Login', 'Branch Module', '2015-12-21 00:53:42'),
(73, 'KEI002-S', 'Logout', 'Branch Module', '2015-12-21 00:54:35'),
(74, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-21 00:54:37'),
(75, 'KEI002-S', 'Logout', 'Branch Module', '2015-12-21 01:25:08'),
(76, 'KEI002-M', 'Login', 'Manager Module', '2015-12-21 01:25:24'),
(77, 'KEI002-M', 'Logout', 'Manager Module', '2015-12-21 01:35:34'),
(78, 'KEI001-W', 'Login', 'Warehouse Module', '2015-12-21 01:35:40');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `telephone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `name`, `address`, `telephone`) VALUES
(18, 'Nutri-Asia Incorporated', 'Warehouse No. 10 Hi-Tone Compound, Diversion Road, Bogtong, Legaspi City, Albay', '483-1099'),
(19, 'Golden Nutrious Foods Corporation', '1092 Lakandula Drive, Cruzada, Legaspi City, Albay', '742-1042'),
(20, 'Quanta Paper Corporation', '', ''),
(21, 'Fiberline Industries Incorporated', '', ''),
(22, 'Green Cross Incorporated', 'San Francisco. Guinobatan, Albay', ''),
(23, 'Rockey Crystal Top Corporation', '', ''),
(24, 'Lamoiyan Corporation', '', ''),
(25, 'Splash', '5th Floor W Office Bldg., 11th Avenue cor. 28th Street, Bonifacio Global City, Taguig, Philippines', ''),
(26, 'Colgate-Palmolive Company', '', ''),
(27, 'Wyeth Philippines Incorporated', '', ''),
(28, 'Sanitary Care Products Asia, Incorporated', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `sku` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_type` varchar(200) NOT NULL,
  `manufacturer` varchar(200) NOT NULL,
  `product_description` text NOT NULL,
  `supplier` int(11) NOT NULL,
  `box_cost` float NOT NULL DEFAULT '0',
  `pack_cost` float NOT NULL DEFAULT '0',
  `piece_cost` float NOT NULL DEFAULT '0',
  `box_price` float NOT NULL DEFAULT '0',
  `pack_price` float NOT NULL DEFAULT '0',
  `piece_price` float NOT NULL DEFAULT '0',
  `box_watch` int(200) NOT NULL DEFAULT '0',
  `pack_watch` int(200) NOT NULL DEFAULT '0',
  `piece_watch` int(200) NOT NULL DEFAULT '0',
  `box_stock` int(200) NOT NULL DEFAULT '0',
  `pack_stock` int(200) NOT NULL DEFAULT '0',
  `piece_stock` int(200) NOT NULL DEFAULT '0',
  `product_id` int(10) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`sku`, `product_name`, `product_type`, `manufacturer`, `product_description`, `supplier`, `box_cost`, `pack_cost`, `piece_cost`, `box_price`, `pack_price`, `piece_price`, `box_watch`, `pack_watch`, `piece_watch`, `box_stock`, `pack_stock`, `piece_stock`, `product_id`) VALUES
('FG000033', 'DP WHT VIN 385mLX24 PET', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 280.71, 0, 0, 13.5, 0, 0, 1, 1, 1, 10, 10, 10, 0000000001),
('FG000584', 'DP SOY SCE NML 385MLX24(PET)', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 325.71, 0, 0, 15.75, 0, 0, 1, 1, 1, 10, 10, 10, 0000000002),
('FG000587', 'DP SOY SCE NML 1LX12(PET)', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 364.29, 0, 0, 34.75, 0, 0, 1, 1, 1, 10, 10, 10, 0000000003),
('FG000496', 'MT LEC SCE REG325GX24 BOT', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 546.43, 0, 0, 26.25, 0, 0, 1, 1, 1, 10, 10, 10, 0000000004),
('FG000564', 'UFC BC 2KGX8 PCON', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 576.43, 0, 0, 82.5, 0, 0, 1, 1, 1, 10, 10, 10, 0000000005),
('FG000151', 'PAPA BC PLUS 320gX24 (BOT) ', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 334.29, 0, 0, 16.25, 0, 0, 1, 1, 1, 9, 8, 7, 0000000006),
('FG000152', 'PAPA  BC PLUS 1KGx12 (PCON) HYB', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 448.93, 0, 0, 43, 0, 0, 1, 1, 1, 10, 10, 10, 0000000007),
('FG000197', 'JUF RHCS 100Gx24 (PCON)', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 387.86, 0, 0, 18.5, 0, 0, 1, 1, 1, 10, 10, 10, 0000000008),
('FG000336', 'UFC SCS 340gX24(BOT)', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 750, 0, 0, 29.5, 0, 0, 1, 1, 1, 10, 10, 10, 0000000009),
('FG000153', 'MAF BC BLND 320gX24', 'Grocery', 'Nutri-Asia Incorporated', '', 1, 304.29, 0, 0, 14.75, 0, 0, 1, 1, 1, 10, 10, 10, 0000000010),
('157', 'GOLDEN INSTANT OATS 35X30X8', 'Grocery', 'Golden Nutrious Foods Corporation', '', 2, 0, 0, 7.5, 0, 0, 8, 1, 1, 1, 10, 10, 10, 0000000011),
('159', 'GOLDEN OATS + CHOCO 35GX30X8', 'Grocery', 'Golden Nutrious Foods Corporation', '', 2, 0, 0, 10.3, 0, 0, 11, 1, 1, 1, 10, 10, 10, 0000000012),
('99', 'GOLDEN OATS PLUS CHOCO  400GX24', 'Grocery', 'Golden Nutrious Foods Corporation', '', 2, 0, 0, 64.6, 0, 0, 68, 1, 1, 1, 10, 10, 10, 0000000013),
('161', 'GOLDEN OATS + VITE OMG 35GX30X8', 'Grocery', 'Golden Nutrious Foods Corporation', '', 2, 0, 0, 10.3, 0, 0, 11, 1, 1, 1, 10, 10, 10, 0000000014),
('102', 'GOLDEN OATS +VITE, OMG 200GX48', 'Grocery', 'Golden Nutrious Foods Corporation', '', 2, 0, 0, 38.75, 0, 0, 40.75, 1, 1, 1, 10, 10, 10, 0000000015),
('103', 'GOLDEN OATS +VITE, OMG 400GX24', 'Grocery', 'Golden Nutrious Foods Corporation', '', 2, 0, 0, 69.25, 0, 0, 72.75, 1, 1, 1, 10, 10, 10, 0000000016),
('271', 'MARCA PINA SOY S. DOY 120MLX72', 'Grocery', '', '', 2, 0, 0, 4, 0, 0, 4.25, 1, 1, 1, 10, 10, 10, 0000000017),
('272', 'MARCA PINA SOY S. DOY 220MLX48', 'Grocery', '', '', 2, 0, 0, 6, 0, 0, 6.5, 1, 1, 1, 10, 10, 10, 0000000018),
('282', 'MARCA SUKANG PUTI PCH 220MLX48', 'Grocery', '', '', 2, 0, 0, 4.5, 0, 0, 4.75, 1, 1, 1, 10, 10, 10, 0000000019),
('285', 'MARCA SUKANG PUTI 1/2 500MLX24', 'Grocery', '', '', 2, 366, 0, 0, 0, 0, 16, 1, 1, 1, 10, 10, 10, 0000000020),
('55210', 'NISSIN MINI BEEF 40GX48', 'Grocery', '', '', 3, 1, 602.05, 0, 14.5, 0, 0, 1, 1, 1, 10, 10, 10, 0000000021),
('55211', 'NISSIN MINI SEAFOOD 40GX48', 'Grocery', '', '', 3, 1, 602.05, 0, 14.5, 0, 0, 1, 1, 1, 10, 10, 10, 0000000022),
('55160', 'N-YAKISOBA CHKN POUCH', 'Grocery', '', '', 3, 1, 520.45, 0, 8.75, 0, 0, 1, 1, 1, 10, 10, 10, 0000000023),
('55161', 'N-YAKISOBA BEEF POUCH', 'Grocery', '', '', 3, 1, 520.45, 0, 8.75, 0, 0, 1, 1, 1, 10, 10, 10, 0000000024),
('55332', 'PAYLESS CHICKEN50X72', 'Grocery', '', '', 3, 3, 385.71, 0, 6.5, 0, 0, 1, 1, 1, 10, 10, 10, 0000000025),
('55333', 'PAYLESS BEEF 50X72', 'Grocery', '', '', 3, 3, 385.71, 0, 6.5, 0, 0, 1, 1, 1, 10, 10, 10, 0000000026),
('55598', 'PAYLESS OKKK XTRA BIG KALAMANSI 130X48', 'Grocery', '', '', 3, 0, 0, 0, 0, 0, 14.5, 1, 1, 1, 10, 10, 10, 0000000027),
('55599', 'PAYLESS OKKK XTRA BIG ORIGINAL 130X48', 'Grocery', '', '', 3, 0, 0, 0, 0, 0, 14.5, 1, 1, 1, 10, 10, 10, 0000000028),
('55661', 'PAYLESS PCANTON X-BIG CHILIMANSI 130X48', 'Grocery', '', '', 3, 0, 0, 0, 0, 0, 14.5, 1, 1, 1, 10, 10, 10, 0000000029),
('55680', 'PAYLESS XTRA BIG HOT CHILI 130X48', 'Grocery', '', '', 3, 0, 0, 0, 0, 0, 14.5, 1, 1, 1, 10, 10, 10, 0000000030),
('FRS033', 'FRESH POP UP TISSUE 2PLY 40PULLS 80S SPX96', 'Grocery', '', '', 4, 424.29, 0, 0, 0, 0, 5.75, 1, 1, 1, 10, 10, 10, 0000000031),
('FGHAR010', 'HARMONY TNFOLDED 1PLY 40S SPx100', 'Grocery', 'Quanta Paper Corporation', '', 4, 888.39, 0, 0, 0, 0, 10.5, 1, 1, 1, 10, 10, 10, 0000000032),
('FGHAR008', 'HARMONY TNPC 1PLY 1000S SPx20', 'Grocery', 'Quanta Paper Corporation', '', 4, 766.96, 0, 0, 0, 0, 45, 1, 1, 1, 10, 10, 10, 0000000033),
('FGVNT070', 'VANITA BTR 2PLY 200P 400s SPx96', 'Grocery', '', '', 4, 1, 0, 0, 0, 0, 12.25, 1, 1, 1, 10, 10, 10, 0000000034),
('FGPCD002D', 'PRIMECARE ADULT DIAPER LARGE 10sx8W/FREE', 'Grocery', 'Fiberline Industries Incorporated', '', 4, 1, 0, 0, 0, 0, 187.75, 1, 1, 1, 10, 10, 10, 0000000035),
('FGSBD218', 'SWEET BABY DIAPERS MEDIUM 12X12(TRIFOLD)', 'Grocery', 'Quanta Paper Corporation', '', 4, 641.25, 0, 0, 0, 0, 60.25, 1, 1, 1, 10, 10, 10, 0000000036),
('FGSBD219', 'SWEET BABY DIAPERS LARGE 12X12 (TRIPOLD)', 'Grocery', 'Quanta Paper Corporation', '', 4, 726.96, 0, 0, 0, 0, 68.5, 1, 1, 1, 10, 10, 10, 0000000037),
('PCD003', 'PRIMECARE ADULT DIAPER SOLO MEDIUM SPx80', 'Grocery', '', '', 4, 1, 0, 0, 0, 0, 16, 1, 1, 1, 10, 10, 10, 0000000038),
('PCD004', 'PRIMECARE ADULT DIAPER SOLO LARGE SPx80P', 'Grocery', '', '', 4, 1, 0, 0, 0, 0, 19, 1, 1, 1, 10, 10, 10, 0000000039),
('FGSBD217', 'SWEET BABY DIAPERS SMALL 12X12 (TRIFOLD)', 'Grocery', '', '', 4, 558.21, 0, 0, 0, 0, 52.5, 1, 1, 1, 10, 10, 10, 0000000040),
('FC1', 'GREEN CROSS ALCOHOL 70% 500 ML', 'Grocery', 'Green Cross Incorporated', '', 5, 0, 0, 59.2, 0, 0, 63.5, 1, 1, 1, 10, 10, 10, 0000000041),
('FC2', 'GREEN CROSS ALCOHOL 70% 150 ML', 'Grocery', 'Green Cross Incorporated', '', 5, 0, 0, 24.2, 0, 0, 26, 1, 1, 1, 10, 10, 10, 0000000042),
('FC3', 'GREEN CROSS ALCOHOL 70% 75 ML', 'Grocery', 'Green Cross Incorporated', '', 5, 390, 0, 0, 0, 0, 17.5, 1, 1, 1, 10, 10, 10, 0000000043),
('FC4', 'GREEN CROSS ALCOHOL 40% 500 ML', 'Grocery', 'Green Cross Incorporated', '', 5, 0, 0, 44.3, 0, 0, 47.5, 1, 1, 1, 10, 10, 10, 0000000044),
('FC5', 'GREEN CROSS ALCOHOL 40%250 ML', 'Grocery', 'Green Cross Incorporated', '', 5, 0, 0, 26.75, 0, 0, 28.75, 1, 1, 1, 10, 10, 10, 0000000045),
('FC6', 'GREEN CROSS ALCOHOL W/MOISTURIZER 70% 500 ML', 'Grocery', 'Green Cross Incorporated', '', 5, 0, 0, 59.9, 0, 0, 64, 1, 1, 1, 10, 10, 10, 0000000046),
('FC7', 'GREEN CROSS ALCOHOL W/MOISTURIZER 70% 150ML', 'Grocery', 'Green Cross Incorporated', '', 5, 0, 0, 24.6, 0, 0, 26.5, 1, 1, 1, 10, 10, 10, 0000000047),
('FC8', 'GREEN CROSS ALCOHOL W/ MOISTURIZER 70% 60 ML', 'Grocery', 'Green Cross Incorporated', '', 5, 362.4, 0, 0, 0, 0, 16.25, 1, 1, 1, 10, 10, 10, 0000000048),
('FC9', 'GREEN CROSS ALCOHOL W/ MOISTURIZER 40% 500 ML', 'Grocery', 'Green Cross Incorporated', '', 5, 0, 0, 47.6, 0, 0, 51, 1, 1, 1, 10, 10, 10, 0000000049),
('FC10', 'GREEN CROSS ALCOHOL W/MOISTURIZER 40% 250 ML', 'Grocery', 'Green Cross Incorporated', '', 5, 0, 0, 29, 0, 0, 31, 1, 1, 1, 10, 10, 10, 0000000050),
('RACK0', 'MR. GULAMAN RED(UNFVRD) 24GX10IB', 'Grocery', 'Rockey Crystal top Corporation', '', 6, 93.5, 0, 0, 0, 0, 10.5, 1, 1, 1, 10, 10, 10, 0000000051),
('RACK1', 'MR. GULAMAN GREEN (UNFVRD)24X10IB', 'Grocery', 'Rockey Crystal top Corporation', '', 6, 93.5, 0, 0, 0, 0, 10.5, 1, 1, 1, 10, 10, 10, 0000000052),
('RACK5', 'MR. GULAMAN YLOW(UNFVRD)24GX10IB', 'Grocery', 'Rockey Crystal top Corporation', '', 6, 93.5, 0, 0, 0, 0, 10.5, 1, 1, 1, 10, 10, 10, 0000000053),
('RACK6', 'MR. GULAMAN WHTE(UNFVRD)24GX10IB', 'Grocery', 'Rockey Crystal top Corporation', '', 6, 93.5, 0, 0, 0, 0, 10.5, 1, 1, 1, 10, 10, 10, 0000000054),
('RACK7', 'MR.GULAMAN BLCK(UNFVRD)24GX10IB', 'Grocery', 'Rockey Crystal top Corporation', '', 6, 93.5, 0, 0, 0, 0, 10.5, 1, 1, 1, 10, 10, 10, 0000000055),
('RACK9', 'MR.GULAMAN RED/STRWBRY(FV)24GX10IB', 'Grocery', 'Rockey Crystal top Corporation', '', 6, 93.5, 0, 0, 0, 0, 10.5, 1, 1, 1, 10, 10, 10, 0000000056),
('RACK10', 'MR. GULAMAN  GRN/BKOPNDN(FV)24GX10IB', 'Grocery', 'Rockey Crystal top Corporation', '', 6, 93.5, 0, 0, 0, 0, 10.5, 1, 1, 1, 10, 10, 10, 0000000057),
('TOSEN', 'SWAN SARDINES GREEN 100X130G', 'Grocery', 'Rockey Crystal top Corporation', '', 6, 1, 0, 0, 0, 0, 13, 1, 1, 1, 10, 10, 10, 0000000058),
('50561', 'DAZZ LEMON OP W/SPONGE48X200G', 'Grocery', 'Lamoiyan Corporation', '', 6, 936, 0, 0, 0, 0, 21.75, 1, 1, 1, 10, 10, 10, 0000000059),
('50566', 'DAZZ LIME OP W/SPONGE 48X200G', 'Grocery', 'Lamoiyan Corporation', '', 6, 936, 0, 0, 0, 0, 21.75, 1, 1, 1, 10, 10, 10, 0000000060),
('405629', 'MAXIPEEL EXFO CRM #1 MILD 10GX144 NEW', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 17.75, 1, 1, 1, 10, 10, 10, 0000000061),
('405630', 'MAXIPEEL EXFO CRM #2 MOD 10GX144 NEW', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 18.75, 1, 1, 1, 10, 10, 10, 0000000062),
('405631', 'MAXIPEEL EXFO  CRM #3 ADV 10GX144 NEW', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 19.25, 1, 1, 1, 10, 10, 10, 0000000063),
('405624', 'MAXIPEEL CONCLNG CRM FAIR 10GX144(NEW)', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 19, 1, 1, 1, 10, 10, 10, 0000000064),
('405670', 'MAXIPEEL EXFO SOLN #1 MILD 30MLX96 NEW', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 34, 1, 1, 1, 10, 10, 10, 0000000065),
('405022', 'SKINWHITE CLSCWHTNG LOT CLSC SPF 10 200ML', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 125, 1, 1, 1, 10, 10, 10, 0000000066),
('405024', 'SKINWHITE ADV WHTNG LOT PWRWHTNNG 200ML', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 233, 1, 1, 1, 10, 10, 10, 0000000067),
('404992', 'SKINWHITE ADV WHTNG LOT GLUTA+VITC 100ML', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 171.75, 1, 1, 1, 10, 10, 10, 0000000068),
('405327', 'SKINWHTE ADV FCRM PWDR WHITE 7G', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 19, 1, 1, 1, 10, 10, 10, 0000000069),
('405326', 'SKINWHITE ADV FCRM PWDR LIGHT BEIGE 7G', 'Grocery', 'Splash', '', 7, 0, 0, 0, 0, 0, 19, 1, 1, 1, 10, 10, 10, 0000000070),
('1524631', 'COL TP FOR KIDS BARBIE 40G LDT', 'Grocery', 'Colgate-Palmolive Company', '', 8, 918, 0, 0, 0, 0, 29.75, 1, 1, 1, 10, 10, 10, 0000000071),
('1509880', 'TENDER CARE BS CLASSIC 55G (PHIL)', 'Grocery', 'Colgate-Palmolive Company', '', 8, 758.57, 0, 0, 0, 0, 12.5, 1, 1, 1, 10, 10, 10, 0000000072),
('1529608', 'PALM SH INTENSIVE MOISTURE 180MLx', 'Grocery', 'Colgate-Palmolive Company', '', 8, 1, 0, 0, 0, 0, 93.5, 1, 1, 1, 10, 10, 10, 0000000073),
('1529623', 'PON SH VIBRANT COLOR 180 MLx24(PH)', 'Grocery', '', '', 8, 1, 0, 0, 0, 0, 93.5, 1, 1, 1, 10, 10, 10, 0000000074),
('1529613', 'PON SH COMPLETE REPAIR 180MLx24_N', 'Grocery', '', '', 8, 1, 0, 0, 0, 0, 94.25, 1, 1, 1, 10, 10, 10, 0000000075),
('1529699', 'PALM NAT SH HEALTHY & SMOOTH 90M', 'Grocery', 'Colgate-Palmolive Company', '', 8, 1, 0, 0, 0, 0, 50, 1, 1, 1, 10, 10, 10, 0000000076),
('1535245', 'TENDER CARE TALC PINK 100G', 'Grocery', 'Colgate-Palmolive Company', '', 8, 1, 0, 0, 0, 0, 29.5, 1, 1, 1, 10, 10, 10, 0000000077),
('1535246', 'TC TALCUM PINK SOFT 50G (PINK)', 'Grocery', '', '', 8, 990, 0, 0, 0, 0, 16.5, 1, 1, 1, 10, 10, 10, 0000000078),
('1536329', 'TENDER CARE TALC-BLUE 100G', 'Grocery', 'Colgate-Palmolive Company', '', 8, 1, 0, 0, 0, 0, 29.5, 1, 1, 1, 10, 10, 10, 0000000079),
('1529628', 'PON CON INTENS MOIST 14ML DUAL (PH)', 'Grocery', '', '', 8, 1, 0, 0, 0, 0, 13.5, 1, 1, 1, 10, 10, 10, 0000000080),
('WPI001', 'BONNA 180GX64', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 0, 0, 99.05, 0, 0, 101, 1, 1, 1, 1, 1, 1, 0000000081),
('WPI053', 'BONNA 360G X 32', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 0, 0, 198.84, 0, 0, 203, 1, 1, 1, 10, 10, 10, 0000000082),
('WPI004', 'BONAMIL 180GX64', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 0, 0, 99.05, 0, 0, 101, 1, 1, 1, 10, 10, 10, 0000000083),
('WPI007', 'BONAMIL 900GX6', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 2, 0, 0, 495, 0, 0, 1, 1, 1, 10, 10, 10, 0000000084),
('WPI058', 'BONAMIL 1.2KG X 12 (400GX3)', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 0, 0, 312.32, 0, 0, 621.5, 1, 1, 1, 10, 10, 10, 0000000085),
('WPI008', 'BONAKID 180GX64', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 0, 0, 99.05, 0, 0, 101.5, 1, 1, 1, 10, 10, 10, 0000000086),
('WPI009', 'BONAKID 400GX36', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 0, 0, 221.22, 0, 0, 225.5, 1, 1, 1, 10, 10, 10, 0000000087),
('WPI052', 'BONAKID 900GX6', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 2, 0, 0, 495, 0, 495, 1, 1, 1, 10, 10, 10, 0000000088),
('WPI046', 'BONAKID 1.6KG (400GX4)X10', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 0, 0, 797.44, 0, 0, 809.5, 1, 1, 1, 10, 10, 10, 0000000089),
('WPI024', 'PROMIL 1.6KGX6', 'Grocery', 'Wyeth Philippines Incorporated', '', 9, 0, 0, 1, 0, 0, 1, 1, 1, 1, 10, 10, 10, 0000000090),
('RMC1', 'SUNNY CHEERS PEANUT BUTTER 2KGSX6PLS', 'Grocery', '', '', 10, 1, 0, 0, 0, 0, 186, 1, 1, 1, 10, 10, 10, 0000000091),
('RMC2', 'EGGHEADS MILK CHOCO 60BAGX20''S', 'Grocery', '', '', 10, 690, 0, 0, 0, 0, 12.25, 1, 1, 1, 10, 10, 10, 0000000092),
('RMC3', 'EGGHEADS MILK CHOCO 48BAGX25''S', 'Grocery', '', '', 10, 696, 0, 0, 0, 0, 15.5, 1, 1, 1, 10, 10, 10, 0000000093),
('RMC4', 'BEAR MARSHMALLOWS 24BAGSX55G', 'Grocery', '', '', 10, 225.69, 0, 0, 0, 0, 10, 1, 1, 1, 10, 10, 10, 0000000094),
('RMC5', 'CATERPILLAR MALLOWS 24BAGSX55G', 'Grocery', '', '', 10, 228, 0, 0, 0, 0, 10, 1, 1, 1, 10, 10, 10, 0000000095),
('RMC6', 'FLOWER MALLOWS 12BAGSX135G', 'Grocery', '', '', 10, 259.63, 0, 0, 0, 0, 23, 1, 1, 1, 10, 10, 10, 0000000096),
('RMC7', 'MELLO MALLOWSPORT 12BAGSX135G', 'Grocery', '', '', 10, 225.69, 0, 0, 0, 0, 10, 1, 1, 1, 10, 10, 10, 0000000097),
('RMC8', 'MAX MALLOWS VANILLA 12BAGSX135G', 'Grocery', '', '', 10, 259.63, 0, 0, 0, 0, 23, 1, 1, 1, 10, 10, 10, 0000000098),
('RMC9', 'MELO PASTEL MARSHMALLOWS 24BAGSX55G', 'Grocery', '', '', 10, 225.69, 0, 0, 0, 0, 10, 1, 1, 1, 10, 10, 10, 0000000099),
('RMC10', 'RAINBOW SPRINKLE 15GX10BAGSX12''S', 'Grocery', '', '', 10, 477.5, 0, 0, 0, 0, 50.75, 1, 1, 1, 10, 10, 10, 0000000100),
('BSN-961400T', 'B.T. SANICARE SOLO 400SHTS 2PLY TINTED DECOR', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 1, 0, 0, 0, 0, 14.5, 1, 1, 1, 7, 6, 3, 0000000101),
('BTV-961400', 'B.T. TISYU SOLO 400SHTS 2P', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 925.71, 0, 0, 0, 0, 12, 1, 1, 1, 7, 7, 7, 0000000102),
('CCTV-090230', 'B.T. TISYU CORELESS TWIN 30g', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 843.74, 0, 0, 0, 0, 11.5, 1, 1, 1, 10, 10, 10, 0000000103),
('BFR-244300T', 'B.T. FEMME 4''S 300SHTS 2P TINTED DECOR', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 653.57, 0, 0, 0, 0, 33.5, 1, 1, 1, 0, 0, 0, 0000000104),
('BJR- 961320', 'B.T. JADE SOLO 320SHTS 2P', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 642.86, 0, 0, 0, 0, 8.25, 1, 1, 1, 0, 9, 9, 0000000105),
('KTFA-162752', 'K.T. FEMME AP TWIN 75P 2P', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 857.14, 0, 0, 0, 0, 66, 1, 1, 1, 10, 10, 10, 0000000106),
('TCFL-032100', 'T.N. CHEERS FLAT 100''S 1P', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 800, 0, 0, 0, 0, 31, 1, 1, 1, 10, 10, 10, 0000000107),
('FFTP-072240', 'F.T. FEMME TRAVEL PACK 40P 2P', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 642.86, 0, 0, 0, 0, 11, 1, 1, 1, 10, 10, 10, 0000000108),
('FJTP- 072250', 'F.T. JADE TRAVEL PACK 50P 2P', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 723.21, 0, 0, 0, 0, 12.5, 1, 1, 1, 10, 10, 10, 0000000109),
('SCR-010288', 'SANICARE COTTON ROLLS 10G', 'Grocery', 'Sanitary Care Products Asia, Incorporated', '', 11, 1, 0, 0, 0, 0, 6, 1, 1, 1, 10, 10, 10, 0000000110);

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `request_id` varchar(200) NOT NULL,
  `product_id` varchar(200) NOT NULL,
  `box` int(11) NOT NULL,
  `pack` int(11) NOT NULL,
  `piece` int(11) NOT NULL,
  `sender_id` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'pending',
  `request_date` datetime NOT NULL,
  `id` int(11) NOT NULL,
  `orderSubject` varchar(200) NOT NULL,
  `orderMessage` text NOT NULL,
  `orderRecipient` varchar(200) NOT NULL,
  `deliveryLocation` varchar(200) NOT NULL,
  `isOpened` tinyint(1) NOT NULL,
  `approved_box` int(11) DEFAULT NULL,
  `approved_pack` int(11) DEFAULT NULL,
  `approved_piece` int(11) DEFAULT NULL,
  `is_updated` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`request_id`, `product_id`, `box`, `pack`, `piece`, `sender_id`, `location`, `status`, `request_date`, `id`, `orderSubject`, `orderMessage`, `orderRecipient`, `deliveryLocation`, `isOpened`, `approved_box`, `approved_pack`, `approved_piece`, `is_updated`) VALUES
('5675f5f8cb107', '0000000103', 0, 0, 0, 'KEI001-W', 'Warehouse', 'pending', '2015-12-20 08:27:43', 1, 'Order list for supplier', '', 'Kier Enterprises Gubat', 'Warehouse', 1, NULL, NULL, NULL, 0),
('5675f5f8cb107', '0000000016', 0, 0, 0, 'KEI001-W', 'Warehouse', 'pending', '2015-12-20 08:27:43', 2, 'Order list for supplier', '', 'Kier Enterprises Gubat', 'Warehouse', 1, NULL, NULL, NULL, 0),
('151220-083332', '0000000005', 0, 0, 0, 'KEI001-W', 'Warehouse', 'pending', '2015-12-20 08:33:37', 4, 'Request form for supplier', '', 'ACCELERATED DISTRIBUTION INCORPORATED', 'Warehouse', 0, NULL, NULL, NULL, 0),
('5675f790d7dbf', '0000000101', 0, 0, 0, 'KEI001-W', 'Warehouse', 'pending', '2015-12-20 08:34:29', 5, 'Request form for branch', '', 'Kier Enterprises Gubat', 'Warehouse', 1, NULL, NULL, NULL, 0),
('56760ae47c915', '0000000101', 3, 3, 3, 'KEG003-S', 'Kier Enterprises Gubat', 'Pending', '2015-12-20 02:57:33', 6, 'Warehouse to Branch Request', '', 'Warehouse', 'Kier Enterprises Gubat', 1, 1, 2, 0, 1),
('56760ae47c915', '0000000008', 3, 3, 3, 'KEG003-S', 'Kier Enterprises Gubat', 'Approved', '2015-12-20 02:57:33', 7, 'Warehouse to Branch Request', '', 'Warehouse', 'Kier Enterprises Gubat', 1, 0, 0, 0, 1),
('5676804561217', '0000000105', 1, 1, 1, 'KEI002-S', 'Kier Enterprises Irosin', 'Disapproved', '2015-12-20 11:18:09', 8, 'Warehouse to Branch Request', '', 'Warehouse', 'Kier Enterprises Irosin', 1, 0, 0, 0, 1),
('5676804561217', '0000000102', 2, 2, 2, 'KEI002-S', 'Kier Enterprises Irosin', 'Approved', '2015-12-20 11:18:09', 9, 'Warehouse to Branch Request', '', 'Warehouse', 'Kier Enterprises Irosin', 1, 1, 1, 1, 1),
('5676804561217', '0000000096', 3, 3, 3, 'KEI002-S', 'Kier Enterprises Irosin', 'Disapproved', '2015-12-20 11:18:09', 10, 'Warehouse to Branch Request', '', 'Warehouse', 'Kier Enterprises Irosin', 1, 0, 0, 0, 1),
('5676dd57b7995', '0000000105', 1, 2, 2, 'KEI001-W', 'Warehouse', 'pending', '2015-12-21 00:55:03', 11, 'Request form for branch', '1', 'Kier Enterprises Irosin', 'Warehouse', 1, NULL, NULL, NULL, 0),
('5676dd57b7995', '0000000085', 2, 2, 2, 'KEI001-W', 'Warehouse', 'pending', '2015-12-21 00:55:03', 12, 'Request form for branch', '', 'Kier Enterprises Irosin', 'Warehouse', 1, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` tinytext NOT NULL,
  `telephone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `telephone`) VALUES
(1, 'ACCELERATED DISTRIBUTION INCORPORATED', 'Warehouse No. 10. Hi-Tone Compound, Diversion Road, Bogtong, Legaspi City, Albay', ''),
(2, 'BAMBINO TRADING', '1091 Lakandula Drive, Cruzada, Legaspi City, Albay', ''),
(3, 'BICOL EXCELLENT SALES & TRADING CORPORATION (BEST)', 'Purok 5, Alegre Street, Balogo, East District, Sorsogon City, Sorsogon', ''),
(4, 'BONHAUR MARKETING CORPORATION', 'Block 4, Lot 5, Mountain View Indl. Complex Maguyam Road, Bancal, Carmona, Cavite', '(02) 529-8336'),
(5, 'FORNIX CORPORATION', 'Zone 7 Arimbay, Legaspi City, Albay', '(052) 838-0543'),
(6, 'PAX & FOUND SALES DISTRIBUTOR', 'Doña Maria Subdivision, Phase IV, Tagas, Daraga, 4501, Albay', '(052) 838-5007'),
(7, 'PRIME GLOBAL - GTD Corporation', 'Partido Ricemill New San Roque, Pili Camarines Sur', '(054) 477-2940'),
(8, 'PRIME SELLER MARKETING', 'RASI Compound, Brgy. Bigaa, Legaspi City, Albay', '(052) 472-9848'),
(9, 'PRINCETON ENTERPRISES', 'Building 8, PLDC Compound Diversion Road, Naga City, Camarines Sur', '(054) 473-2206'),
(10, 'RODZON MARKETING CORPORATION', '2451 Lakandula Street, Pasay City, Metro Manila, Philippines', '(631) 844-8001'),
(11, 'SANITARY CARE  PRODUCTS ASIA, INCORPORATED', '4403 San Jose, Pii, Camarines Sur', '(054) 478-7797');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `product_id` int(10) UNSIGNED ZEROFILL NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`product_id`, `name`) VALUES
(0000000001, '2323'),
(0000000002, ''),
(0000000003, ''),
(0000000004, '5'),
(0000000005, '5'),
(0000000006, '4'),
(0000000007, '4'),
(0000000008, '4'),
(0000000009, '4'),
(0000000010, '6'),
(0000000011, '6'),
(0000000012, '6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_old`
--
ALTER TABLE `invoice_old`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`logid`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `invoice_old`
--
ALTER TABLE `invoice_old`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `logid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `product_id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
