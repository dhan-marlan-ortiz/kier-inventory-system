The Lewis College
College of Computer Studies
479 Magsaysay St., Cogon, Sorsogon City

WEB-BASED WAREHOUSE INVENTORY SYSTEM FOR KIER ENTERPRISES

An Undergraduate Capstone Project
Presented to the Faculty of College of Computer Studies
The Lewis College
Sorsogon City

In Partial Fulfillmentof the Requirement for the Degree of
Bachelor of Science in Information Technology

By:
Ortiz, Dhan Marlan A., Programmer
Gayon, Jessica R., System Analyst
France, Neckelyn F., Documentarian

School Year 2015-2016

########## ########## ########## ########## ##########

RECOMMENDATION FOR ORAL EXAMINATION

This Capstone Project entitled “Web-based Warehouse Inventory System for Kier Enterprises”prepared and submitted by Dhan Marlan A. Ortiz, Jessica R. Gayon and Neckelyn F. France in partial fulfilment for the Degree in Bachelor of Science in Information Technology is hereby submitted to the System Project Committee for consideration.

Christian D. Jamisola
System Adviser

Mutya A. Aycocho
Content Editor

########## ########## ########## ########## ##########

CAPSTONE PROJECT COMMITTEE
The Capstone Project “Web-based Warehouse Inventory System for Kier Enterprises”prepared and submitted by Dhan Marlan A. Ortiz, Jessica R. Gayon and Neckelyn F. France in partial fulfilment for the Degree in Bachelor of Science in Information Technology is hereby accepted for the Final Oral Examination.

MICHAEL ANGELO D. BROGADA, M.I.T
Professor, Capstone Project

########## ########## ########## ########## ##########

APPROVAL SHEET

Under the recommendation of the Oral Examination Committee, this Capstone Project entitled “Web-based Warehouse Inventory System for Kier Enterprises” prepared and submitted by Dhan Marlan A. Ortiz, Jessica R. Gayon and Neckelyn F. France is hereby approved in partial fulfilment for the Degree in Bachelor of Science in Information Technology.

MICHAEL ANGELO D. BROGADA, M.I.T
Professor, Capstone Project

########## ########## ########## ########## ##########

CONTENT EDITOR'S CERTIFICATION

This is certify that this Capstone Project "Web-based Warehouse Inventory System for Kier Enterprises" prepared and submitted by Dhan Marlan A. Ortiz, Jessica R. Gayon and Neckelyn F. France in partial fulfillment for the Degree in Bachelor of Science in Information Technology has been read and edited.

Mutya A. Aycocho
Content Editor

########## ########## ########## ########## ##########

SYSTEM ADVISER'S CERTIFICATION

This is certify that this Capstone Project "Web-based Warehouse Inventory System for Kier Enterprises" prepared and submitted by Dhan Marlan A. Ortiz, Jessica R. Gayon and Neckelyn F. France in partial fulfilment for the Degree in Bachelor of Science in Information Technology has been read and edited.

Christian D. Jamisola
System Adviser

########## ########## ########## ########## ##########

CERTIFICATE OF COMPLETED CLIENT TEST

This capstone Project entitled “Web-based Warehouse Inventory System of Kier Enterprises” and submitted by Dhan Marlan A. Ortiz, Jessica R. Gayon and Neckelyn F. France in partial fulfilment for the Degree in Bachelor of Science in Information Technology, had completed series of client test and was evaluated with the rating of _______.

This certification is issued this 4th day of December, 2015 upon the request of the above mentioned BSIT-IV students requisite to their application in final defense.

Yvonne S. Yau
Manager

########## ########## ########## ########## ##########

ACKNOWLEDGEMENT

We would like to extend our sincerest thanks to the following persons who have supported the Project Team throughout the development of the study:

To my friends and classmates who have shared their thoughts and ideas that related in our Capstone Project.

To our professor in our Capstone Project Michael Angelo D. Brogada, Christian D. Jamisola our System Editor, and Mutya A. Aycocho our Content Editor, for guiding and sharing your knowledge for the completion of this proposed study.

To our Parents, for the support that they have shownus in order to achieve our goal.

Lastly, to our Almighty God for giving us the strength and wisdom we need in making the Capstone Project a big accomplishment.

Team Strong JND

########## ########## ########## ########## ##########

ABSTRACT

Title : Web-based Warehouse Inventory System for Kier Enterprises
Author :Dhan Marlan A. Ortiz, Jessica R. Gayon and Neckelyn F. France
Document Type : Unpublished Undergraduate Capstone Project
Host : The Lewis College
Keyword : Warehouse, Inventory System, Web-based System

The Web-based Warehouse Inventory System for Kier Enterprises was developed to create a more efficient and reliable way of sorting and monitoring the company’s stocks.

The Inventory System intended to improve the current manual system used by Kier Enterprises. It automates the inventory processes of Kier Enterprises, from handling the request of products from different store branches to an automated generation of Monthly Reports, On-Hand Count of stocks, Delivery and Request Reports. The system helps the Kier Enterprises to monitor the available stocks and the products that arrive in their warehouse and stores. With the help of this new generation of technology, the users can now do their specific tasks more effectively and efficiently.

The team used the Rapid Application Development which is a programming system that enables programmers to quickly build working programs. This also has four phases namely the Requirements Planning, System Design, Development, and Cutover. Requirements planning is where the team outlines all the requirements for the proposed system. System Design is where the team creates the initial look and process of the system based on the data gathered in the first phase. Development phase is where the project team creates the prototypes and develops codes by modules. The last phase is the cutover where the team conducted client testing using the actual system designed for the client.

Based on the result of the Software Evaluation, the respondents found out that the new system truly automated the manual transaction process of the client and can perform easily the data manipulation, product monitoring, request stocks from store branches, purchase order and report generation. The system’s efficiency reduced the use of paper for record-keeping, and the transactions are done faster and easier. Generation of Monthly Reports, On-Hand Count of stocks, Delivery and Request Reports are now computerized and can be printed out for easy access.